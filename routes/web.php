<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

//for admin controller
Route::get('/login', function () {
    return view('/auth/login');
});


Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home')->middleware('verified');
Route::post('/sendSMS','SendSMSController@store')->middleware('auth')->middleware('verified');
Route::post('/getBalance','SendSMSController@getBalance')->middleware('auth')->middleware('verified');
Route::get('/demo','DemoController@index')->middleware('auth')->middleware('verified');
Route::prefix('/admin')->middleware(['auth','verified'])->group(function () {
	Route::resource('/module','ModuleController',['expect'=>['show']]);
	Route::resource('/submodule','SubModuleController');
	Route::resource('/organizationdetail','OrganizationDetailController');
	Route::resource('/menusetup','MenuSetupController');
	Route::resource('/submenusetup','SubMenuSetupController');
	Route::resource('/usersetup', 'UserSetupController');
	Route::get('/changepassword/create','UserSetupController@changepasswordcreate');
	Route::get('/changepassword','UserSetupController@changepassword')->name('usersetup.changepassword');
	Route::resource('/footercontactsetup','FooterContactSetupController');
	Route::resource('/quicklinksetup','QuickLinkSetupController');
	Route::resource('/socialmediaicon','SocialMediaIconController');
	Route::resource('/slidersetup','SliderSetupController');
	Route::resource('/noticesetup','NoticeSetupController');
	Route::resource('/aboutus','AboutUsController');
	Route::resource('/service','ServiceController');
	Route::resource('/counter','CounterSetupController');
	Route::resource('/timetable','TimeTableController');
	Route::resource('/membermessage','MemberSetupController');
	Route::resource('/eventsetup','EventSetupController');
	Route::resource('/partner','PartnerController');
	Route::resource('/gallery','GalleryController');
	Route::resource('/testimonial','TestimonialController');
	Route::get('/gallery/album/{id}','GalleryController@intoAlbum');
	Route::post('/gallery/upload/store','GalleryController@fileStore')->name('users.fileupload');
	Route::get('/image/delete','GalleryController@fileDestroy');
	Route::get('/delete/image/{id}/{albumid}','GalleryController@deleteImage');
	Route::resource('/class','ClassesController');
	Route::resource('/section','SectionController');
	Route::resource('/subject','SubjectController');
	Route::resource('/lessonentity','LessonEntityController');
	Route::resource('/lessondetail','LessondetailController');
	Route::resource('/lesson','LessonController');
	Route::get('/getsubject','LessonController@getsubject');
	Route::get('/getlesson','LessonController@getlesson');
	Route::resource('/student','StudentController');
	Route::get('/getsection','StudentController@getsection');
});


//for frontend controller
Route::get('/','FrontEndController@index');
Route::get('/aboutus','FrontEndController@aboutus');
Route::get('/classes','FrontEndController@classes');
Route::get('/classes-timetable','FrontEndController@classestimetable');
Route::get('/services','FrontEndController@services');
Route::get('/ourteam','FrontEndController@ourteam');
Route::get('/contact','FrontEndController@contact');
Route::post('/contact','FrontEndController@sendmail');
Route::get('/ourblog','FrontEndController@ourblog');
Route::get('/gallery','FrontEndController@gallery');
Route::get('/bmi-calculator','FrontEndController@bmicalculator');
Route::post('/testimonial','FrontEndController@testimonial');




