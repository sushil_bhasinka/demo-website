@extends('layouts.adminlayouts.app')
@section('content')
<!-- start page content -->
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<div class="page-title-breadcrumb">
				<div class=" pull-left">
					<div class="page-title">Member Message Setup</div>
				</div>
				<ol class="breadcrumb page-breadcrumb pull-right">
					<li><a class="parent-item"
							href="/home"><i class="fa fa-home"></i></a>&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li>Settings&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li class="active">Member Message Setup</li>
				</ol>
			</div>
		</div>
		<input type="hidden" name="" value="/admin/membermessage" class="activeurl">
		<div class="row">
			<div class="col-md-12">
				<div class="card card-topline-green">
					<div class="card-head">
						<header>Member Detail</header>
						<div class="tools">
							<a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
							<a class="t-collapse btn-color fa fa-chevron-down"
								href="javascript:;"></a>
						</div>
					</div>
					<div class="card-body " id="savetoimage">
						
							@if (session('msg'))
		                        <div class="tstSuccess msg" style="display: none;">
		                            {{ session('msg') }}
		                        </div>
                    		@endif
                    		@if (session('error'))
		                        <div class="tstError error" style="display: none;">
		                            {{ session('error') }}
		                        </div>
                    		@endif
						<div class="row col-md-12">
							 <!-- Button trigger modal -->
							<button type="button" class="btn btn-circle btn-success" data-toggle="modal" data-target="#addmember"><i class="fa fa-plus"></i> Add </button>
					
							<!-- Modal -->
							<div class="modal fade" id="addmember" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
							  <div class="modal-dialog modal-lg" role="document">
							  	<form action="{{ route('membermessage.store') }}" method="POST" enctype="multipart/form-data">
							  		@csrf
								   	<div class="modal-content card-box">
										<div class="modal-header">
											<header>Add Member Detail</header>
										</div>
								   		<div class="row">
											<div class="card-body row form-body">
												<div class="col-lg-3 p-t-20">
													<div class="form-label-group">
														<label class="control-label" for="name">Name
														</label>
														<input type="text" name="name" id="name" class="form-control" placeholder="Enter Member Name" required="">
													</div>
												</div>
												<div class="col-lg-3 p-t-20">
													<div class="form-label-group">
														<label class="control-label" for="position">Position
														</label>
														<input type="text" name="position" id="position" class="form-control" placeholder="Enter Member Position" required="">
													</div>
												</div>
												<div class="col-lg-3 p-t-20"> 
													<div class="form-label-group">
														<label class="control-label" for="image">Member Image
														</label>
														<input type="file" name="image" id="image" class="image form-control" required="">
													</div>
												</div>
												<div class="col-lg-3 p-t-20"> 
													<div class="form-label-group">
														<img src="" class="imgs" height="100" width="150">
													</div>
												</div>
												<div class="col-lg-12 p-t-20">
													<div class="form-label-group">
														<label class="control-label" for="about">About Member
														</label>
														<textarea name="about" id="about" class="form-control" placeholder="About Member"></textarea>
													</div>
												</div>
												<div class="col-lg-12 p-t-20">
													<div class="form-label-group">
														<label class="control-label" for="message">Member Message
														</label>
														<textarea name="message" id="message" class="form-control" placeholder="Member Message"></textarea>
													</div>
												</div>
												<div class="col-lg-12 p-t-20">
													<div class="form-label-group">
														<input type="checkbox" name="backgroundimage" id="backgroundimage" style="height: 20px; width: 20px;"/>
														<label class="control-label" for="backgroundimage">Is Background Image
														</label>
													</div>
												</div>
											</div>
											<div class="col-lg-12 p-t-20 p-b-20 text-center">
												<button type="button" class="btn btn-circle btn-secondary" data-dismiss="modal">Close</button>
									    		<button type="submit" class="btn btn-circle btn-success save">Save changes</button>
											</div>
										</div>
									</div>
								</form>
							  </div>
							</div>
							<!-- Modal Ends -->
						</div>
						<div class="table-scrollable ">
							<table class="table display" width="100%">
								<thead class="text-center">
									<tr>
										<th>S.N</th>
										<th>Name</th>
										<th>Position</th>
										<th>About</th>
										<th>Message</th>
										<th>Image</th>
										<th>Background Image</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									@php
									$i=1;
									@endphp
									@foreach($members as $member)
									<tr class="text-center">
										<td>{{ $i++ }}</td>
										<td>{{ $member->name }}</td>
										<td>{{ $member->position }}</td>
										<td>{!! $member->about !!}</td>
										<td>{!! $member->message !!}</td>
										<td><object data="{{'data:image/jpg;base64,'.$member->image}}" style="height:80px;width:100px"></object></td>
										@if($member->is_backgroundimage == "on")
										<td>Yes</td>
										@else
										<td>No</td>
										@endif
										<td class="text-left">
											<!-- Button trigger modal -->
											<a href="" data-toggle="modal" data-target="#edit{{$member->id}}">
												<i class="material-icons text-warning">edit</i>
											</a>
											<!-- Modal -->
											<div class="modal fade" id="edit{{ $member->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
											  <div class="modal-dialog modal-lg" role="document">
											  	<form action="{{ url('admin/membermessage/'.$member->id) }}" method="POST" enctype="multipart/form-data">
											  		@csrf
											  		@method('put')
												   	<div class="modal-content card-box">
														<div class="modal-header">
															<header>Edit Member Detail</header>
														</div>
												   		<div class="row">
															<div class="card-body row form-body">
																<div class="col-lg-3 p-t-20">
																	<div class="form-label-group">
																		<label class="control-label" for="name">Name
																		</label>
																		<input type="text" name="name" id="name" class="form-control" placeholder="Enter Member Name" required="" value="{{ $member->name }}">
																	</div>
																</div>
																<div class="col-lg-3 p-t-20">
																	<div class="form-label-group">
																		<label class="control-label" for="position">Position
																		</label>
																		<input type="text" name="position" id="position" class="form-control" placeholder="Enter Member Position" required="" value="{{ $member->position }}">
																	</div>
																</div>
																<div class="col-lg-3 p-t-20"> 
																	<div class="form-label-group">
																		<label class="control-label" for="image">Member Image
																		</label>
																		<input type="file" name="image" id="image" class="image form-control">
																	</div>
																</div>
																<div class="col-lg-3 p-t-20"> 
																	<div class="form-label-group">
																		<img src="{{'data:image/jpg;base64,'.$member->image}}" class="imgs" height="100" width="150">
																	</div>
																</div>
																<div class="col-lg-12 p-t-20">
																	<div class="form-label-group">
																		<label class="control-label" for="about">About Member
																		</label>
																		<textarea name="about" id="about" class="form-control" placeholder="About Member">{{ $member->about }}</textarea>
																	</div>
																</div>
																<div class="col-lg-12 p-t-20">
																	<div class="form-label-group">
																		<label class="control-label" for="message">Member Message
																		</label>
																		<textarea name="message" id="message" class="form-control" placeholder="Member Message">{{ $member->message }}</textarea>
																	</div>
																</div>
																<div class="col-lg-12 p-t-20">
																	<div class="form-label-group">
																		@if($member->is_backgroundimage == "on")
																		<input type="checkbox" checked="" name="backgroundimage" id="backgroundimage" style="height: 20px; width: 20px;"/>
																		@else
																		<input type="checkbox" name="backgroundimage" id="backgroundimage" style="height: 20px; width: 20px;"/>

																		@endif
																		<label class="control-label" for="backgroundimage">Is Background Image
																		</label>
																	</div>
																</div>
															</div>
															<div class="col-lg-12 p-t-20 p-b-20 text-center">
																<button type="button" class="btn btn-circle btn-secondary" data-dismiss="modal">Close</button>
													    		<button type="submit" class="btn btn-circle btn-success save">Update</button>
															</div>
														</div>
													</div>
												</form>
											  </div>
											</div>
											<!-- Modal Ends -->
											<a href="" data-toggle="modal" data-target="#delete{{$member->id}}">
												<i class="material-icons text-danger">delete</i>
											</a>
											<!-- Modal -->
											<div class="modal fade" id="delete{{ $member->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
											  <div class="modal-dialog modal-dialog-centered" role="document">
											  	<form action="{{ url('admin/membermessage/'.$member->id) }}" method="POST">
											  		@csrf
											  		@method('delete')
												   	<div class=" modal-content card-box">
												   		<div class="row">
														<div class="col-sm-12">
														<div class="card-head">
															<header>Delete Member Detail</header>
														</div>
														<div class="card-body row ml-5">
															<h4 class="text-white">Do you want to delete {{$member->name}}??</h4>
															<h4 class="text-white">Can't Revert it!!!</h4>
														</div>
														<div class="col-lg-12 p-t-20 p-b-20 text-center">
															<button type="button" class="btn btn-circle btn-secondary" data-dismiss="modal">Close</button>
												    		<button type="submit" class="btn btn-circle btn-danger">Delete</button>
														</div>
														</div>
														</div>
													</div>
												</form>
											  </div>
											</div>
											<!-- Modal Ends -->
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- end page content -->
@endsection
@section('script')
<script type="text/javascript">
	function preview(input){
		if(input.files && input.files[0]){
			var reader = new FileReader();
			reader.onload = function(e){
				$('.imgs').attr('src', e.target.result);
			}
			reader.readAsDataURL(input.files[0]);
			}
	}
	$('.image').change(function(){
		preview(this);
	})
</script>
@endsection