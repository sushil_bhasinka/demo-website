@extends('layouts.adminlayouts.app')
@section('content')
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Smart</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
                            href="index-2.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Demo</li>
                </ol>
            </div>
        </div>
        <div class="row col-md-10">
            <div class="card">
                <div class="card-header">Send SMS</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if (session('msg'))
                        <div class="alert alert-success" role="alert">
                            <pre>{{ session('msg') }}</pre>
                        </div>
                    @endif
                    <form action="{{url('/sendSMS')}}" method="POST">
                        <center>
                        <div class="row">
                            <div class="col-sm-12">
                                <label for="input-number">Sent To:</label>
                                <input type="text" name="number" id="input-number" class="form-control" placeholder="Enter Phone Number">
                            </div>
                        </div>
                        <div class="row">
                            
                            <div class="col-sm-12">
                                <label for="input-message">Message:</label>
                                <textarea class="form-control" placeholder="write some message" name="message"></textarea>
                            </div>
                        </div><br> 
                        @csrf
                        <input type="submit" name="send" value="Send" class="btn btn-sm btn-success">
                       </center>
                    </form><br>
                    <center>
                    <form action="{{ url('/getBalance') }}" method="POST">
                       @csrf
                        <input type="submit" name="getBalance" value="Get Balance" class="btn btn-sm btn-primary"> 
                    </form>
                    </center>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
