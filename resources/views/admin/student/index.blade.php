@extends('layouts.adminlayouts.app')
@section('content')
<!-- start page content -->
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<div class="page-title-breadcrumb">
				<div class=" pull-left">
					<div class="page-title">New Student</div>
				</div>
				<ol class="breadcrumb page-breadcrumb pull-right">
					<li><a class="parent-item"
							href="/home"><i class="fa fa-home"></i></a>&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li>Settings&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li class="active">New Student</li>
				</ol>
			</div>
		</div>
		<input type="hidden" name="" value="/admin/student" class="activeurl">
		<div class="row">
			<div class="col-md-12">
				<div class="card card-topline-green">
					<div class="card-head">
						<header>New Student</header>
						<div class="tools">
							<a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
							<a class="t-collapse btn-color fa fa-chevron-down"
								href="javascript:;"></a>
						</div>
					</div>
					<div class="card-body " id="savetoimage">
						
							@if (session('msg'))
		                        <div class="tstSuccess msg" style="display: none;">
		                            {{ session('msg') }}
		                        </div>
                    		@endif
                    		@if ($errors->any())
							    <div class="tstError error" style="display: none;">
							            @foreach ($errors->all() as $error)
							                {{ $error }}
							            @endforeach
							    </div>
							@endif
						<div class="row col-md-12">
							<button class="btn-success btn-circle"> <a class="text-white" href="{{ route('student.create') }}"><i class="fa fa-plus"></i> Add</a></button>
						</div>
						<div class="iq-card-body">
	                        <div class="table-responsive">
	                          <!--  <div class="row justify-content-between">
	                              <div class="col-sm-12 col-md-6">
	                                 <div id="user_list_datatable_info" class="dataTables_filter">
	                                    <form class="mr-3 position-relative">
	                                       <div class="form-group mb-0">
	                                          <input type="search" class="form-control" id="exampleInputSearch" placeholder="Search" aria-controls="user-list-table">
	                                       </div>
	                                    </form>
	                                 </div>
	                              </div>
	                              <div class="col-sm-12 col-md-6">
	                                 <div class="user-list-files d-flex float-right">
	                                    <a href="javascript:void();" class="chat-icon-phone">
	                                       Print
	                                     </a>
	                                    <a href="javascript:void();" class="chat-icon-video">
	                                       Excel
	                                     </a>
	                                     <a href="javascript:void();" class="chat-icon-delete">
	                                       Pdf
	                                     </a>
	                                   </div>
	                              </div>
	                           </div> -->
	                           <table class="table table-striped table-bordered mt-4" role="grid">
	                             <thead>
	                                 <tr class="text-center">
	                                    <th>Image</th>
	                                    <th>Name</th>
	                                    <th>Class</th>
	                                    <th>Section</th>
	                                    <th>Roll no.</th>
	                                    <th>Address</th>
	                                    <th>Action</th>
	                                 </tr>
	                             </thead>
	                           	 <tbody>
	                           	 	@foreach($students as $student)
	                           	 	<tr class="text-center">
	                           	 		<td><img class="rounded-circle img-fluid" src="{{'data:image/jpg;base64,'.$student->image}}" height="60px" width="60px"  alt="profile"></td>
	                           	 		<td class="text-left">{{ $student->firstname }} {{ $student->lastname }}</td>
	                           	 		<td>{{ $student->classname }}</td>
	                           	 		<td>{{ $student->sectionname }}</td>
	                           	 		<td>{{ $student->studentrollno }}</td>
	                           	 		<td class="text-left">{{ $student->studentaddress }}</td>
	                           	 		<td><a href="{{ url('/admin/student/'.$student->studentid.'/edit') }}"><i class="material-icons text-warning">edit</i></a> 
                           	 			<a href="" data-toggle="modal" data-target="#delete{{$student->studentid}}">
											<i class="material-icons text-danger">delete</i>
										</a>
										<!-- Modal -->
										<div class="modal fade" id="delete{{ $student->studentid }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
										  <div class="modal-dialog modal-dialog-centered" role="document">
										  	<form action="{{ url('admin/student/'.$student->studentid) }}" method="POST">
										  		@csrf
										  		@method('delete')
											   	<div class=" modal-content card-box">
											   		<div class="row">
													<div class="col-sm-12">
													<div class="card-head">
														<header>Delete Student</header>
													</div>
													<div class="card-body row ml-5">
														<h4 class="text-white">Do you want to delete {{$student->firstname}} {{$student->lastname}}??</h4>
														<h4 class="text-white">Can't Revert it!!!</h4>
													</div>
													<div class="col-lg-12 p-t-20 p-b-20 text-center">
														<button type="button" class="btn btn-circle btn-secondary" data-dismiss="modal">Close</button>
											    		<button type="submit" class="btn btn-circle btn-danger">Delete</button>
													</div>
													</div>
													</div>
												</div>
											</form>
										  </div>
										</div>
										<!-- Modal Ends -->
	                           	 	</tr>
	                           	 	@endforeach
	                           	 </tbody>
	                           </table>
	                        </div>
	                           <div class="row justify-content-end mt-3">
	                              {{ $students->links() }}
	                           </div>
	                     </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- end page content -->
@endsection
@section('script')

@endsection