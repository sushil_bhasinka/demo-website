@extends('layouts.adminlayouts.app')
@section('content')
<div class="page-content-wrapper">
   <div class="page-content">
      <div class="page-bar">
         <div class="page-title-breadcrumb">
            <div class=" pull-left">
               <div class="page-title">Edit Student</div>
            </div>
            <ol class="breadcrumb page-breadcrumb pull-right">
               <li><a class="parent-item"
                     href="/home"><i class="fa fa-home"></i></a>&nbsp;<i class="fa fa-angle-right"></i>
               </li>
               <li>Settings&nbsp;<i class="fa fa-angle-right"></i>
               </li>
               <li class="active">Edit Student</li>
            </ol>
         </div>
      </div>
      <input type="hidden" name="" value="/admin/student" class="activeurl">
      <div class="row">
         <form action="{{ url('admin/student/'.$student->studentid) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('put')
            <div class="pull-left col-lg-4">
                  <div class="card">
                     <div class="card-header d-flex justify-content-between">
                        <div class="header-title">
                           <h4 class="card-title">Student Information</h4>
                        </div>
                     </div>
                     <div class="card-body">
                           <div class="form-group">
                              <div class="add-img-student">
                                 <img src="{{'data:image/jpg;base64,'.$student->image}}" class="imgs" height="180" width="250">
                                 <div class="p-image">
                                   <input type="file" name="image" id="image" class="image form-control">
                                </div>
                              </div>
                             <div class="img-extension mt-3">
                                <div class="d-inline-block align-items-center text-muted">
                                    <span>Only</span>
                                    <span>.jpg</span>
                                    <span>.png</span>
                                    <span>.jpeg</span>
                                   <span>allowed</span>
                                </div>
                             </div>
                           </div>
                           <div class="form-group">
                              <label for="fname">First Name:</label>
                              <input type="text" name="fname" class="form-control" id="fname" placeholder="First Name" value="{{ $student->firstname }}">
                           </div>
                           <div class="form-group">
                              <label for="lname">Last Name:</label>
                              <input type="text" name="lname" class="form-control" id="lname" placeholder="Last Name" value="{{ $student->lastname }}">
                           </div>
                           <div class="form-group">
                              <label>Class :</label>
                              <select name="classid" class="form-control classid" id="classid">
                                 <option disabled selected>Choose Class............</option>
                                 @foreach($classes as $class)
                                 @if($class->classid == $student->studentclass)
                                 <option value="{{ $class->classid }}" selected="">{{ $class->classname }}</option>
                                 @else
                                 <option value="{{ $class->classid }}">{{ $class->classname }}</option>
                                 @endif
                                 @endforeach
                              </select>
                           </div>
                           <div class="form-group">
                              <label for="sectionid">Section:</label>
                              <select name="sectionid" class="form-control sectionid" id="sectionid">
                                 <option disabled selected>Choose Section..........</option>
                                 @foreach($sections as $section)
                                 @if($section->sectionid == $student->studentsection)
                                 <option value="{{ $section->sectionid }}" selected="">{{ $section->sectionname }}</option>
                                 @endif
                                 @endforeach
                              </select>
                           </div>
                           <div class="form-group">
                              <label for="rollno">Roll no. :</label>
                              <input type="text" name="rollno" class="form-control" id="rollno" placeholder="Roll Number" value="{{ $student->studentrollno }}">
                           </div> 
                     </div>
                  </div>
            </div>
            <div class="col-lg-8 pull-right">
                  <div class="card">
                     <div class="card-header d-flex justify-content-between">
                        <div class="header-title">
                           <h4 class="card-title">Parent Information</h4>
                        </div>
                     </div>
                     <div class="card-body">
                        <div class="Edit-student-info">
                              <div class="row">
                                 <div class="form-group col-md-6">
                                    <label for="fathername">Father's Name:</label>
                                    <input type="text" name="fathername" class="form-control" id="fathername" placeholder="Father's Name" value="{{ $student->fathername }}">
                                 </div>
                                 <div class="form-group col-md-6">
                                    <label for="fjob">Father's Occupation:</label>
                                    <input type="text" name="fjob" class="form-control" id="fjob" placeholder="Father's Occupation" value="{{ $student->fatheroccupation }}">
                                 </div>
                                 <div class="form-group col-md-6">
                                    <label for="mothername">Mother's Name:</label>
                                    <input type="text" name="mothername" class="form-control" id="mothername" placeholder="Mother's Name" value="{{ $student->mothername }}">
                                 </div>
                                 <div class="form-group col-md-6">
                                    <label for="mjob">Mother's Occupation:</label>
                                    <input type="text" name="mjob" class="form-control" id="mjob" placeholder="Mother's Occupation" value="{{ $student->motheroccupation }}">
                                 </div>
                                 <div class="form-group col-md-6">
                                    <label for="add">Address :</label>
                                    <input type="text" name="address" class="form-control" id="add" placeholder="Address" value="{{ $student->studentaddress }}">
                                 </div>
                                 <div class="form-group col-md-6">
                                    <label for="mobno">Mobile Number:</label>
                                    <input type="text" name="mobileno" class="form-control" id="mobno" placeholder="Mobile Number" value="{{ $student->mobileno }}">
                                 </div>
                                 <div class="form-group col-md-6">
                                    <label for="altconno">Alternate Contact:</label>
                                    <input type="text" name="altmobileno" class="form-control" id="altconno" placeholder="Alternate Contact" value="{{ $student->alternativemobileno }}">
                                 </div>
                                 <div class="form-group col-md-6">
                                    <label for="email">Email:</label>
                                    <input type="email" name="email" class="form-control" id="email" placeholder="Email" value="{{ $student->parentemail }}">
                                 </div>
                              </div>
                              <hr>
                              <h5 class="mb-3">Login Security</h5>
                              <div class="row">
                                 @foreach($user as $usr)
                                 <div class="form-group col-md-6">
                                    <label for="username">Username:</label>
                                    <input type="text" name="username" class="form-control" id="username" placeholder="Login Username" value="{{ $usr->name }}">
                                 </div>
                                 <div class="form-group col-md-6">
                                    <label for="emailid">Email:</label>
                                    <input type="email" name="emailid" class="form-control" id="emailid" placeholder="Login Email ID" value="{{ $usr->email }}">
                                 </div>
                                 <div class="form-group col-md-6">
                                    <label for="pass">Password:</label>
                                    <input type="password" name="password" class="form-control password" id="pass" placeholder="Password">
                                 </div>
                                 <div class="form-group col-md-6">
                                    <label for="cpass">Confirmed Password:</label>
                                    <input type="password" name="cpass" class="form-control confirmpassword" id="cpass" placeholder="Confirmed Password ">
                                    <div class="confirmpassword_error text-danger"></div>
                                    <div class="confirmpassword_success text-success"></div>
                                 </div>
                                 @endforeach
                              </div>
                              <button type="submit" class="btn btn-success btn-circle save">Save</button>
                        </div>
                     </div>
                  </div>
            </div>
         </form>
      </div>
   </div>
</div>

@endsection
@section('script')
<script type="text/javascript">
   function preview(input){
      if(input.files && input.files[0]){
         var reader = new FileReader();
         reader.onload = function(e){
            $('.imgs').attr('src', e.target.result);
         }
         reader.readAsDataURL(input.files[0]);
         }
   }
   $('.image').change(function(){
      preview(this);
   })
</script>
<script type="text/javascript">
   $(document).ready(function(){
      $(document).on('change','.classid', function(){
         var classid=$(this).val();
         $('.sectionid').html('');
         var op="";
         $.ajax({
             type:'get',
             url:'{!!URL::to('/admin/getsection')!!}',
             data:{'classid':classid},
             success:function(data){
               console.log(data);
               op+='<option disabled selected> Choose Section.....</option>';
                for(var i=0;i<data.length;i++){
                op+='<option value="'+data[i].sectionid+'">'+data[i].sectionname+'</option>';
                 }

                 $('.sectionid').html(" ");
                 $('.sectionid').html(op);
               }
           })
      })
   })
</script>

@endsection