@extends('layouts.adminlayouts.app')
@section('content')
<!-- start page content -->
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<div class="page-title-breadcrumb">
				<div class=" pull-left">
					<div class="page-title">Social Media Icon</div>
				</div>
				<ol class="breadcrumb page-breadcrumb pull-right">
					<li><a class="parent-item"
							href="/home"><i class="fa fa-home"></i></a>&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li>Settings&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li class="active">Social Media Icon</li>
				</ol>
			</div>
		</div>
		<input type="hidden" name="" value="/admin/socialmediaicon" class="activeurl">
		<div class="row">
			<div class="col-md-12">
				<div class="card card-topline-green">
					<div class="card-head">
						<header>Social Media Icon</header>
						<div class="tools">
							<a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
							<a class="t-collapse btn-color fa fa-chevron-down"
								href="javascript:;"></a>
						</div>
					</div>
					<div class="card-body " id="savetoimage">
						
							@if (session('msg'))
		                        <div class="tstSuccess msg" style="display: none;">
		                            {{ session('msg') }}
		                        </div>
                    		@endif
                    		@if (session('error'))
		                        <div class="tstError error" style="display: none;">
		                            {{ session('error') }}
		                        </div>
                    		@endif
						<div class="row col-md-12">
							 <!-- Button trigger modal -->
							<button type="button" class="btn btn-circle btn-success" data-toggle="modal" data-target="#addicon"><i class="fa fa-plus"></i> Add </button>
							
							<!-- Modal -->
							<div class="modal fade" id="addicon" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
							  <div class="modal-dialog modal-dialog-centered" role="document">
							  	<form action="{{ route('socialmediaicon.store') }}" method="POST" enctype="multipart/form-data">
							  		@csrf
								   	<div class="modal-content card-box">
										<div class="modal-header">
											<header>Add Social Media Icon</header>
										</div>
								   		<div class="row">
										<div class="card-body row form-body">

											<div class="col-lg-12 p-t-20">
												<label class="control-label" for="medianame">Social Media Name</label>
												<select class="form-control" name="iconid" id="medianame">
													<option selected="" disabled="">Select Social Media Name</option>
													@foreach($icons as $icon)
														<option value='{{ $icon->id }}'>{{ $icon->iconname }}</option>
													@endforeach
												</select>
											</div>
											<div class="col-lg-12 p-t-20">
												<div class="form-label-group">
													<label class="control-label" for="url">Social Media Address
													</label>
													<input type="text" name="url" id="url" placeholder="Enter Social Media Address" class="form-control" required="" />
												</div>
											</div>
										</div>
										<div class="col-lg-12 p-t-20 p-b-20 text-center">
											<button type="button" class="btn btn-circle btn-secondary" data-dismiss="modal">Close</button>
								    		<button type="submit" class="btn btn-circle btn-success">Save changes</button>
										</div>
										</div>
									</div>
								</form>
							  </div>
							</div>
							<!-- Modal Ends -->
						</div>
						<div class="table-scrollable ">
							<table class="table display" width="100%">
								<thead class="text-center">
									<tr>
										<th>S.N</th>
										<th>Social Media Name</th>
										<th>Social Media Icon</th>
										<th>Social Media Address</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									@php
									$i=1;
									@endphp
									@foreach($socialmediaicons as $socialmediaicon)
									<tr class="text-center">
										<td>{{ $i++ }}</td>
										<td>{{ $socialmediaicon->iconname }}</td>
										<td>{!! $socialmediaicon->icon !!}</td>
										<td>{{ $socialmediaicon->url }}</td>
										<td class="text-left">
											<!-- Button trigger modal -->
											<a href="" data-toggle="modal" data-target="#edit{{$socialmediaicon->id}}">
												<i class="material-icons text-warning">edit</i>
											</a>
											<!-- Modal -->
											<div class="modal fade" id="edit{{ $socialmediaicon->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
											  <div class="modal-dialog modal-dialog-centered" role="document">
											  	<form action="{{ url('admin/socialmediaicon/'.$socialmediaicon->id) }}" method="POST" enctype="multipart/form-data">
											  		@csrf
											  		@method('put')
												   	<div class="modal-content card-box">
														<div class="modal-header">
															<header>Edit Social Media Icon</header>
														</div>
												   		<div class="row">
														<div class="card-body row form-body">
															<div class="col-lg-12 p-t-20">
																<label class="control-label" for="medianame">Social Media Name</label>
																<select class="form-control" name="iconid" id="medianame">
																	<option selected="" disabled="">Select Social Media Name</option>
																	@foreach($icons as $icon)
																	@if($icon->id == $socialmediaicon->iconid)
																		<option selected="" value='{{ $icon->id }}'>{{ $icon->iconname }}</option>
																	@else
																		<option value='{{ $icon->id }}'>{{ $icon->iconname }}</option>
																	@endif
																	@endforeach	
																</select>
															</div>
															<div class="col-lg-12 p-t-20">
																<div class="form-label-group">
																	<label class="control-label" for="url">Social Media Address
																	</label>
																	<input type="text" name="url" id="url" placeholder="Enter Link Url" class="form-control" value="{{ $socialmediaicon->url }}"/>
																</div>
															</div>
														</div>
														<div class="col-lg-12 p-t-20 p-b-20 text-center">
															<button type="button" class="btn btn-circle btn-secondary" data-dismiss="modal">Close</button>
												    		<button type="submit" class="btn btn-circle btn-success">Update</button>
														</div>
														</div>
													</div>
												</form>
											  </div>
											</div>
											<!-- Modal Ends -->
											<a href="" data-toggle="modal" data-target="#delete{{$socialmediaicon->id}}">
												<i class="material-icons text-danger">delete</i>
											</a>
											<!-- Modal -->
											<div class="modal fade" id="delete{{ $socialmediaicon->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
											  <div class="modal-dialog modal-dialog-centered" role="document">
											  	<form action="{{ url('admin/socialmediaicon/'.$socialmediaicon->id) }}" method="POST">
											  		@csrf
											  		@method('delete')
												   	<div class=" modal-content card-box">
												   		<div class="row">
														<div class="col-sm-12">
														<div class="card-head">
															<header>Delete Quick Link</header>
														</div>
														<div class="card-body row ml-5">
															<h4 class="text-white">Do you want to delete {{$socialmediaicon->url}}??</h4>
															<h4 class="text-white">Can't Revert it!!!</h4>
														</div>
														<div class="col-lg-12 p-t-20 p-b-20 text-center">
															<button type="button" class="btn btn-circle btn-secondary" data-dismiss="modal">Close</button>
												    		<button type="submit" class="btn btn-circle btn-danger">Delete</button>
														</div>
														</div>
														</div>
													</div>
												</form>
											  </div>
											</div>
											<!-- Modal Ends -->
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- end page content -->
@endsection
@section('script')

@endsection