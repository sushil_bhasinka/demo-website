@extends('layouts.adminlayouts.app')
@section('content')
<!-- start page content -->
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<div class="page-title-breadcrumb">
				<div class=" pull-left">
					<div class="page-title">Footer Contact Setup</div>
				</div>
				<ol class="breadcrumb page-breadcrumb pull-right">
					<li><a class="parent-item"
							href="/home"><i class="fa fa-home"></i></a>&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li>Settings&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li class="active">Footer Contact Setup</li>
				</ol>
			</div>
		</div>
		<input type="hidden" name="" value="/admin/footercontactsetup" class="activeurl">
		<div class="row">
			<div class="col-md-12">
				<div class="card card-topline-green">
					<div class="card-head">
						<header>Footer Contact Setup</header>
						<div class="tools">
							<a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
							<a class="t-collapse btn-color fa fa-chevron-down"
								href="javascript:;"></a>
						</div>
					</div>
					<div class="card-body " id="savetoimage">
						
							@if (session('msg'))
		                        <div class="tstSuccess msg" style="display: none;">
		                            {{ session('msg') }}
		                        </div>
                    		@endif
                    		@if (session('error'))
		                        <div class="tstError error" style="display: none;">
		                            {{ session('error') }}
		                        </div>
                    		@endif
						<div class="row col-md-12">
							 <!-- Button trigger modal -->
							@if(count($footercontactsetups) < 1)
								<button type="button" class="btn btn-circle btn-success" data-toggle="modal" data-target="#addfootercontactsetup"><i class="fa fa-plus"></i> Add </button>
							@endif
							<!-- Modal -->
							<div class="modal fade" id="addfootercontactsetup" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
							  <div class="modal-dialog modal-lg" role="document">
							  	<form action="{{ route('footercontactsetup.store') }}" method="POST" enctype="multipart/form-data">
							  		@csrf
								   	<div class="modal-content card-box">
										<div class="modal-header">
											<header>Add Footer Contact Setup</header>
										</div>
								   		<div class="row">
										<div class="card-body row form-body">
											<div class="col-lg-4 p-t-20">
												<label class="control-label" for="tole">Tole</label>
												<input type="text" name="tole" id="tole" placeholder="Enter Tole Name" class="form-control" />
											</div>
											<div class="col-lg-4 p-t-20">
												<div class="form-label-group">
													<label class="control-label" for="wardno">Ward No.
													</label>
													<input type="text" name="wardno" id="wardno" placeholder="Enter Ward No." class="form-control" pattern="-?[0-9]*(\.[0-9]+)?"/>
												</div>
											</div>
											<div class="col-lg-4 p-t-20">
												<label class="control-label" for="municipality">VDC/Municipality</label>
												<input type="text" name="municipality" id="municipality" placeholder="Enter VDC>Municipality Name" class="form-control" />
											</div>
											<div class="col-lg-4 p-t-20">
												<label class="control-label" for="district">District</label>
												<input type="text" name="district" id="district" placeholder="Enter District Name" class="form-control" />
											</div>
											<div class="col-lg-4 p-t-20">
												<div class="form-label-group">
													<label class="control-label" for="email">Email
													</label>
													<input type="email" name="email" id="email" placeholder="abc@____.com" class="form-control" />
												</div>
											</div>
											<div class="col-lg-4 p-t-20">
												<div class="form-label-group">
													<label class="control-label" for="phone">Phone no.
													</label>
													<input type="text" name="phone" id="phone" placeholder="Enter Phone number" class="form-control" pattern="-?[0-9]*(\.[0-9]+)?"/>
												</div>
											</div>
											
											<div class="col-lg-12 p-t-20">
												<div class="form-label-group">
													<label class="control-label" for="map">Google Map
													</label>
													<textarea name="map" id="map" class="form-control" placeholder="Google Map Iframe"></textarea>
												</div>
											</div>
											<div class="row col-lg-12 p-t-20">
													<div class="col-lg-8">
														<div class="form-label-group">
															<label class="control-label" for="image">Background Image
															</label>
															<input type="file" name="image" id="image" class="image form-control">
														</div>
													</div>
													<div class="col-lg-4"> 
														<div class="form-label-group">
															<img src="" class="imgs" height="100" width="250">
														</div>
													</div>
												</div>
										</div>
										<div class="col-lg-12 p-t-20 p-b-20 text-center">
											<button type="button" class="btn btn-circle btn-secondary" data-dismiss="modal">Close</button>
								    		<button type="submit" class="btn btn-circle btn-success">Save changes</button>
										</div>
										</div>
									</div>
								</form>
							  </div>
							</div>
							<!-- Modal Ends -->
						</div>
						<div class="table-scrollable ">
							<table class="table display" width="100%">
								<thead class="text-center">
									<tr>
										<th width="10%">Tole</th>
										<th width="5%">Ward No.</th>
										<th width="10%">VDC/Municipality</th>
										<th width="10%">District</th>
										<th width="10%">Email</th>
										<th width="10%">Phone</th>
										<th width="30%">Google Map Iframe</th>
										<th width="10%">Background Image</th>
										<th width="5%">Action</th>
									</tr>
								</thead>
								<tbody>
									@foreach($footercontactsetups as $footercontactsetup)
									<tr class="text-center">
										<td>{{ $footercontactsetup->tole }}</td>
										<td>{{ $footercontactsetup->wardno }}</td>
										<td>{{ $footercontactsetup->municipality }}</td>
										<td>{{ $footercontactsetup->district }}</td>
										<td>{{ $footercontactsetup->email }}</td>
										<td>{{ $footercontactsetup->phone }}</td>
										<td>{!! $footercontactsetup->map !!}</td>
										<td><img src="{{'data:image/jpg;base64,'.$footercontactsetup->image}}" height="100" width="250"></td>
										<td class="text-left">
											<!-- Button trigger modal -->
											<a href="" data-toggle="modal" data-target="#edit{{$footercontactsetup->footercontactsetupid}}">
												<i class="material-icons text-warning">edit</i>
											</a>
											<!-- Modal -->
											<div class="modal fade" id="edit{{ $footercontactsetup->footercontactsetupid }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
											  <div class="modal-dialog modal-lg" role="document">
											  	<form action="{{ url('admin/footercontactsetup/'.$footercontactsetup->footercontactsetupid) }}" method="POST" enctype="multipart/form-data">
											  		@csrf
											  		@method('put')
												   	<div class="modal-content card-box">
														<div class="modal-header">
															<header>Edit Footer Contact Setup</header>
														</div>
												   		<div class="row">
														<div class="card-body row form-body">
															<div class="col-lg-4 p-t-20">
																<label class="control-label" for="tole">Tole</label>
																<input type="text" name="tole" id="tole" placeholder="Enter Tole Name" class="form-control" value="{{ $footercontactsetup->tole }}" />
															</div>
															<div class="col-lg-4 p-t-20">
																<div class="form-label-group">
																	<label class="control-label" for="wardno">Ward No.
																	</label>
																	<input type="text" name="wardno" id="wardno" placeholder="Enter Ward No." class="form-control" pattern="-?[0-9]*(\.[0-9]+)?" value="{{ $footercontactsetup->wardno }}"/>
																</div>
															</div>
															<div class="col-lg-4 p-t-20">
																<label class="control-label" for="municipality">VDC/Municipality</label>
																<input type="text" name="municipality" id="municipality" placeholder="Enter VDC>Municipality Name" class="form-control" value="{{ $footercontactsetup->municipality }}"/>
															</div>
															<div class="col-lg-4 p-t-20">
																<label class="control-label" for="district">District</label>
																<input type="text" name="district" id="district" placeholder="Enter District Name" class="form-control" value="{{ $footercontactsetup->district }}"/>
															</div>
															<div class="col-lg-4 p-t-20">
																<div class="form-label-group">
																	<label class="control-label" for="email">Email
																	</label>
																	<input type="email" name="email" id="email" placeholder="abc@____.com" class="form-control" value="{{ $footercontactsetup->email }}"/>
																</div>
															</div>
															<div class="col-lg-4 p-t-20">
																<div class="form-label-group">
																	<label class="control-label" for="phone">Phone no.
																	</label>
																	<input type="text" name="phone" id="phone" placeholder="Enter Phone number" class="form-control" pattern="-?[0-9]*(\.[0-9]+)?" value="{{ $footercontactsetup->phone }}"/>
																</div>
															</div>
															
															<div class="col-lg-12 p-t-20">
																<div class="form-label-group">
																	<label class="control-label" for="map">Google Map
																	</label>
																	<textarea name="map" id="map" class="form-control" placeholder="Google Map Iframe" rows="5">{{$footercontactsetup->map}}</textarea>
																</div>
															</div>
															<div class="row col-lg-12 p-t-20">
																<div class="col-lg-4">
																	<div class="form-label-group">
																		<label class="control-label" for="image">Background Image
																		</label>
																		<input type="file" name="image" id="image" class="image form-control">
																	</div>
																</div>
																<div class="col-lg-4"> 
																	<div class="form-label-group">
																		<img src="{{'data:image/jpg;base64,'.$footercontactsetup->image}}" class="imgs" height="100" width="250">
																	</div>
																</div>
															</div>
														</div>
														<div class="col-lg-12 p-t-20 p-b-20 text-center">
															<button type="button" class="btn btn-circle btn-secondary" data-dismiss="modal">Close</button>
												    		<button type="submit" class="btn btn-circle btn-success">Update</button>
														</div>
														</div>
													</div>
												</form>
											  </div>
											</div>
											<!-- Modal Ends -->
											<a href="" data-toggle="modal" data-target="#delete{{$footercontactsetup->footercontactsetupid}}">
												<i class="material-icons text-danger">delete</i>
											</a>
											<!-- Modal -->
											<div class="modal fade" id="delete{{ $footercontactsetup->footercontactsetupid }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
											  <div class="modal-dialog modal-dialog-centered" role="document">
											  	<form action="{{ url('admin/footercontactsetup/'.$footercontactsetup->footercontactsetupid) }}" method="POST">
											  		@csrf
											  		@method('delete')
												   	<div class=" modal-content card-box">
												   		<div class="row">
														<div class="col-sm-12">
														<div class="card-head">
															<header>Delete Module</header>
														</div>
														<div class="card-body row ml-5">
															<h4 class="text-white">Do you want to delete {{$footercontactsetup->tole}}??</h4>
															<h4 class="text-white">Can't Revert it!!!</h4>
														</div>
														<div class="col-lg-12 p-t-20 p-b-20 text-center">
															<button type="button" class="btn btn-circle btn-secondary" data-dismiss="modal">Close</button>
												    		<button type="submit" class="btn btn-circle btn-danger">Delete</button>
														</div>
														</div>
														</div>
													</div>
												</form>
											  </div>
											</div>
											<!-- Modal Ends -->
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- end page content -->
@endsection
@section('script')
<script type="text/javascript">
	function preview(input){
		if(input.files && input.files[0]){
			var reader = new FileReader();
			reader.onload = function(e){
				$('.imgs').attr('src', e.target.result);
			}
			reader.readAsDataURL(input.files[0]);
			}
	}
	$('.image').change(function(){
		preview(this);
	})
</script>
@endsection