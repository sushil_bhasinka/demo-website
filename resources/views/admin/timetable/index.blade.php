@extends('layouts.adminlayouts.app')
@section('content')
<!-- start page content -->
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<div class="page-title-breadcrumb">
				<div class=" pull-left">
					<div class="page-title">Time Table Setup</div>
				</div>
				<ol class="breadcrumb page-breadcrumb pull-right">
					<li><a class="parent-item"
							href="/home"><i class="fa fa-home"></i></a>&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li>Front End Setup&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li class="active">Time Table Setup</li>
				</ol>
			</div>
		</div>
		<input type="hidden" name="" value="/admin/timetable" class="activeurl">
		<div class="row">
			<div class="col-md-12">
				<div class="card card-topline-green">
					<div class="card-head">
						<header>Time Table</header>
						<div class="tools">
							<a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
							<a class="t-collapse btn-color fa fa-chevron-down"
								href="javascript:;"></a>
						</div>
					</div>
					<div class="card-body " id="savetoimage">
						
							@if (session('msg'))
		                        <div class="tstSuccess msg" style="display: none;">
		                            {{ session('msg') }}
		                        </div>
                    		@endif
                    		@if (session('error'))
		                        <div class="tstError error" style="display: none;">
		                            {{ session('error') }}
		                        </div>
                    		@endif
						<div class="row col-md-12">
							 <!-- Button trigger modal -->
							<button type="button" class="btn btn-circle btn-success" data-toggle="modal" data-target="#addtimetable"><i class="fa fa-plus"></i> Add </button>
					
							<!-- Modal -->
							<div class="modal fade" id="addtimetable" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
							  <div class="modal-dialog modal-lg" role="document">
							  	<form action="{{ route('timetable.store') }}" method="POST" enctype="multipart/form-data">
							  		@csrf
								   	<div class="modal-content card-box">
										<div class="modal-header">
											<header>Add Time Table</header>
										</div>
								   		<div class="row">
											<div class="card-body row form-body">
												<div class="col-lg-4 p-t-20">
													<div class="form-label-group">
														<label class="control-label">Week Day
														</label>
														<select name="weekdayid" class="form-control">
															<option disabled selected>Choose Week Day</option>
															@foreach($weekdays as $days)
															<option value="{{ $days->id }}">{{ $days->day }}</option>
															@endforeach
														</select>
													</div>
												</div>
												<div class="col-lg-4 p-t-20">
													<div class="form-label-group">
														<label class="control-label">Start time
														</label>
														<div class="input-group mb-2">
															<input type="text" class="form-control starttimepicker" name="starttime" placeholder="Start Time">
													     </div>
													</div>
												</div>
												<div class="col-lg-4 p-t-20">
													<div class="form-label-group">
														<label class="control-label">End time
														</label>
														<div class="input-group mb-2">
															<input type="text" name="endtime" class="endtimepicker form-control" placeholder="End Time" >
													     </div>
													</div>
												</div>
												<div class="col-lg-4 p-t-20"> 
													<div class="form-label-group">
														<label class="control-label">Workout Category</label>
														<select class="form-control" name="workoutcategoryid">
															<option disabled="" selected="">Choose Workoit Category</option>
														@foreach($workoutcategories as $category)
															<option value="{{ $category->id }}">{{ $category->title }}</option>
														@endforeach
														</select>
													</div>
												</div>
												<div class="col-lg-4 p-t-20">
													<div class="form-label-group">
														<label class="control-label">Workout Title
														</label>
														<input type="text" name="title" class="form-control" placeholder="Enter  Workout Title">
													</div>
												</div>
												<div class="col-lg-4 p-t-20">
													<div class="form-label-group">
														<label class="control-label">Trainer
														</label>
														<input type="text" name="trainer" class="form-control" placeholder="Enter  Trainer">
													</div>
												</div>
												<div class="col-lg-8 p-t-20"> 
													<div class="form-label-group">
														<label class="control-label">Image
														</label>
														<input type="file" name="image" class="image form-control">
													</div>
												</div>
												<div class="col-lg-4 p-t-20"> 
													<div class="form-label-group">
														<img src="" class="imgs" height="100" width="150">
													</div>
												</div>
												<div class="col-lg-12 p-t-20">
													<div class="form-label-group">
														<input type="checkbox" name="backgroundimage" id="backgroundimage" style="height: 20px; width: 20px;"/>
														<label class="control-label" for="backgroundimage">Is Background Image
														</label>
													</div>
												</div>
											</div>
											<div class="col-lg-12 p-t-20 p-b-20 text-center">
												<button type="button" class="btn btn-circle btn-secondary" data-dismiss="modal">Close</button>
									    		<button type="submit" class="btn btn-circle btn-success save">Save changes</button>
											</div>
										</div>
									</div>
								</form>
							  </div>
							</div>
							<!-- Modal Ends -->
						</div>
						<div class="table-scrollable ">
							<table class="table display" width="100%">
								<thead class="text-center">
									<tr>
										<th>S.N</th>
										<th>Weekday</th>
										<th>Start Time</th>
										<th>End Time</th>
										<th>Workout Category</th>
										<th>Workout Title</th>
										<th>Trainer</th>
										<th>Image</th>
										<th>Background Image</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									@php
									$i=1;
									@endphp
									@foreach($timetables as $timetable)
									<tr class="text-center">
										<td>{{ $i++ }}</td>
										<td>{{ $timetable->day }}</td>
										<td>{{ $timetable->starttime }}</td>
										<td>{{ $timetable->endtime }}</td>
										<td>{{ $timetable->title }}</td>
										<td>{{ $timetable->workouttitle }}</td>
										<td>{{ $timetable->trainer }}</td>
										<td>
											<object data="{{'data:image/jpg;base64,'.$timetable->image}}" style="height:80px;width:100px"></object>
										</td>
										@if($timetable->is_backgroundimage == "on")
										<td>Yes</td>
										@else
										<td>No</td>
										@endif
										<td class="text-left">
											<!-- Button trigger modal -->
											<a href="" data-toggle="modal" data-target="#edit{{$timetable->id}}">
												<i class="material-icons text-warning">edit</i>
											</a>
											<!-- Modal -->
											<div class="modal fade" id="edit{{ $timetable->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
											  <div class="modal-dialog modal-lg" role="document">
											  	<form action="{{ url('admin/timetable/'.$timetable->id) }}" method="POST" enctype="multipart/form-data">
											  		@csrf
											  		@method('put')
												   	<div class="modal-content card-box">
														<div class="modal-header">
															<header>Edit Time Table</header>
														</div>
												   		<div class="row">
															<div class="card-body row form-body">
																<div class="col-lg-4 p-t-20">
																	<div class="form-label-group">
																		<label class="control-label">Week Day
																		</label>
																		<select name="weekdayid" class="form-control">
																			<option disabled selected>Choose Week Day</option>
																			@foreach($weekdays as $days)
																			@if($days->id == $timetable->weekdayid)
																			<option value="{{ $days->id }}" selected="">{{ $days->day }}</option>
																			@else
																			<option value="{{ $days->id }}">{{ $days->day }}</option>
																			@endif
																			@endforeach
																		</select>
																	</div>
																</div>
																<div class="col-lg-4 p-t-20">
																	<div class="form-label-group">
																		<label class="control-label">Start time
																		</label>
																		<div class="input-group mb-2">
																			<input type="text" class="form-control starttimepicker" name="starttime" value="{{ $timetable->starttime }}" placeholder="Start Time">
																	     </div>
																	</div>
																</div>
																<div class="col-lg-4 p-t-20">
																	<div class="form-label-group">
																		<label class="control-label">End time
																		</label>
																		<div class="input-group mb-2">
																			<input type="text" name="endtime" class="endtimepicker form-control"  value="{{ $timetable->endtime }}"placeholder="End Time" >
																	     </div>
																	</div>
																</div>
																<div class="col-lg-4 p-t-20"> 
																	<div class="form-label-group">
																		<label class="control-label">Workout Category</label>
																		<select class="form-control" name="workoutcategoryid">					
																			@foreach($workoutcategories as $category)
																			@if($category->id == $timetable->workoutcategoryid)
																				<option value="{{ $category->id }}" selected="">{{ $category->title }}</option>
																			@else
																				<option value="{{ $category->id }}">{{ $category->title }}</option>
																			@endif
																			@endforeach
																		</select>
																	</div>
																</div>
																<div class="col-lg-4 p-t-20">
																	<div class="form-label-group">
																		<label class="control-label">Workout Title
																		</label>
																		<input type="text" name="title" class="form-control" value="{{ $timetable->workouttitle }}" placeholder="Enter  Workout Title">
																	</div>
																</div>
																<div class="col-lg-4 p-t-20">
																	<div class="form-label-group">
																		<label class="control-label">Trainer
																		</label>
																		<input type="text" name="trainer" class="form-control" value="{{ $timetable->trainer }}" placeholder="Enter  Trainer">
																	</div>
																</div>
																<div class="col-lg-8 p-t-20"> 
																	<div class="form-label-group">
																		<label class="control-label">Image
																		</label>
																		<input type="file" name="image" class="image form-control">
																	</div>
																</div>
																<div class="col-lg-4 p-t-20"> 
																	<div class="form-label-group">
																		<img src="{{'data:image/jpg;base64,'.$timetable->image}}" class="imgs" height="100" width="150">
																	</div>
																</div>
																<div class="col-lg-12 p-t-20">
																	<div class="form-label-group">
																		@if($timetable->is_backgroundimage == "on")
																		<input type="checkbox" checked="" name="backgroundimage"  style="height: 20px; width: 20px;"/>
																		@else
																		<input type="checkbox" name="backgroundimage"  style="height: 20px; width: 20px;"/>

																		@endif
																		<label class="control-label">Is Background Image
																		</label>
																	</div>
																</div>
															</div>
															<div class="col-lg-12 p-t-20 p-b-20 text-center">
																<button type="button" class="btn btn-circle btn-secondary" data-dismiss="modal">Close</button>
													    		<button type="submit" class="btn btn-circle btn-success save">Update</button>
															</div>
														</div>
													</div>
												</form>
											  </div>
											</div>
											<!-- Modal Ends -->
											<a href="" data-toggle="modal" data-target="#delete{{$timetable->id}}">
												<i class="material-icons text-danger">delete</i>
											</a>
											<!-- Modal -->
											<div class="modal fade" id="delete{{ $timetable->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
											  <div class="modal-dialog modal-dialog-centered" role="document">
											  	<form action="{{ url('admin/timetable/'.$timetable->id) }}" method="POST">
											  		@csrf
											  		@method('delete')
												   	<div class=" modal-content card-box">
												   		<div class="row">
														<div class="col-sm-12">
														<div class="card-head">
															<header>Delete Time Table</header>
														</div>
														<div class="card-body row ml-5">
															<h4 class="text-white">Do you want to delete {{$timetable->workouttitle}}??</h4>
															<h4 class="text-white">Can't Revert it!!!</h4>
														</div>
														<div class="col-lg-12 p-t-20 p-b-20 text-center">
															<button type="button" class="btn btn-circle btn-secondary" data-dismiss="modal">Close</button>
												    		<button type="submit" class="btn btn-circle btn-danger">Delete</button>
														</div>
														</div>
														</div>
													</div>
												</form>
											  </div>
											</div>
											<!-- Modal Ends -->
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- end page content -->
@endsection
@section('script')
<script type="text/javascript">
	function preview(input){
		if(input.files && input.files[0]){
			var reader = new FileReader();
			reader.onload = function(e){
				$('.imgs').attr('src', e.target.result);
			}
			reader.readAsDataURL(input.files[0]);
			}
	}
	$('.image').change(function(){
		preview(this);
	})
</script>
@endsection