@extends('layouts.adminlayouts.app')
@section('content')
<!-- start page content -->
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<div class="page-title-breadcrumb">
				<div class=" pull-left">
					<div class="page-title">Menu Setup</div>
				</div>
				<ol class="breadcrumb page-breadcrumb pull-right">
					<li><a class="parent-item"
							href="/home"><i class="fa fa-home"></i></a>&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li>Settings&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li class="active">Menu Setup</li>
				</ol>
			</div>
		</div>
		<input type="hidden" name="" value="/admin/menusetup" class="activeurl">
		<div class="row">
			<div class="col-md-12">
				<div class="card card-topline-green">
					<div class="card-head">
						<header>Menu Setup</header>
						<div class="tools">
							<a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
							<a class="t-collapse btn-color fa fa-chevron-down"
								href="javascript:;"></a>
						</div>
					</div>
					<div class="card-body " id="savetoimage">
						
							@if (session('msg'))
		                        <div class="tstSuccess msg" style="display: none;">
		                            {{ session('msg') }}
		                        </div>
                    		@endif
                    		@if (session('error'))
		                        <div class="tstError error" style="display: none;">
		                            {{ session('error') }}
		                        </div>
                    		@endif
						<div class="row col-md-12">
							 <!-- Button trigger modal -->
							<button type="button" class="btn btn-circle btn-success" data-toggle="modal" data-target="#addmenu"><i class="fa fa-plus"></i> Add </button>
							
							<!-- Modal -->
							<div class="modal fade" id="addmenu" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
							  <div class="modal-dialog modal-dialog-centered" role="document">
							  	<form action="{{ route('menusetup.store') }}" method="POST" enctype="multipart/form-data">
							  		@csrf
								   	<div class="modal-content card-box">
										<div class="modal-header">
											<header>Add Menu Setup</header>
										</div>
								   		<div class="row">
										<div class="card-body row form-body">
											<div class="col-lg-6 p-t-20">
												<label class="control-label" for="menuname">Name</label>
												<input type="text" name="menuname" id="menuname" placeholder="Enter Name" class="form-control" />
											</div>
											<div class="col-lg-6 p-t-20">
												<div class="form-label-group">
													<label class="control-label" for="menuurl">URL
													</label>
													<input type="text" name="menuurl" id="menuurl" placeholder="Enter URL" class="form-control" />
												</div>
											</div>
											<div class="col-lg-6 p-t-20">
												<div class="form-label-group">
													<label class="control-label" for="menuorder">Priority
													</label>
													<input type="text" name="menuorder" id="menuorder" placeholder="Enter Priority" class="form-control" />
												</div>
											</div>
											<div class="col-lg-6 p-t-20">
												<div class="form-label-group">
													<input type="checkbox" name="is_active" id="is_active" style="height: 20px; width: 20px; margin-top: 40px;"/>
													<label class="control-label" for="is_active">Is_Active
													</label>
												</div>
											</div>
										</div>
										<div class="col-lg-12 p-t-20 p-b-20 text-center">
											<button type="button" class="btn btn-circle btn-secondary" data-dismiss="modal">Close</button>
								    		<button type="submit" class="btn btn-circle btn-success">Save changes</button>
										</div>
										</div>
									</div>
								</form>
							  </div>
							</div>
							<!-- Modal Ends -->
						</div>
						<div class="table-scrollable ">
							<table class="table display" width="100%">
								<thead class="text-center">
									<tr>
										<th>S.N</th>
										<th>Name</th>
										<th>URL</th>
										<th>Priority</th>
										<th>Is_Active</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									@php
									$i=1;
									@endphp
									@foreach($menusetups as $menusetup)
									<tr class="text-center">
										<td>{{ $i++ }}</td>
										<td>{{ $menusetup->menuname }}</td>
										<td>{{ $menusetup->menuurl }}</td>
										<td>{{ $menusetup->menuorder }}</td>
										<td>{{ $menusetup->is_active }}</td>
										<td class="text-left">
											<!-- Button trigger modal -->
											<a href="" data-toggle="modal" data-target="#edit{{$menusetup->menusetupid}}">
												<i class="material-icons text-warning">edit</i>
											</a>
											<!-- Modal -->
											<div class="modal fade" id="edit{{ $menusetup->menusetupid }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
											  <div class="modal-dialog modal-dialog-centered" role="document">
											  	<form action="{{ url('admin/menusetup/'.$menusetup->menusetupid) }}" method="POST" enctype="multipart/form-data">
											  		@csrf
											  		@method('put')
												   	<div class="modal-content card-box">
														<div class="modal-header">
															<header>Edit Menu Setup</header>
														</div>
												   		<div class="row">
														<div class="card-body row form-body">
															<div class="col-lg-6 p-t-20">
																<label class="control-label" for="menuname">Name</label>
																<input type="text" name="menuname" id="menuname" placeholder="Enter Name" class="form-control" value="{{ $menusetup->menuname }}" />
															</div>
															<div class="col-lg-6 p-t-20">
																<div class="form-label-group">
																	<label class="control-label" for="menuurl">URL
																	</label>
																	<input type="text" name="menuurl" id="menuurl" placeholder="Enter URL" class="form-control" value="{{ $menusetup->menuurl }}"/>
																</div>
															</div>
															<div class="col-lg-6 p-t-20">
																<div class="form-label-group">
																	<label class="control-label" for="menuorder">Priority
																	</label>
																	<input type="text" name="menuorder" id="menuorder" placeholder="Enter Priority" class="form-control" value="{{ $menusetup->menuorder }}"/>
																</div>
															</div>
															<div class="col-lg-6 p-t-20">
																<div class="form-label-group">
																	@if($menusetup->is_active == "YES")
																	<input type="checkbox" name="is_active" id="is_active" style="height: 20px; width: 20px; margin-top: 40px;" checked=""/>
																	@else
																	<input type="checkbox" name="is_active" id="is_active" style="height: 20px; width: 20px; margin-top: 40px;" />
																	@endif
																	<label class="control-label" for="is_active">Is_Active
																	</label>
																</div>
															</div>
														</div>
														<div class="col-lg-12 p-t-20 p-b-20 text-center">
															<button type="button" class="btn btn-circle btn-secondary" data-dismiss="modal">Close</button>
												    		<button type="submit" class="btn btn-circle btn-success">Update</button>
														</div>
														</div>
													</div>
												</form>
											  </div>
											</div>
											<!-- Modal Ends -->
											<a href="" data-toggle="modal" data-target="#delete{{$menusetup->menusetupid}}">
												<i class="material-icons text-danger">delete</i>
											</a>
											<!-- Modal -->
											<div class="modal fade" id="delete{{ $menusetup->menusetupid }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
											  <div class="modal-dialog modal-dialog-centered" role="document">
											  	<form action="{{ url('admin/menusetup/'.$menusetup->menusetupid) }}" method="POST">
											  		@csrf
											  		@method('delete')
												   	<div class=" modal-content card-box">
												   		<div class="row">
														<div class="col-sm-12">
														<div class="card-head">
															<header>Delete Module</header>
														</div>
														<div class="card-body row ml-5">
															<h4 class="text-white">Do you want to delete {{$menusetup->menuname}}??</h4>
															<h4 class="text-white">Can't Revert it!!!</h4>
														</div>
														<div class="col-lg-12 p-t-20 p-b-20 text-center">
															<button type="button" class="btn btn-circle btn-secondary" data-dismiss="modal">Close</button>
												    		<button type="submit" class="btn btn-circle btn-danger">Delete</button>
														</div>
														</div>
														</div>
													</div>
												</form>
											  </div>
											</div>
											<!-- Modal Ends -->
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- end page content -->
@endsection
@section('script')

@endsection