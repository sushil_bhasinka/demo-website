@extends('layouts.adminlayouts.app')
@section('content')
<!-- start page content -->
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<div class="page-title-breadcrumb">
				<div class=" pull-left">
					<div class="page-title">Counter</div>
				</div>
				<ol class="breadcrumb page-breadcrumb pull-right">
					<li><a class="parent-item"
							href="/home"><i class="fa fa-home"></i></a>&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li>Settings&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li class="active">Counter</li>
				</ol>
			</div>
		</div>
		<input type="hidden" name="" value="/admin/counter" class="activeurl">
		<div class="row">
			<div class="col-md-12">
				<div class="card card-topline-green">
					<div class="card-head">
						<header>Counter Detail</header>
						<div class="tools">
							<a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
							<a class="t-collapse btn-color fa fa-chevron-down"
								href="javascript:;"></a>
						</div>
					</div>
					<div class="card-body " id="savetoimage">
						
							@if (session('msg'))
		                        <div class="tstSuccess msg" style="display: none;">
		                            {{ session('msg') }}
		                        </div>
                    		@endif
                    		@if (session('error'))
		                        <div class="tstError error" style="display: none;">
		                            {{ session('error') }}
		                        </div>
                    		@endif
						<div class="row col-md-12">
							 <!-- Button trigger modal -->
							<button type="button" class="btn btn-circle btn-success" data-toggle="modal" data-target="#addcounter"><i class="fa fa-plus"></i> Add </button>
					
							<!-- Modal -->
							<div class="modal fade" id="addcounter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
							  <div class="modal-dialog modal-lg" role="document">
							  	<form action="{{ route('counter.store') }}" method="POST" enctype="multipart/form-data">
							  		@csrf
								   	<div class="modal-content card-box">
										<div class="modal-header">
											<header>Add Counter Detail</header>
										</div>
								   		<div class="row">
											<div class="card-body row form-body">
												<div class="col-lg-6 p-t-20">
													<div class="form-label-group">
														<label class="control-label" for="title"> Counter Title
														</label>
														<input type="text" name="title" id="title" class="form-control" placeholder="Counter Title" required="">
													</div>
												</div>
												<div class="col-lg-6 p-t-20">
													<div class="form-label-group">
														<label class="control-label" for="number"> Counter Number
														</label>
														<input type="text" name="number" id="number" class="form-control" placeholder="Counter Title" required="">
													</div>
												</div>
												<div class="col-lg-12 p-t-20">
													<div class="form-label-group">
														<label class="control-label" for="description">Counter Description
														</label>
														<textarea name="description" id="description" class="form-control" placeholder="Counter Title in Detail"></textarea>
													</div>
												</div>
											</div>
											<div class="col-lg-12 p-t-20 p-b-20 text-center">
												<button type="button" class="btn btn-circle btn-secondary" data-dismiss="modal">Close</button>
									    		<button type="submit" class="btn btn-circle btn-success save">Save changes</button>
											</div>
										</div>
									</div>
								</form>
							  </div>
							</div>
							<!-- Modal Ends -->
						</div>
						<div class="table-scrollable ">
							<table class="table display" width="100%">
								<thead class="text-center">
									<tr>
										<th>S.N</th>
										<th>Title</th>
										<th>Number</th>
										<th>Description</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									@php
									$i=1;
									@endphp
									@foreach($counters as $counter)
									<tr class="text-center">
										<td width="5%">{{$i++}}</td>
										<td class="text-left" width="15%">{{ $counter->title }}</td>
										<td  width="10%">{{ $counter->number }}</td>
										<td class="text-left" width="60%">{!! $counter->description !!}</td>
										<td class="text-left" width="10%">
											<!-- Button trigger modal -->
											<a href="" data-toggle="modal" data-target="#edit{{$counter->id}}">
												<i class="material-icons text-warning">edit</i>
											</a>
											<!-- Modal -->
											<div class="modal fade" id="edit{{ $counter->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
											  <div class="modal-dialog modal-lg" role="document">
											  	<form action="{{ url('admin/counter/'.$counter->id) }}" method="POST" enctype="multipart/form-data">
											  		@csrf
											  		@method('put')
												   	<div class="modal-content card-box">
														<div class="modal-header">
															<header>Edit Counter Detail</header>
														</div>
												   		<div class="row">
															<div class="card-body row form-body">	
																<div class="col-lg-6 p-t-20">
																	<div class="form-label-group">
																		<label class="control-label" for="title"> Counter Title
																		</label>
																		<input type="text" name="title" id="title" class="form-control" placeholder="Counter Title" required="" value="{{$counter->title}}">
																	</div>
																</div>
																<div class="col-lg-6 p-t-20">
																	<div class="form-label-group">
																		<label class="control-label" for="number"> Counter Number
																		</label>
																		<input type="text" name="number" id="number" class="form-control" placeholder="Counter Title" required="" value="{{$counter->number}}">
																	</div>
																</div>
																<div class="col-lg-12 p-t-20">
																	<div class="form-label-group">
																		<label class="control-label" for="description">Counter Description
																		</label>
																		<textarea name="description" id="description" class="form-control" placeholder="Counter Title in Detail">{{$counter->description}}</textarea>
																	</div>
																</div>
															</div>
															<div class="col-lg-12 p-t-20 p-b-20 text-center">
																<button type="button" class="btn btn-circle btn-secondary" data-dismiss="modal">Close</button>
													    		<button type="submit" class="btn btn-circle btn-success save">Update</button>
															</div>
														</div>
													</div>
												</form>
											  </div>
											</div>
											<!-- Modal Ends -->
											<a href="" data-toggle="modal" data-target="#delete{{$counter->id}}">
												<i class="material-icons text-danger">delete</i>
											</a>
											<!-- Modal -->
											<div class="modal fade" id="delete{{ $counter->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
											  <div class="modal-dialog modal-dialog-centered" role="document">
											  	<form action="{{ url('admin/counter/'.$counter->id) }}" method="POST">
											  		@csrf
											  		@method('delete')
												   	<div class=" modal-content card-box">
												   		<div class="row">
														<div class="col-sm-12">
														<div class="card-head">
															<header>Delete Counter Detail</header>
														</div>
														<div class="card-body row ml-5">
															<h4 class="text-white">Do you want to delete {{ $counter->title }}??</h4>
															<h4 class="text-white">Can't Revert it !!!</h4>
														</div>
														<div class="col-lg-12 p-t-20 p-b-20 text-center">
															<button type="button" class="btn btn-circle btn-secondary" data-dismiss="modal">Close</button>
												    		<button type="submit" class="btn btn-circle btn-danger">Delete</button>
														</div>
														</div>
														</div>
													</div>
												</form>
											  </div>
											</div>
											<!-- Modal Ends -->
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- end page content -->
@endsection
@section('script')
<script type="text/javascript">
	function preview(input){
		if(input.files && input.files[0]){
			var reader = new FileReader();
			reader.onload = function(e){
				$('.imgs').attr('src', e.target.result);
			}
			reader.readAsDataURL(input.files[0]);
			}
	}
	$('.image').change(function(){
		preview(this);
	})
</script>
@endsection