@extends('layouts.adminlayouts.app')
@section('content')
<!-- start page content -->
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<div class="page-title-breadcrumb">
				<div class=" pull-left">
					<div class="page-title">SubModule Setup</div>
				</div>
				<ol class="breadcrumb page-breadcrumb pull-right">
					<li><a class="parent-item"
							href="/home"><i class="fa fa-home"></i></a>&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li>Settings&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li class="active">SubModules</li>
				</ol>
			</div>
		</div>
		<input type="hidden" name="" value="/admin/submodule" class="activeurl">
		<div class="row">
			<div class="col-md-12">
				<div class="card card-topline-green">
					<div class="card-head">
						<header>SubModule Setup</header>
						<div class="tools">
							<a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
							<a class="t-collapse btn-color fa fa-chevron-down"
								href="javascript:;"></a>
						</div>
					</div>
					<div class="card-body " id="savetoimage">
						
							@if (session('msg'))
		                        <div class="tstSuccess msg" style="display: none;">
		                            {{ session('msg') }}
		                        </div>
                    		@endif
                    		@if (session('error'))
		                        <div class="tstError error" style="display: none;">
		                            {{ session('error') }}
		                        </div>
                    		@endif
						<div class="row col-md-12">
							 <!-- Button trigger modal -->
							<button type="button" class="btn btn-circle btn-success" data-toggle="modal" data-target="#addsubmodule"><i class="fa fa-plus"></i> Add </button>
							<!-- Modal -->
							<div class="modal fade" id="addsubmodule" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
							  <div class="modal-dialog modal-dialog-centered" role="document">
							  	<form action="{{ route('submodule.store') }}" method="POST">
							  		@csrf
								   	<div class="modal-content card-box">
										<div class="modal-header">
											<header>Add SubModule</header>
										</div>
										<div class="card-body row form-body">
											<div class="col-lg-6 p-t-20">
												<label class="control-label" for="submodulename">SubModule Name</label>
												<select class="form-control" name="moduleid">
													<option selected disabled>Select SubModule Name</option>
													@foreach($modules as $module)
													<option value="{{ $module->moduleid }}">{{ $module->modulename }}</option>
													@endforeach
												</select>
											</div>
											<div class="col-lg-6 p-t-20">
												<div class="form-label-group">
													<label class="control-label" for="submodulename">SubModule Name
													</label>
													<input type="text" name="submodulename" id="submodulename" placeholder="enter submodule name" class="form-control" />
												</div>
											</div>
											<div class="col-lg-6 p-t-20">
												<div class="form-label-group">
													<label class="control-label" for="submoduleurl">SubModule URL
													</label>
													<input type="text" name="submoduleurl" id="submoduleurl" placeholder="enter submodule url" class="form-control" />
												</div>
											</div>
											<div class="col-lg-6 p-t-20">
												<div class="form-label-group">
													<label class="control-label" for="submoduleorder">SubModule Priority
													</label>
													<input type="number" name="submoduleorder" id="submoduleorder" placeholder="enter submodule priority" class="form-control" />
												</div>
											</div>
										</div>
										<div class="col-lg-12 p-t-20 p-b-20 text-center">
											<button type="button" class="btn btn-circle btn-secondary" data-dismiss="modal">Close</button>
								    		<button type="submit" class="btn btn-circle btn-success">Save changes</button>
										</div>
									</div>
								</form>
							  </div>
							</div>
							<!-- Modal Ends -->
						</div>
						<div class="table-scrollable ">
							<table class="table display" width="100%">
								<thead class="text-center">
									<tr>
										<th>S.N</th>
										<th>Module Name</th>
										<th>SubModule Name</th>
										<th>SubModule Priority</th>
										<th>SubModule url</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									@php
									$i=1;
									@endphp
									@foreach($submodule as $submod)
									<tr class="text-center">
										<td>{{ $i++ }}</td>
										<td>{{ $submod->modulename }}</td>
										<td>{{ $submod->submodulename }}</td>
										<td>{{ $submod->submoduleorder }}</td>
										<td>{{ $submod->url }}</td>
										<td class="text-left">
											<!-- Button trigger modal -->
											<a href="" data-toggle="modal" data-target="#edit{{$submod->submoduleid}}">
												<i class="material-icons text-warning">edit</i>
											</a>
											<!-- Modal -->
											<div class="modal fade" id="edit{{ $submod->submoduleid }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
											  <div class="modal-dialog modal-dialog-centered" role="document">
											  	<form action="{{ url('admin/submodule/'.$submod->submoduleid) }}" method="POST">
											  		@csrf
											  		@method('put')
												   	<div class=" modal-content card-box">
												   		<div class="row">
														<div class="col-sm-12">
														<div class="modal-header">
															<header>Edit Module</header>
														</div>
														<div class="card-body row form-body text-left">
															<div class="col-lg-6 p-t-20">
																<label class="control-label" for="submodulename">SubModule Name</label>
																<select class="form-control" name="moduleid">
																	<option selected disabled>Select SubModule Name</option>
																	@foreach($modules as $module)
																	@if($module->moduleid == $submod->moduleid) 
																	<option selected value="{{ $module->moduleid }}">{{ $module->modulename }}</option>
																	@endif
																	@endforeach
																</select>
															</div>
															<div class="col-lg-6 p-t-20">
																<div class="form-label-group">
																	<label class="control-label" for="submodulename">SubModule Name
																	</label>
																	<input type="text" name="submodulename" id="submodulename" placeholder="enter submodule name" class="form-control" value="{{ old('submodulename',$submod->submodulename) }}" />
																</div>
															</div>
															<div class="col-lg-6 p-t-20">
																<div class="form-label-group">
																	<label class="control-label" for="submoduleurl">SubModule URL
																	</label>
																	<input type="text" name="submoduleurl" id="submoduleurl" placeholder="enter submodule url" class="form-control" value="{{ old('url',$submod->url) }}"/>
																</div>
															</div>
															<div class="col-lg-6 p-t-20">
																<div class="form-label-group">
																	<label class="control-label" for="submoduleorder">SubModule Priority
																	</label>
																	<input type="number" name="submoduleorder" id="submoduleorder" placeholder="enter submodule priority" class="form-control" value="{{ old('submoduleorder',$submod->submoduleorder) }}"/>
																</div>
															</div>
														</div>
														<div class="col-lg-12 p-t-20 p-b-20 text-center">
															<button type="button" class="btn btn-circle btn-secondary" data-dismiss="modal">Close</button>
												    		<button type="submit" class="btn btn-circle btn-success">Update changes</button>
														</div>
														</div>
														</div>
													</div>
												</form>
											  </div>
											</div>
											<!-- Modal Ends -->
											<a href="" data-toggle="modal" data-target="#delete{{$submod->submoduleid}}">
												<i class="material-icons text-danger">delete</i>
											</a>
											<!-- Modal -->
											<div class="modal fade" id="delete{{ $submod->submoduleid }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
											  <div class="modal-dialog modal-dialog-centered" role="document">
											  	<form action="{{ url('admin/submodule/'.$submod->submoduleid) }}" method="POST">
											  		@csrf
											  		@method('delete')
												   	<div class=" modal-content card-box text-center">
												   		<div class="row">
														<div class="col-sm-12">
														<div class="card-head">
															<header>Delete Module</header>
														</div>
														<div class="card-body row ml-5">
															<h3 class="text-white">Do you want to delete {{$submod->modulename}} Module??</h3>
															<h4 class="text-white">Can't Revert it!!!</h4>
														</div>
														<div class="col-lg-12 p-t-20 p-b-20 text-center">
															<button type="button" class="btn btn-circle btn-secondary" data-dismiss="modal">Close</button>
												    		<button type="submit" class="btn btn-circle btn-danger">Delete</button>
														</div>
														</div>
														</div>
													</div>
												</form>
											  </div>
											</div>
											<!-- Modal Ends -->
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
							{{ $submodule->links() }}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- end page content -->
@endsection