@extends('layouts.adminlayouts.app')
@section('content')
<!-- start page content -->
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<div class="page-title-breadcrumb">
				<div class=" pull-left">
					<div class="page-title">Event </div>
				</div>
				<ol class="breadcrumb page-breadcrumb pull-right">
					<li><a class="parent-item"
							href="/home"><i class="fa fa-home"></i></a>&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li>Settings&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li class="active">Event </li>
				</ol>
			</div>
		</div>
		<input type="hidden" name="" value="/admin/eventsetup" class="activeurl">
		<div class="row">
			<div class="col-md-12">
				<div class="card card-topline-green">
					<div class="card-head">
						<header>Event Detail</header>
						<div class="tools">
							<a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
							<a class="t-collapse btn-color fa fa-chevron-down"
								href="javascript:;"></a>
						</div>
					</div>
					<div class="card-body " id="savetoimage">
						
							@if (session('msg'))
		                        <div class="tstSuccess msg" style="display: none;">
		                            {{ session('msg') }}
		                        </div>
                    		@endif
                    		@if (session('error'))
		                        <div class="tstError error" style="display: none;">
		                            {{ session('error') }}
		                        </div>
                    		@endif
						<div class="row col-md-12">
							 <!-- Button trigger modal -->
							<button type="button" class="btn btn-circle btn-success" data-toggle="modal" data-target="#addevent"><i class="fa fa-plus"></i> Add </button>
					
							<!-- Modal -->
							<div class="modal fade" id="addevent" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
							  <div class="modal-dialog modal-lg" role="document">
							  	<form action="{{ route('eventsetup.store') }}" method="POST" enctype="multipart/form-data">
							  		@csrf
								   	<div class="modal-content card-box">
										<div class="modal-header">
											<header>Add Event</header>
										</div>
								   		<div class="row">
											<div class="card-body row form-body">
												<div class="col-lg-6 p-t-20">
													<div class="form-label-group">
														<label class="control-label" for="title"> Event Title
														</label>
														<input type="text" name="title" id="title" class="form-control" placeholder="Event Title" required=""> 
													</div>
												</div>
												<div class="col-lg-6 p-t-20">
													<div class="form-label-group">
														<label class="control-label" for="date"> Event Date
														</label>
														<input type="text" name="date" class="form-control" placeholder="YYYY/MM/DD" required="">
													</div>
												</div>
												<div class="row col-lg-12 p-t-20">
													<div class="col-lg-4">
														<div class="form-label-group">
															<label class="control-label" for="image">Event Image
															</label>
															<input type="file" name="image" id="image" class="image form-control" required="">
														</div>
													</div>
													<div class="col-lg-4"> 
														<div class="form-label-group">
															<img src="" class="imgs" height="100" width="250">
														</div>
													</div>
												</div>
												<div class="col-lg-12 p-t-20">
													<div class="form-label-group">
														<label class="control-label" for="description"> Description
														</label>
														<textarea name="description" id="description" class="form-control" placeholder="Event Description" required=""></textarea>
													</div>
												</div>
											</div>
											<div class="col-lg-12 p-t-20 p-b-20 text-center">
												<button type="button" class="btn btn-circle btn-secondary" data-dismiss="modal">Close</button>
									    		<button type="submit" class="btn btn-circle btn-success save">Save changes</button>
											</div>
										</div>
									</div>
								</form>
							  </div>
							</div>
							<!-- Modal Ends -->
						</div>
						<div class="table-scrollable ">
							<table class="table display" width="100%">
								<thead class="text-center">
									<tr>
										<th>S.N</th>
										<th>Title</th>
										<th>Description</th>
										<th>Image</th>
										<th>Event Date</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									@php
									$i=1;
									@endphp
									@foreach($events as $event)
									<tr class="text-center">
										<td>{{$i++}}</td>
										<td class="text-left">{{ $event->title }}</td>
										<td class="text-left">{!! $event->description !!}</td>
										<td><object data="{{'data:image/jpg;base64,'.$event->image}}" style="height:80px;width:130px"></object></td>
										<td>{{ $event->date }}</td>
										<td class="text-left" width="10%">
											<!-- Button trigger modal -->
											<a href="" data-toggle="modal" data-target="#edit{{$event->id}}">
												<i class="material-icons text-warning">edit</i>
											</a>
											<!-- Modal -->
											<div class="modal fade" id="edit{{ $event->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
											  <div class="modal-dialog modal-lg" role="document">
											  	<form action="{{ url('admin/eventsetup/'.$event->id) }}" method="POST" enctype="multipart/form-data">
											  		@csrf
											  		@method('put')
												   	<div class="modal-content card-box">
														<div class="modal-header">
															<header>Edit Event</header>
														</div>
												   		<div class="row">
															<div class="card-body row form-body">	
																<div class="col-lg-6 p-t-20">
																	<div class="form-label-group">
																		<label class="control-label" for="title"> Event Title
																		</label>
																		<input type="text" name="title" id="title" class="form-control" placeholder="Event Title" required="" value="{{ $event->title }}"> 
																	</div>
																</div>
																<div class="col-lg-6 p-t-20">
																	<div class="form-label-group">
																		<label class="control-label" for="date"> Event Date
																		</label>
																		<input type="text" name="date" class="form-control" required="" value="{{ $event->date }}">
																	</div>
																</div>
																<div class="row col-lg-12 p-t-20">
																	<div class="col-lg-4">
																		<div class="form-label-group">
																			<label class="control-label" for="image">Event Image
																			</label>
																			<input type="file" name="image" id="image" class="image form-control">
																		</div>
																	</div>
																	<div class="col-lg-4"> 
																		<div class="form-label-group">
																			<img src="{{'data:image/jpg;base64,'.$event->image}}" class="imgs" height="100" width="250">
																		</div>
																	</div>
																</div>
																<div class="col-lg-12 p-t-20">
																	<div class="form-label-group">
																		<label class="control-label" for="description"> Description
																		</label>
																		<textarea name="description" id="description" class="form-control" placeholder="Event Description" required="">{{ $event->description }}</textarea>
																	</div>
																</div>
															</div>
															<div class="col-lg-12 p-t-20 p-b-20 text-center">
																<button type="button" class="btn btn-circle btn-secondary" data-dismiss="modal">Close</button>
													    		<button type="submit" class="btn btn-circle btn-success save">Update</button>
															</div>
														</div>
													</div>
												</form>
											  </div>
											</div>
											<!-- Modal Ends -->
											<a href="" data-toggle="modal" data-target="#delete{{$event->id}}">
												<i class="material-icons text-danger">delete</i>
											</a>
											<!-- Modal -->
											<div class="modal fade" id="delete{{ $event->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
											  <div class="modal-dialog modal-dialog-centered" role="document">
											  	<form action="{{ url('admin/eventsetup/'.$event->id) }}" method="POST">
											  		@csrf
											  		@method('delete')
												   	<div class=" modal-content card-box">
												   		<div class="row">
														<div class="col-sm-12">
														<div class="card-head">
															<header>Delete Event</header>
														</div>
														<div class="card-body row ml-5">
															<h4 class="text-white">Do you want to delete {{$event->title}}??</h4>
															<h4 class="text-white">Can't Revert it !!!</h4>
														</div>
														<div class="col-lg-12 p-t-20 p-b-20 text-center">
															<button type="button" class="btn btn-circle btn-secondary" data-dismiss="modal">Close</button>
												    		<button type="submit" class="btn btn-circle btn-danger">Delete</button>
														</div>
														</div>
														</div>
													</div>
												</form>
											  </div>
											</div>
											<!-- Modal Ends -->
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- end page content -->
@endsection
@section('script')
<script type="text/javascript">
	function preview(input){
		if(input.files && input.files[0]){
			var reader = new FileReader();
			reader.onload = function(e){
				$('.imgs').attr('src', e.target.result);
			}
			reader.readAsDataURL(input.files[0]);
			}
	}
	$('.image').change(function(){
		preview(this);
	})
</script>
@endsection