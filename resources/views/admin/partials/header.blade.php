<!-- google font -->
	<!-- <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" /> -->
	<!-- icons -->
	<link href="{{ asset('/admin/fonts/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/admin/fonts/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/admin/fonts/material-design-icons/material-icon.css') }}" rel="stylesheet" type="text/css" />
	<!--bootstrap -->
	<link href="{{ asset('/admin/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/admin/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" type="text/css" />
	<!-- data tables -->
    <link href="{{ asset('/admin/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/admin/plugins/summernote/summernote.css') }}" rel="stylesheet">
	<!-- Material Design Lite CSS -->
	<link rel="stylesheet" href="{{ asset('/admin/plugins/material/material.min.css') }}">
	<link rel="stylesheet" href="{{ asset('/admin/css/material_style.css') }}">
	<!-- inbox style -->
	<link href="{{ asset('/admin/css/pages/inbox.min.css') }}" rel="stylesheet" type="text/css" />
	<!-- Theme Styles -->
	<link href="{{ asset('/admin/css/theme/dark/theme_style.css') }}" rel="stylesheet" id="rt_style_components" type="text/css" />
	<link href="{{ asset('/admin/css/plugins.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/admin/css/theme/dark/style.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/admin/css/responsive.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/admin/css/theme/dark/theme-color.css') }}" rel="stylesheet" type="text/css" />
	<!-- dropzone -->
	<link href="{{asset('/admin/plugins/dropzone/dropzone.css')}}" rel="stylesheet" media="screen">
	<!-- Date Time item CSS -->
	<link rel="stylesheet" href="{{asset('/admin/plugins/material-datetimepicker/bootstrap-material-datetimepicker.css')}}" />
	
	<!-- Jquery Toast css -->
	<link rel="stylesheet" href="{{ asset('admin/plugins/jquery-toast/dist/jquery.toast.min.css') }}">
	<style type="text/css">
		input[type=checkbox]{
			height: 15px; 
			width: 15px; 
		}
	</style>
