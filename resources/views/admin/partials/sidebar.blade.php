<!-- start page container -->
		<div class="page-container">
			<!-- start sidebar menu -->
			<div class="sidebar-container">
				<div class="sidemenu-container navbar-collapse collapse fixed-menu">
					<div id="remove-scroll" class="left-sidemenu">
						<ul class="sidemenu  page-header-fixed slimscroll-style" data-keep-expanded="false"
							data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
							<li class="sidebar-toggler-wrapper hide">
								<div class="sidebar-toggler">
									<span></span>
								</div>
							</li>
							<li class="sidebar-user-panel">
								<div class="user-panel" style="padding: 0px 0px 5px 5px;">
									<div class="pull-left image">
										<img src="{{'data:image/jpg;base64,'.Auth::user()->image}}" class="img-circle user-img-circle"
											alt="User Image" style="height: 70px;"/>
									</div>
									<div class="pull-left info">
										<p> {{ Auth::user()->name }}</p>
										<p> {{ Auth::user()->contactno }}</p>
									</div>
								</div>
							</li>
							<li class="nav-item start">
								<a href="{{ url('/home') }}" class="nav-link nav-toggle">
									<i class="material-icons">dashboard</i>
									<span class="title">Dashboard</span>
									<span class="selected"></span>
								</a>
							</li>
							
							@foreach($module as $module)
							<li class="nav-item">
								<a href="#" class="nav-link nav-toggle"> 
									{!! $module->moduleicon !!}
									<span class="title">{{$module->modulename}}</span> 
									<span class="arrow"></span>
								</a>
								<ul class="sub-menu">
									@foreach($submodules as $submodule)
									@if($module->moduleid == $submodule->moduleid)
									<li class="nav-item">
										<a href="{{ url($submodule->url) }}" class="nav-link forselect"> 
											{!! $submodule->submoduleicon !!}
											<span class="title">{{$submodule->submodulename}}</span>
										</a>
									</li>
									@endif
									@endforeach
								</ul>
							</li>
							@endforeach
						</ul>
					</div>
				</div>
			</div>
			<!-- end sidebar menu -->
		</div>
		<!-- end page container -->