		<!-- start footer -->
		<div class="page-footer">
			<div class="page-footer-inner">
		      <script>
		        document.write(new Date().getFullYear())
		      </script> &copy; Made by
		      <a href="mailto:bhasanewar@gmail.com" target="_blank">Sushil Bhasinka</a>  for a Better Website.
			</div>
			<div class="scroll-to-top">
				<i class="icon-arrow-up"></i>
			</div>
		</div>
		<!-- end footer -->