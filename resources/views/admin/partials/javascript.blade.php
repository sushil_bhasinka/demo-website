	<!-- start js include path -->
	<script src="{{ asset('/admin/plugins/jquery/jquery.min.js') }}"></script>
	<script src="{{ asset('/admin/plugins/popper/popper.js') }}"></script>
	<script src="{{ asset('/admin/plugins/jquery-blockui/jquery.blockui.min.js') }}"></script>
	<script src="{{ asset('/admin/plugins/jquery-slimscroll/jquery.slimscroll.js') }}"></script>
	<script src="{{ asset('/admin/plugins/moment/moment.min.js') }}"></script>
	<!-- bootstrap -->
	<script src="{{ asset('/admin/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('/admin/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js') }}"></script>
	<script src="{{ asset('/admin/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>
<!-- 	<script src="{{ asset('/admin/plugins/sparkline/jquery.sparkline.js') }}"></script>
	<script src="{{ asset('/admin/js/pages/sparkline/sparkline-data.js') }}"></script> -->
	<!-- Common js-->
	<script src="{{ asset('/admin/js/app.js') }}"></script>
	<script src="{{ asset('/admin/js/layout.js') }}"></script>
	<script src="{{ asset('/admin/js/theme-color.js') }}"></script>
	<!-- material -->
	<script src="{{ asset('/admin/plugins/material/material.min.js') }}"></script>
	<script src="{{asset('/admin/js/pages/material-select/getmdl-select.js')}}"></script>
	<script src="{{asset('/admin/plugins/material-datetimepicker/moment-with-locales.min.js')}}"></script>
	<script src="{{asset('/admin/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js')}}"></script>
	<script src="{{asset('/admin/plugins/material-datetimepicker/datetimepicker.js')}}"></script>
	<!-- dropzone -->
	<script src="{{asset('/dropzone/dropzone.js')}}"></script>
<!-- 
	<script src="{{ asset('/admin/plugins/chart-js/Chart.bundle.js') }}"></script>
	<script src="{{ asset('/admin/plugins/chart-js/utils.js') }}"></script>
	<script src="{{ asset('/admin/js/pages/chart/chartjs/home-data.js') }}"></script> -->
	<!-- summernote -->
	<script src="{{ asset('/admin/plugins/summernote/summernote.js') }}"></script>
	<script src="{{ asset('/admin/js/pages/summernote/summernote-data.js') }}"></script>
	 <!-- data tables -->
    <script src="{{ asset('/admin/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('/admin/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('/admin/js/pages/table/table_data.js')}}"></script>
	<!-- Toastjs -->
	<script src="{{ asset('/admin/plugins/jquery-toast/dist/jquery.toast.min.js') }}"></script>
	<script src="{{ asset('/admin/plugins/jquery-toast/dist/toast.js') }}"></script>
	<!-- fontawesome -->
<!-- 	<script src="{{ asset('/fontawesome/js/fontawesome.min.js') }}"></script>
	<script src="{{ asset('/fontawesome/js/all.min.js') }}"></script> -->

<script type="text/javascript">
    $(document).ready(function()
    {
         var url = $('.activeurl').val();
        var urlelement = (window.location.href).split("/");
        var mainurl = urlelement[0] + "//" + urlelement[2]+url;
        console.log(urlelement[3])
        
        $('ul.sub-menu > li.nav-item a').each(function(e){
            if(mainurl == $(this).attr('href')){
            $('.forselect').removeClass('active');
            $(this).addClass('active');
            $(this).parents('ul').add(this).each(function(){
            $(this).parent('li.nav-item').addClass('open');
            $(this).parent('li.nav-item').addClass('active');
            });
            }
        });
        if(urlelement[3] == 'home')
        {
        	$('.start').addClass('active')
        	$('.start').addClass('open')
        }
    });
</script>

 <script>
	// Time picker only
	$('.starttimepicker').datetimepicker({
		format: 'LT'
	});

	$('.endtimepicker').datetimepicker({
		format: 'LT'
	});
</script>

