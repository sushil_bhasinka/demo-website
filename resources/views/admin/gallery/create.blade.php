@extends('layouts.adminlayouts.app')
@section('content')

<!-- start page content -->
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<div class="page-title-breadcrumb">
				<div class=" pull-left">
					<div class="page-title">Gallery</div>
				</div>
				<ol class="breadcrumb page-breadcrumb pull-right">
					<li><a class="parent-item"
							href="/home"><i class="fa fa-home"></i></a>&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li>Front End Setup&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li class="active"><a href="{{ url('/admin/gallery') }}" class="parent-item text-white">Gallery</a></li>
				</ol>
			</div>
		</div>
		<input type="hidden" name="" value="/admin/gallery" class="activeurl">
		<div class="row">
			<div class="col-md-12">
				<div class="card card-topline-green">
					<div class="card-head">
						<header>{{ $albumname[0]->albumname }}({{ $albumname[0]->albumdate }})</header>
						<div class="tools">
                            <a href="{{ url('/admin/gallery') }}" class="text-white parent-item"><i class="material-icons f-left">arrow_back</i></a>
						</div>
					</div>
					<div class="card-body " id="savetoimage">
						
							@if (session('msg'))
		                        <div class="tstSuccess msg" style="display: none;">
		                            {{ session('msg') }}
		                        </div>
                    		@endif
                    		@if (session('error'))
		                        <div class="tstError error" style="display: none;">
		                            {{ session('error') }}
		                        </div>
                    		@endif
                    	
						    <form method="post" action="{{url('admin/gallery/upload/store')}}" enctype="multipart/form-data" class="dropzone" id="dropzone" >
						    @csrf
						       	<input type="hidden" name="albumid" value="{{$albumid}}" class="albumid">
							</form>   
						<hr>
						<div class="row col-md-12 albumImage">
                            @foreach($albumgalleries as $key=>$value)
							<div class="col-md-2"><object data="{{'data:image/jpg;base64,'.$value->image}}" style="height:100px;width:130px;"></object><a href="" class="text-danger" data-toggle="modal" data-target="#deleteImage_{{$value->id}}">Remove Image</a>
                            <!-- Modal -->
                            <div class="modal fade" id="deleteImage_{{$value->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                              <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLongTitle">Delete Image</h5>
                                  </div>
                                  <div class="modal-body">
                                    ...
                                  </div>
                                  <div class="modal-footer">
                                    <a href="{{ url('admin/delete/image/'.$value->id.'/'.$value->albumid) }}" class="btn btn-success" >Yes</a>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
                                  </div>
                                </div>
                              </div>
                            </div></div>
                            
                            @endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<!-- end page content -->
@endsection
@section('script')
<script type="text/javascript">
	function preview(input){
		if(input.files && input.files[0]){
			var reader = new FileReader();
			reader.onload = function(e){
				$('.imgs').attr('src', e.target.result);
			}
			reader.readAsDataURL(input.files[0]);
			}
	}
	$('.image').change(function(){
		preview(this);
	})
</script>
<script type="text/javascript">
Dropzone.options.dropzone =
 {
    paramName: 'file[]',
    maxFilesize: 12,
    renameFile: function(file) {
        var dt = new Date();
        var time = dt.getTime();
       return time+file.name;
    },
    acceptedFiles: ".jpeg,.jpg,.png,.gif",
    addRemoveLinks: true,
    timeout: 50000,
    removedfile: function(file) 
    {
        var name = file.upload.filename;
        var albumid=$('.albumid').val();
        /*console.log(name)
        console.log(albumid)*/
        $.ajax({
            type: 'GET',
            url: '{{ url("admin/image/delete") }}',
            data: {'filename': name,'albumid': albumid},
            success: function (data){
            	console.log(data)
                $.toast({
				    heading: 'Error',
				    text: data+'<br> Successfully Deleted!!',
				    showHideTransition: 'slide',
				    icon: 'error',
				    position: 'top-right',
				})
            },
            error: function(e) {
                console.log(e);
            }});
            var fileRef;
            return (fileRef = file.previewElement) != null ? 
            fileRef.parentNode.removeChild(file.previewElement) : void 0;
    },

    success: function(file, response) 
    {
        console.log(response);
        $.toast({
		    heading: 'Success',
		    text: response.success+'<br> Successfully Uploaded!!',
		    showHideTransition: 'slide',
		    icon: 'success',
		    position: 'top-right',
		})
    },
    error: function(file, response)
    {
       return false;
    }
};
</script>

@endsection