@extends('layouts.adminlayouts.app')
@section('content')
<!-- start page content -->
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<div class="page-title-breadcrumb">
				<div class=" pull-left">
					<div class="page-title">Gallery</div>
				</div>
				<ol class="breadcrumb page-breadcrumb pull-right">
					<li><a class="parent-item"
							href="/home"><i class="fa fa-home"></i></a>&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li>Settings&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li class="active">Gallery</li>
				</ol>
			</div>
		</div>
		<input type="hidden" name="" value="/admin/gallery" class="activeurl">
		<div class="row">
			<div class="col-md-12">
				<div class="card card-topline-green">
					<div class="card-head">
						<header>Albums</header>
						<div class="tools">
							<a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
							<a class="t-collapse btn-color fa fa-chevron-down"
								href="javascript:;"></a>
						</div>
					</div>
					<div class="card-body " id="savetoimage">
						
							@if (session('msg'))
		                        <div class="tstSuccess msg" style="display: none;">
		                            {{ session('msg') }}
		                        </div>
                    		@endif
                    		@if (session('error'))
		                        <div class="tstError error" style="display: none;">
		                            {{ session('error') }}
		                        </div>
                    		@endif
						<div class="row col-md-12">
							 <!-- Button trigger modal -->
							<button type="button" class="btn btn-circle btn-success" data-toggle="modal" data-target="#addalbum"><i class="material-icons f-left">add_to_photos</i>Ablum </button>
					
							<!-- Modal -->
							<div class="modal fade" id="addalbum" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
							  <div class="modal-dialog" role="document">
							  	<form action="{{ route('gallery.store') }}" method="POST" enctype="multipart/form-data">
							  		@csrf
								   	<div class="modal-content card-box">
										<div class="modal-header">
											<header>Add Album</header>
										</div>
								   		<div class="row">
											<div class="card-body row form-body">
												<div class="col-lg-12 p-t-20">
													<div class="form-label-group">
														<label class="control-label" for="albumname"> Album Name
														</label>
														<input type="text" name="albumname" id="albumname" class="form-control" placeholder="Enter Album Name">
													</div>
												</div>
												<div class="col-lg-12 p-t-20">
													<div class="form-label-group">
														<label class="control-label" for="albumdate"> Album Date
														</label>
														<input type="text" name="albumdate" id="albumdate" class="form-control" placeholder="YYYY/MM/DD">
													</div>
												</div>
											</div>
											<div class="col-lg-12 p-t-20 p-b-20 text-center">
												<button type="button" class="btn btn-circle btn-secondary" data-dismiss="modal">Close</button>
									    		<button type="submit" class="btn btn-circle btn-success save">Save changes</button>
											</div>
										</div>
									</div>
								</form>
							  </div>
							</div>
							<!-- Modal Ends -->
						</div>
						<hr>
						<div class="row col-md-12">
							@foreach($albums as $album)
							<div class="col-lg-2 text-center">
								<a href="{{url('/admin/gallery/album/'.$album->id)}}" style="color: orange;"><i class="material-icons f-left">folder</i>
								<h5>{{$album->albumname}}</h5>
								<h5>{{$album->albumdate}}</h5></a>
							</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- end page content -->
@endsection
@section('script')
<script type="text/javascript">
	function preview(input){
		if(input.files && input.files[0]){
			var reader = new FileReader();
			reader.onload = function(e){
				$('.imgs').attr('src', e.target.result);
			}
			reader.readAsDataURL(input.files[0]);
			}
	}
	$('.image').change(function(){
		preview(this);
	})
</script>
@endsection