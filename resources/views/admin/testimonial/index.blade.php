@extends('layouts.adminlayouts.app')
@section('content')
<!-- start page content -->
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<div class="page-title-breadcrumb">
				<div class=" pull-left">
					<div class="page-title">Testimonials Setup</div>
				</div>
				<ol class="breadcrumb page-breadcrumb pull-right">
					<li><a class="parent-item"
							href="/home"><i class="fa fa-home"></i></a>&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li>Settings&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li class="active">Testimonials Setup</li>
				</ol>
			</div>
		</div>
		<input type="hidden" name="" value="/admin/testimonial" class="activeurl">
		<div class="row">
			<div class="col-md-12">
				<div class="card card-topline-green">
					<div class="card-head">
						<header>Testimonials</header>
						<div class="tools">
							<a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
							<a class="t-collapse btn-color fa fa-chevron-down"
								href="javascript:;"></a>
						</div>
					</div>
					<div class="card-body " id="savetoimage">
						
							@if (session('msg'))
		                        <div class="tstSuccess msg" style="display: none;">
		                            {{ session('msg') }}
		                        </div>
                    		@endif
                    		@if (session('error'))
		                        <div class="tstError error" style="display: none;">
		                            {{ session('error') }}
		                        </div>
                    		@endif
						<div class="row col-md-12">
							 <!-- Button trigger modal -->
							<button type="button" class="btn btn-circle btn-success" data-toggle="modal" data-target="#addmember"><i class="fa fa-plus"></i> Add </button>
					
							<!-- Modal -->
							<div class="modal fade" id="addmember" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
							  <div class="modal-dialog modal-lg" role="document">
							  	<form action="{{ route('testimonial.store') }}" method="POST" enctype="multipart/form-data">
							  		@csrf
								   	<div class="modal-content card-box">
										<div class="modal-header">
											<header>Add Testimonials</header>
										</div>
								   		<div class="row">
											<div class="card-body row form-body">
												<div class="col-lg-4 p-t-20">
													<div class="form-label-group">
														<label class="control-label" for="name">Name
														</label>
														<input type="text" name="name" id="name" class="form-control" placeholder="Enter  Name">
													</div>
												</div>
												<div class="col-lg-4 p-t-20">
													<div class="form-label-group">
														<label class="control-label" for="position">Position
														</label>
														<input type="text" name="position" id="position" class="form-control" placeholder="Enter  Position">
													</div>
												</div>
												<div class="col-lg-4 p-t-20">
													<div class="form-label-group">
														<label class="control-label" for="email">Email
														</label>
														<input type="text" name="email" id="email" class="form-control" placeholder="Email">
													</div>
												</div>
												<div class="col-lg-8 p-t-20"> 
													<div class="form-label-group">
														<label class="control-label" for="image">Image
														</label>
														<input type="file" name="image" id="image" class="image form-control">
													</div>
												</div>
												<div class="col-lg-4 p-t-20"> 
													<div class="form-label-group">
														<img src="" class="imgs" height="100" width="150">
													</div>
												</div>
												<div class="col-lg-12 p-t-20"> 
													<div class="form-label-group">
														<label class="control-label" for="imagecode">Image Code</label>
														<textarea name="imagecode" id="imagecode" class="form-control" placeholder="Image Code"></textarea>
													</div>
												</div>
												<div class="col-lg-12 p-t-20">
													<div class="form-label-group">
														<label class="control-label" for="testimonial">Testimonial
														</label>
														<textarea name="testimonial" id="testimonial" class="form-control" placeholder="Testimonial"></textarea>
													</div>
												</div>
												<div class="col-lg-12 p-t-20">
													<div class="form-label-group">
														<input type="checkbox" name="backgroundimage" id="backgroundimage" style="height: 20px; width: 20px;"/>
														<label class="control-label" for="backgroundimage">Is Background Image
														</label>
													</div>
												</div>
											</div>
											<div class="col-lg-12 p-t-20 p-b-20 text-center">
												<button type="button" class="btn btn-circle btn-secondary" data-dismiss="modal">Close</button>
									    		<button type="submit" class="btn btn-circle btn-success save">Save changes</button>
											</div>
										</div>
									</div>
								</form>
							  </div>
							</div>
							<!-- Modal Ends -->
						</div>
						<div class="table-scrollable ">
							<table class="table display" width="100%">
								<thead class="text-center">
									<tr>
										<th>S.N</th>
										<th>Name</th>
										<th>Email</th>
										<th>Occupation</th>
										<th>Testimonial</th>
										<th>Image</th>
										<th>Background Image</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									@php
									$i=1;
									@endphp
									@foreach($testimonials as $testimonial)
									<tr class="text-center">
										<td>{{ $i++ }}</td>
										<td>{{ $testimonial->fullname }}</td>
										<td>{{ $testimonial->email }}</td>
										<td>{{ $testimonial->position }}</td>
										<td>{{ $testimonial->testimonial }}</td>
										<td>
											@if($testimonial->image == null)
											<object data="{{'data:image/jpg;base64,'.$testimonial->imagecode}}" style="height:80px;width:100px"></object>
											@else
											<object data="{{'data:image/jpg;base64,'.$testimonial->image}}" style="height:80px;width:100px"></object>
											@endif
										</td>
										@if($testimonial->is_backgroundimage == "on")
										<td>Yes</td>
										@else
										<td>No</td>
										@endif
										<td class="text-left">
											<!-- Button trigger modal -->
											<a href="" data-toggle="modal" data-target="#edit{{$testimonial->id}}">
												<i class="material-icons text-warning">edit</i>
											</a>
											<!-- Modal -->
											<div class="modal fade" id="edit{{ $testimonial->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
											  <div class="modal-dialog modal-lg" role="document">
											  	<form action="{{ url('admin/testimonial/'.$testimonial->id) }}" method="POST" enctype="multipart/form-data">
											  		@csrf
											  		@method('put')
												   	<div class="modal-content card-box">
														<div class="modal-header">
															<header>Edit Testimonials</header>
														</div>
												   		<div class="row">
															<div class="card-body row form-body">
																<div class="col-lg-4 p-t-20">
																	<div class="form-label-group">
																		<label class="control-label" for="name">Name
																		</label>
																		<input type="text" name="name" id="name" class="form-control" value="{{ $testimonial->fullname }}" placeholder="Enter  Name">
																	</div>
																</div>
																<div class="col-lg-4 p-t-20">
																	<div class="form-label-group">
																		<label class="control-label" for="position">Position
																		</label>
																		<input type="text" name="position" id="position" class="form-control" value="{{ $testimonial->position }}" placeholder="Enter  Position">
																	</div>
																</div>
																<div class="col-lg-4 p-t-20">
																	<div class="form-label-group">
																		<label class="control-label" for="email">Email
																		</label>
																		<input type="text" name="email" id="email" class="form-control" value="{{ $testimonial->email }}" placeholder="Email">
																	</div>
																</div>
																<div class="col-lg-8 p-t-20"> 
																	<div class="form-label-group">
																		<label class="control-label" for="image">Image
																		</label>
																		<input type="file" name="image" id="image" class="image form-control">
																	</div>
																</div>
																<div class="col-lg-4 p-t-20"> 
																	<div class="form-label-group">
																		<img src="{{'data:image/jpg;base64,'.$testimonial->image}}" class="imgs" height="100" width="150">
																	</div>
																</div>
																<div class="col-lg-12 p-t-20"> 
																	<div class="form-label-group">
																		<label class="control-label" for="imagecode">Image Code</label>
																		<textarea name="imagecode" id="imagecode" class="form-control" placeholder="Image Code">{{ $testimonial->imagecode }}</textarea>
																	</div>
																</div>
																<div class="col-lg-12 p-t-20">
																	<div class="form-label-group">
																		<label class="control-label" for="testimonial">Testimonial
																		</label>
																		<textarea name="testimonial" id="testimonial" class="form-control" placeholder="Testimonial">{{ $testimonial->testimonial }}</textarea>
																	</div>
																</div>
																<div class="col-lg-12 p-t-20">
																	<div class="form-label-group">
																		@if($testimonial->is_backgroundimage == "on")
																		<input type="checkbox" checked="" name="backgroundimage" id="backgroundimage" style="height: 20px; width: 20px;"/>
																		@else
																		<input type="checkbox" name="backgroundimage" id="backgroundimage" style="height: 20px; width: 20px;"/>

																		@endif
																		<label class="control-label" for="backgroundimage">Is Background Image
																		</label>
																	</div>
																</div>
															</div>
															<div class="col-lg-12 p-t-20 p-b-20 text-center">
																<button type="button" class="btn btn-circle btn-secondary" data-dismiss="modal">Close</button>
													    		<button type="submit" class="btn btn-circle btn-success save">Update</button>
															</div>
														</div>
													</div>
												</form>
											  </div>
											</div>
											<!-- Modal Ends -->
											<a href="" data-toggle="modal" data-target="#delete{{$testimonial->id}}">
												<i class="material-icons text-danger">delete</i>
											</a>
											<!-- Modal -->
											<div class="modal fade" id="delete{{ $testimonial->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
											  <div class="modal-dialog modal-dialog-centered" role="document">
											  	<form action="{{ url('admin/testimonial/'.$testimonial->id) }}" method="POST">
											  		@csrf
											  		@method('delete')
												   	<div class=" modal-content card-box">
												   		<div class="row">
														<div class="col-sm-12">
														<div class="card-head">
															<header>Delete Testimonials</header>
														</div>
														<div class="card-body row ml-5">
															<h4 class="text-white">Do you want to delete {{$testimonial->name}}??</h4>
															<h4 class="text-white">Can't Revert it!!!</h4>
														</div>
														<div class="col-lg-12 p-t-20 p-b-20 text-center">
															<button type="button" class="btn btn-circle btn-secondary" data-dismiss="modal">Close</button>
												    		<button type="submit" class="btn btn-circle btn-danger">Delete</button>
														</div>
														</div>
														</div>
													</div>
												</form>
											  </div>
											</div>
											<!-- Modal Ends -->
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- end page content -->
@endsection
@section('script')
<script type="text/javascript">
	function preview(input){
		if(input.files && input.files[0]){
			var reader = new FileReader();
			reader.onload = function(e){
				$('.imgs').attr('src', e.target.result);
			}
			reader.readAsDataURL(input.files[0]);
			}
	}
	$('.image').change(function(){
		preview(this);
	})
</script>
@endsection