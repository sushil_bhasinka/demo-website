@extends('layouts.adminlayouts.app')
@section('content')
<!-- start page content -->
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<div class="page-title-breadcrumb">
				<div class=" pull-left">
					<div class="page-title">Organization Detail</div>
				</div>
				<ol class="breadcrumb page-breadcrumb pull-right">
					<li><a class="parent-item"
							href="/home"><i class="fa fa-home"></i></a>&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li>Settings&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li class="active">Organization Detail</li>
				</ol>
			</div>
		</div>
		<input type="hidden" name="" value="/admin/organizationdetail" class="activeurl">
		<div class="row">
			<div class="col-md-12">
				<div class="card card-topline-green">
					<div class="card-head">
						<header>Organization Detail</header>
						<div class="tools">
							<a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
							<a class="t-collapse btn-color fa fa-chevron-down"
								href="javascript:;"></a>
						</div>
					</div>
					<div class="card-body " id="savetoimage">
						
							@if (session('msg'))
		                        <div class="tstSuccess msg" style="display: none;">
		                            {{ session('msg') }}
		                        </div>
                    		@endif
                    		@if (session('error'))
		                        <div class="tstError error" style="display: none;">
		                            {{ session('error') }}
		                        </div>
                    		@endif
						<div class="row col-md-12">
							 <!-- Button trigger modal -->
							@if(count($orgdetails) < 1)
								<button type="button" class="btn btn-circle btn-success" data-toggle="modal" data-target="#addorganizationdetail"><i class="fa fa-plus"></i> Add </button>
							@endif
							<!-- Modal -->
							<div class="modal fade" id="addorganizationdetail" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
							  <div class="modal-dialog modal-lg" role="document">
							  	<form action="{{ route('organizationdetail.store') }}" method="POST" enctype="multipart/form-data">
							  		@csrf
								   	<div class="modal-content card-box">
										<div class="modal-header">
											<header>Add Organization Detail</header>
										</div>
								   		<div class="row">
										<div class="card-body row form-body">
											<div class="col-lg-4 p-t-20">
												<label class="control-label" for="organizationname">Name</label>
												<input type="text" name="organizationname" id="organizationname" placeholder="Enter Name" class="form-control" />
											</div>
											<div class="col-lg-4 p-t-20">
												<div class="form-label-group">
													<label class="control-label" for="organizationaddress">Address
													</label>
													<input type="text" name="organizationaddress" id="organizationaddress" placeholder="Enter Address" class="form-control" />
												</div>
											</div>
											<div class="col-lg-4 p-t-20">
												<div class="form-label-group">
													<label class="control-label" for="organizationemail">Email
													</label>
													<input type="email" name="organizationemail" id="organizationemail" placeholder="abc@____.com" class="form-control" />
												</div>
											</div>
											<div class="col-lg-4 p-t-20">
												<div class="form-label-group">
													<label class="control-label" for="organizationphone">Phone no.
													</label>
													<input type="text" name="organizationphone" id="organizationphone" placeholder="Enter Phone number" class="form-control" pattern="-?[0-9]*(\.[0-9]+)?"/>
												</div>
											</div>
											<div class="col-lg-4 p-t-20">
												<div class="form-label-group">
													<label class="control-label" for="organizationfax">Fax no.
													</label>
													<input type="text" name="organizationfax" id="organizationfax" placeholder="Enter Fax number" class="form-control" pattern="-?[0-9]*(\.[0-9]+)?"/>
												</div>
											</div>
											<div class="col-lg-12 p-t-20">
												<div class="form-label-group">
													<label class="control-label" for="organizationabout">About Organization
													</label>
													<textarea name="organizationabout" id="organizationabout" class="form-control" placeholder="About Us Shortly"></textarea>
												</div>
											</div>
											<div class="row col-lg-12 p-t-20">
												<div class="col-lg-8"> 
													<div class="form-label-group">
														<label class="control-label" for="organizationlogo">Logo
														</label>
														<input type="file" name="organizationlogo" id="organizationlogo" class="organizationlogo form-control" required="">
													</div>
												</div>
												<div class="col-lg-4"> 
													<div class="form-label-group">
														<img src="" class="imgs" height="100" width="150">
													</div>
												</div>
											</div>
										</div>
										<div class="col-lg-12 p-t-20 p-b-20 text-center">
											<button type="button" class="btn btn-circle btn-secondary" data-dismiss="modal">Close</button>
								    		<button type="submit" class="btn btn-circle btn-success">Save changes</button>
										</div>
										</div>
									</div>
								</form>
							  </div>
							</div>
							<!-- Modal Ends -->
						</div>
						<div class="table-scrollable ">
							<table class="table display" width="100%">
								<thead class="text-center">
									<tr>
										<th>Name</th>
										<th>Address</th>
										<th>Email</th>
										<th>Phone</th>
										<th>Fax</th>
										<th>Content</th>
										<th>Logo</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									@foreach($orgdetails as $orgdetail)
									<tr class="text-center">
										<td>{{ $orgdetail->name }}</td>
										<td>{{ $orgdetail->address }}</td>
										<td>{{ $orgdetail->email }}</td>
										<td>{{ $orgdetail->phonenumber }}</td>
										<td>{{ $orgdetail->faxnumber }}</td>
										<td>{!! $orgdetail->content !!}</td>
										<td><object data="{{'data:image/png;base64,'.$orgdetail->logo}}" style="height:80px;width:130px"></object></td>
										<td class="text-left">
											<!-- Button trigger modal -->
											<a href="" data-toggle="modal" data-target="#edit{{$orgdetail->organizationdetailid}}">
												<i class="material-icons text-warning">edit</i>
											</a>
											<!-- Modal -->
											<div class="modal fade" id="edit{{ $orgdetail->organizationdetailid }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
											  <div class="modal-dialog modal-lg" role="document">
											  	<form action="{{ url('admin/organizationdetail/'.$orgdetail->organizationdetailid) }}" method="POST" enctype="multipart/form-data">
											  		@csrf
											  		@method('put')
												   	<div class="modal-content card-box">
														<div class="modal-header">
															<header>Edit Organization Detail</header>
														</div>
												   		<div class="row">
														<div class="card-body row form-body">
															<div class="col-lg-4 p-t-20">
																<label class="control-label" for="organizationname">Name</label>
																<input type="text" name="organizationname" id="organizationname" placeholder="Enter Name" class="form-control" value="{{ $orgdetail->name }}" />
															</div>
															<div class="col-lg-4 p-t-20">
																<div class="form-label-group">
																	<label class="control-label" for="organizationaddress">Address
																	</label>
																	<input type="text" name="organizationaddress" id="organizationaddress" placeholder="Enter Address" class="form-control" value="{{ $orgdetail->address }}"/>
																</div>
															</div>
															<div class="col-lg-4 p-t-20">
																<div class="form-label-group">
																	<label class="control-label" for="organizationemail">Email
																	</label>
																	<input type="email" name="organizationemail" id="organizationemail" placeholder="abc@____.com" class="form-control" value="{{ $orgdetail->email }}"/>
																</div>
															</div>
															<div class="col-lg-4 p-t-20">
																<div class="form-label-group">
																	<label class="control-label" for="organizationphone">Phone no.
																	</label>
																	<input type="text" name="organizationphone" id="organizationphone" placeholder="Enter Phone number" class="form-control" pattern="-?[0-9]*(\.[0-9]+)?" value="{{ $orgdetail->phonenumber }}"/>
																</div>
															</div>
															<div class="col-lg-4 p-t-20">
																<div class="form-label-group">
																	<label class="control-label" for="organizationfax">Fax no.
																	</label>
																	<input type="text" name="organizationfax" id="organizationfax" placeholder="Enter Fax number" class="form-control" pattern="-?[0-9]*(\.[0-9]+)?" value="{{ $orgdetail->faxnumber }}"/>
																</div>
															</div>
															<div class="col-lg-12 p-t-20">
																<div class="form-label-group">
																	<label class="control-label" for="organizationabout">About Organization
																	</label>
																	<textarea name="organizationabout" id="organizationabout" class="form-control" placeholder="About Us Shortly">{{ $orgdetail->content }}</textarea>
																</div>
															</div>
															<div class="row col-lg-12 p-t-20">
																<div class="col-lg-8"> 
																	<div class="form-label-group">
																		<label class="control-label" for="organizationlogo">Logo
																		</label>
																		<input type="file" name="organizationlogo" id="organizationlogo" class="organizationlogo form-control">
																	</div>
																</div>
																<div class="col-lg-4"> 
																	<div class="form-label-group">
																		<img src="{{'data:image/png;base64,'.$orgdetail->logo}}" class="imgs" height="100" width="150">
																	</div>
																</div>
															</div>
														</div>
														<div class="col-lg-12 p-t-20 p-b-20 text-center">
															<button type="button" class="btn btn-circle btn-secondary" data-dismiss="modal">Close</button>
												    		<button type="submit" class="btn btn-circle btn-success">Update</button>
														</div>
														</div>
													</div>
												</form>
											  </div>
											</div>
											<!-- Modal Ends -->
											<a href="" data-toggle="modal" data-target="#delete{{$orgdetail->organizationdetailid}}">
												<i class="material-icons text-danger">delete</i>
											</a>
											<!-- Modal -->
											<div class="modal fade" id="delete{{ $orgdetail->organizationdetailid }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
											  <div class="modal-dialog modal-dialog-centered" role="document">
											  	<form action="{{ url('admin/organizationdetail/'.$orgdetail->organizationdetailid) }}" method="POST">
											  		@csrf
											  		@method('delete')
												   	<div class=" modal-content card-box">
												   		<div class="row">
														<div class="col-sm-12">
														<div class="card-head">
															<header>Delete Module</header>
														</div>
														<div class="card-body row ml-5">
															<h4 class="text-white">Do you want to delete {{$orgdetail->name}}??</h4>
															<h4 class="text-white">Can't Revert it!!!</h4>
														</div>
														<div class="col-lg-12 p-t-20 p-b-20 text-center">
															<button type="button" class="btn btn-circle btn-secondary" data-dismiss="modal">Close</button>
												    		<button type="submit" class="btn btn-circle btn-danger">Delete</button>
														</div>
														</div>
														</div>
													</div>
												</form>
											  </div>
											</div>
											<!-- Modal Ends -->
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- end page content -->
@endsection
@section('script')
<script type="text/javascript">
	function preview(input){
		if(input.files && input.files[0]){
			var reader = new FileReader();
			reader.onload = function(e){
				$('.imgs').attr('src', e.target.result);
			}
			reader.readAsDataURL(input.files[0]);
			}
	}
	$('.organizationlogo').change(function(){
		preview(this);
	})
</script>
@endsection