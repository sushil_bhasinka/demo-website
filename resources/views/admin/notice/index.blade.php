@extends('layouts.adminlayouts.app')
@section('content')
<!-- start page content -->
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<div class="page-title-breadcrumb">
				<div class=" pull-left">
					<div class="page-title">Notice Setup</div>
				</div>
				<ol class="breadcrumb page-breadcrumb pull-right">
					<li><a class="parent-item"
							href="/home"><i class="fa fa-home"></i></a>&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li>Settings&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li class="active">Notice Setup</li>
				</ol>
			</div>
		</div>
		<input type="hidden" name="" value="/admin/noticesetup" class="activeurl">
		<div class="row">
			<div class="col-md-12">
				<div class="card card-topline-green">
					<div class="card-head">
						<header>Notice Detail</header>
						<div class="tools">
							<a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
							<a class="t-collapse btn-color fa fa-chevron-down"
								href="javascript:;"></a>
						</div>
					</div>
					<div class="card-body " id="savetoimage">
						
							@if (session('msg'))
		                        <div class="tstSuccess msg" style="display: none;">
		                            {{ session('msg') }}
		                        </div>
                    		@endif
                    		@if (session('error'))
		                        <div class="tstError error" style="display: none;">
		                            {{ session('error') }}
		                        </div>
                    		@endif
						<div class="row col-md-12">
							 <!-- Button trigger modal -->
							<button type="button" class="btn btn-circle btn-success" data-toggle="modal" data-target="#addnotice"><i class="fa fa-plus"></i> Add </button>
					
							<!-- Modal -->
							<div class="modal fade" id="addnotice" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
							  <div class="modal-dialog modal-lg" role="document">
							  	<form action="{{ route('noticesetup.store') }}" method="POST" enctype="multipart/form-data">
							  		@csrf
								   	<div class="modal-content card-box">
										<div class="modal-header">
											<header>Add Notice Detail</header>
										</div>
								   		<div class="row">
											<div class="card-body row form-body">
												<div class="col-lg-4 p-t-20">
													<div class="form-label-group">
														<label class="control-label" for="publishdate">Publish Date
														</label>
														<input type="text" name="publishdate" id="publishdate" class="form-control">
													</div>
												</div>
												<div class="col-lg-4 p-t-20">
													<div class="form-label-group">
														<label class="control-label" for="expirydate">Expiry Date
														</label>
														<input type="text" name="expirydate" id="expirydate" class="form-control" >
													</div>
												</div>
												<div class="col-lg-4 p-t-20">
													<div class="form-label-group">
														<label class="control-label" for="title">Notice Title
														</label>
														<input type="text" name="title" id="title" class="form-control" placeholder="Notice Title">
													</div>
												</div>
												<div class="col-lg-12 p-t-20">
													<div class="form-label-group">
														<label class="control-label" for="description">Notice Description
														</label>
														<textarea name="description" id="description" class="form-control" placeholder="Notice Description"></textarea>
													</div>
												</div>
												<div class="row col-lg-12 p-t-20">
													<div class="col-lg-8"> 
														<div class="form-label-group">
															<label class="control-label" for="image"> Image
															</label>
															<input type="file" name="image" id="image" class="image form-control">
														</div>
													</div>
													<div class="col-lg-4"> 
														<div class="form-label-group">
															<img src="" class="imgs" height="100" width="250">
														</div>
													</div>
												</div>
												<div class="col-lg-12 p-t-20">
													<div class="form-label-group">
														<label class="control-label" for="video">Video Link
														</label>
														<input type="text" name="video" id="video" class="form-control" placeholder="https://www.youtube.com/watch?v=">
													</div>
												</div>
											</div>
											<div class="col-lg-12 p-t-20 p-b-20 text-center">
												<button type="button" class="btn btn-circle btn-secondary" data-dismiss="modal">Close</button>
									    		<button type="submit" class="btn btn-circle btn-success save">Save changes</button>
											</div>
										</div>
									</div>
								</form>
							  </div>
							</div>
							<!-- Modal Ends -->
						</div>
						<div class="table-scrollable ">
							<table class="table display" width="100%">
								<thead class="text-center">
									<tr>
										<th>S.N</th>
										<th>Title</th>
										<th>Description</th>
										<th>Image</th>
										<th>Video Link</th>
										<th>Publish Date</th>
										<th>Expiry Date</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									@php
									$i=1;
									@endphp
									@foreach($notices as $notice)
									<tr class="text-center">
										<td>{{$i++}}</td>
										<td>{{ $notice->title }}</td>
										<td>{{ $notice->description }}</td>
										<td><object data="{{'data:image/jpg;base64,'.$notice->image}}" style="height:80px;width:130px"></object></td>
										<td>{{ $notice->video }}</td>
										<td>{{ $notice->publishdate }}</td>
										<td>{!! $notice->expirydate !!}</td>
										<td class="text-left">
											<!-- Button trigger modal -->
											<a href="" data-toggle="modal" data-target="#edit{{$notice->id}}">
												<i class="material-icons text-warning">edit</i>
											</a>
											<!-- Modal -->
											<div class="modal fade" id="edit{{ $notice->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
											  <div class="modal-dialog modal-lg" role="document">
											  	<form action="{{ url('admin/noticesetup/'.$notice->id) }}" method="POST" enctype="multipart/form-data">
											  		@csrf
											  		@method('put')
												   	<div class="modal-content card-box">
														<div class="modal-header">
															<header>Edit Notice Detail</header>
														</div>
												   		<div class="row">
															<div class="card-body row form-body"><div class="col-lg-4 p-t-20">
													<div class="form-label-group">
														<label class="control-label" for="publishdate">Publish Date
														</label>
														<input type="text" name="publishdate" id="publishdate" class="form-control" value="{{ $notice->publishdate }}">
													</div>
												</div>
												<div class="col-lg-4 p-t-20">
													<div class="form-label-group">
														<label class="control-label" for="expirydate">Expiry Date
														</label>
														<input type="text" name="expirydate" id="expirydate" class="form-control" value="{{ $notice->expirydate }}">
													</div>
												</div>
												<div class="col-lg-4 p-t-20">
													<div class="form-label-group">
														<label class="control-label" for="title">Notice Title
														</label>
														<input type="text" name="title" id="title" class="form-control" placeholder="Notice Title" value="{{ $notice->title }}"> 
													</div>
												</div>
												<div class="col-lg-12 p-t-20">
													<div class="form-label-group">
														<label class="control-label" for="description">Notice Description
														</label>
														<textarea name="description" id="description" class="form-control" placeholder="Notice Description">{{ $notice->description }}</textarea>
													</div>
												</div>
												<div class="row col-lg-12 p-t-20">
													<div class="col-lg-8"> 
														<div class="form-label-group">
															<label class="control-label" for="image"> Image
															</label>
															<input type="file" name="image" id="image" class="image form-control">
														</div>
													</div>
													<div class="col-lg-4"> 
														<div class="form-label-group">
															<img src="{{'data:image/jpg;base64,'.$notice->image}}" class="imgs" height="100" width="250">
														</div>
													</div>
												</div>
												<div class="col-lg-12 p-t-20">
													<div class="form-label-group">
														<label class="control-label" for="video">Video Link
														</label>
														<input type="text" name="video" id="video" class="form-control" placeholder="https://www.youtube.com/watch?v=" value="{{ $notice->video }}">
													</div>
												</div>
															</div>
															<div class="col-lg-12 p-t-20 p-b-20 text-center">
																<button type="button" class="btn btn-circle btn-secondary" data-dismiss="modal">Close</button>
													    		<button type="submit" class="btn btn-circle btn-success save">Update</button>
															</div>
														</div>
													</div>
												</form>
											  </div>
											</div>
											<!-- Modal Ends -->
											<a href="" data-toggle="modal" data-target="#delete{{$notice->id}}">
												<i class="material-icons text-danger">delete</i>
											</a>
											<!-- Modal -->
											<div class="modal fade" id="delete{{ $notice->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
											  <div class="modal-dialog modal-dialog-centered" role="document">
											  	<form action="{{ url('admin/noticesetup/'.$notice->id) }}" method="POST">
											  		@csrf
											  		@method('delete')
												   	<div class=" modal-content card-box">
												   		<div class="row">
														<div class="col-sm-12">
														<div class="card-head">
															<header>Delete Notice Detail</header>
														</div>
														<div class="card-body row ml-5">
															<h4 class="text-white">Do you want to delete {{$notice->title}}??</h4>
															<h4 class="text-white">Can't Revert it!!!</h4>
														</div>
														<div class="col-lg-12 p-t-20 p-b-20 text-center">
															<button type="button" class="btn btn-circle btn-secondary" data-dismiss="modal">Close</button>
												    		<button type="submit" class="btn btn-circle btn-danger">Delete</button>
														</div>
														</div>
														</div>
													</div>
												</form>
											  </div>
											</div>
											<!-- Modal Ends -->
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- end page content -->
@endsection
@section('script')
<script type="text/javascript">
	function preview(input){
		if(input.files && input.files[0]){
			var reader = new FileReader();
			reader.onload = function(e){
				$('.imgs').attr('src', e.target.result);
			}
			reader.readAsDataURL(input.files[0]);
			}
	}
	$('.image').change(function(){
		preview(this);
	})
</script>
@endsection