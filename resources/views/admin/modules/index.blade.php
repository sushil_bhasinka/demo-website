@extends('layouts.adminlayouts.app')
@section('content')
<!-- start page content -->
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<div class="page-title-breadcrumb">
				<div class=" pull-left">
					<div class="page-title">Module Setup</div>
				</div>
				<ol class="breadcrumb page-breadcrumb pull-right">
					<li><a class="parent-item"
							href="/home"><i class="fa fa-home"></i></a>&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li>Settings&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li class="active">Modules</li>
				</ol>
			</div>
		</div>
		<input type="hidden" name="" value="/admin/module" class="activeurl">
		<div class="row">
			<div class="col-md-12">
				<div class="card card-topline-green">
					<div class="card-head">
						<header>Module Setup</header>
						<div class="tools">
							<a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
							<a class="t-collapse btn-color fa fa-chevron-down"
								href="javascript:;"></a>
						</div>
					</div>
					<div class="card-body ">
						
							@if (session('msg'))
		                        <div class="tstSuccess msg" style="display: none;">
		                            {{ session('msg') }}
		                        </div>
                    		@endif
                    		@if (session('error'))
		                        <div class="tstError error" style="display: none;">
		                            {{ session('error') }}
		                        </div>
                    		@endif
						<div class="row col-md-12">
							 <!-- Button trigger modal -->
							<button type="button" class="btn btn-circle btn-success" data-toggle="modal" data-target="#addmodule"><i class="fa fa-plus"></i> Add </button>
							<!-- Modal -->
							<div class="modal fade" id="addmodule" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
							  <div class="modal-dialog modal-dialog-centered" role="document">
							  	<form action="{{ route('module.store') }}" method="POST">
							  		@csrf
								   	<div class=" modal-content card-box">
								   		<div class="row">
											<div class="col-sm-12">
										<div class="card-head">
											<header>Add Module</header>
										</div>
										<div class="card-body row">
											<div class="col-lg-6 p-t-20">
											<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
												<input class="mdl-textfield__input" type="text" id="modulename" name="modulename" required="true">
												<label class="mdl-textfield__label" for="modulename">Module Name</label>
												<span class="mdl-textfield__error" >Module Name Required!</span>
											</div>
											</div>
											<div class="col-lg-6 p-t-20">
												<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
													<input class="mdl-textfield__input" type="number"
														pattern="-?[0-9]*(\.[0-9]+)?" id="moduleorder" name="moduleorder" required="true">
													<label class="mdl-textfield__label" for="moduleorder">Module Priority</label>
													<span class="mdl-textfield__error">Number required!</span>
												</div>
											</div>
										</div>
										<div class="col-lg-12 p-t-20 text-center">
											<button type="button" class="btn btn-circle btn-secondary" data-dismiss="modal">Close</button>
								    		<button type="submit" class="btn btn-circle btn-success">Save changes</button>
										</div>
										</div>
										</div>
									</div>
								</form>
							  </div>
							</div>
							<!-- Modal Ends -->
						</div>
						<div class="table-scrollable ">
							<table class="table display" width="100%">
								<thead class="text-center">
									<tr>
										<th>S.N</th>
										<th>Module Name</th>
										<th>Module Priority</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									@php
									$i=1;
									@endphp
									@foreach($modules as $module)
									<tr class="text-center">
										<td>{{ $i++ }}</td>
										<td>{{ $module->modulename }}</td>
										<td>{{ $module->moduleorder }}</td>
										<td>
											<!-- Button trigger modal -->
											<a href="" data-toggle="modal" data-target="#edit{{$module->moduleid}}">
												<i class="material-icons text-warning">edit</i>
											</a>
											<!-- Modal -->
											<div class="modal fade" id="edit{{ $module->moduleid }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
											  <div class="modal-dialog modal-dialog-centered" role="document">
											  	<form action="{{ url('admin/module/'.$module->moduleid) }}" method="POST">
											  		@csrf
											  		@method('put')
												   	<div class=" modal-content card-box">
												   		<div class="row">
														<div class="col-sm-12">
														<div class="card-head">
															<header>Edit Module</header>
														</div>
														<div class="card-body row">
															<div class="col-lg-6 p-t-20">
															<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
																<input class="mdl-textfield__input" type="text" id="modulename" name="modulename" required="true" value="{{ $module->modulename }}">
																<label class="mdl-textfield__label" for="modulename">Module Name</label>
																<span class="mdl-textfield__error" >Module Name Required!</span>
															</div>
															</div>
															<div class="col-lg-5 p-t-20">
																<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
																	<input class="mdl-textfield__input" type="number"
																		pattern="-?[0-9]*(\.[0-9]+)?" id="moduleorder" name="moduleorder" required="true"  value="{{ $module->moduleorder }}">
																	<label class="mdl-textfield__label" for="moduleorder">Module Priority</label>
																	<span class="mdl-textfield__error">Number required!</span>
																</div>
															</div>
														</div>
														<div class="col-lg-12 p-t-20 text-center">
															<button type="button" class="btn btn-circle btn-secondary" data-dismiss="modal">Close</button>
												    		<button type="submit" class="btn btn-circle btn-success">Update changes</button>
														</div>
														</div>
														</div>
													</div>
												</form>
											  </div>
											</div>
											<!-- Modal Ends -->
											<a href="" data-toggle="modal" data-target="#delete{{$module->moduleid}}">
												<i class="material-icons text-danger">delete</i>
											</a>
											<!-- Modal -->
											<div class="modal fade" id="delete{{ $module->moduleid }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
											  <div class="modal-dialog modal-dialog-centered" role="document">
											  	<form action="{{ url('admin/module/'.$module->moduleid) }}" method="POST">
											  		@csrf
											  		@method('delete')
												   	<div class=" modal-content card-box">
												   		<div class="row">
														<div class="col-sm-12">
														<div class="card-head">
															<header>Delete Module</header>
														</div>
														<div class="card-body row">
															<h4 class="text-white">Do you want to delete {{$module->modulename}} Module??</h4>
															<h5 class="text-white">Can't Revert it!!!</h5>
														</div>
														<div class="col-lg-12 p-t-20 text-center">
															<button type="button" class="btn btn-circle btn-secondary" data-dismiss="modal">Close</button>
												    		<button type="submit" class="btn btn-circle btn-danger">Delete</button>
														</div>
														</div>
														</div>
													</div>
												</form>
											  </div>
											</div>
											<!-- Modal Ends -->
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
							{{ $modules->links() }}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- end page content -->
@endsection