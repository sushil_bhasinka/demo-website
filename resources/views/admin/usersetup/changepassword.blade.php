@extends('layouts.adminlayouts.app')
@section('content')
<!-- start page content -->
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<div class="page-title-breadcrumb">
				<div class=" pull-left">
					<div class="page-title">Change Password</div>
				</div>
				<ol class="breadcrumb page-breadcrumb pull-right">
					<li><a class="parent-item"
							href="/home"><i class="fa fa-home"></i></a>&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li class="active">Change Password</li>
				</ol>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="card card-topline-green">
					<div class="card-head">
						<header>Change Password</header>
						<div class="tools">
							<a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
							<a class="t-collapse btn-color fa fa-chevron-down"
								href="javascript:;"></a>
						</div>
					</div>
					<div class="card-body " id="savetoimage">
						@if (session('msg'))
	                        <div class="tstSuccess msg" style="display: none;">
	                            {{ session('msg') }}
	                        </div>
	            		@endif
	            		@if (session('error'))
	                        <div class="tstError error" style="display: none;">
	                            {{ session('error') }}
	                        </div>
	            		@endif
	            		<form action="{{ route('usersetup.changepassword') }}" method="GET">
	            			@csrf
	            		<label> Old Password : </label>
						<input class = 'form-control' type = 'password' name = 'oldpassword' required/>
						<label>New Password : </label>
						<input class = 'form-control' type = 'password' name = 'password' id='password'required />
						<label>Confirm Password : </label>
						<input class = 'form-control' type = 'password' name = 'password_confirmation' id='password_confirmation' required />
						<div id='message'></div>
						<br>
						<input type = 'submit' class = 'btn btn-success'  value="Save" />
					 </form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- end page content -->
@endsection
@section('script')
<script type="text/javascript">

$('#password_confirmation').on('keyup', function () {
          //console.log($('#password').val()+'working');
	if(($('#password').val()).length!= 0)
	{
	  if ( $('#password_confirmation').val()== $('#password').val()) 
	  {
	    $('#message').html('Password Match').css('color', 'green');
	  } 
	  else
	  {
	    $('#message').html('Password Does Not Match').css('color', 'red');
	  } 
    }
    else
    {
      document.getElementById('message').innerHTML='';
    }
});
</script>
@endsection