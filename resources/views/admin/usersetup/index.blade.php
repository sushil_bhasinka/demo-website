@extends('layouts.adminlayouts.app')
@section('content')
<!-- start page content -->
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<div class="page-title-breadcrumb">
				<div class=" pull-left">
					<div class="page-title">User Setup</div>
				</div>
				<ol class="breadcrumb page-breadcrumb pull-right">
					<li><a class="parent-item"
							href="/home"><i class="fa fa-home"></i></a>&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li>Settings&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li class="active">User Setup</li>
				</ol>
			</div>
		</div>
		<input type="hidden" name="" value="/admin/usersetup" class="activeurl">
		<div class="row">
			<div class="col-md-12">
				<div class="card card-topline-green">
					<div class="card-head">
						<header>User Detail</header>
						<div class="tools">
							<a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
							<a class="t-collapse btn-color fa fa-chevron-down"
								href="javascript:;"></a>
						</div>
					</div>
					<div class="card-body">
							@if(session('success'))
		                        <div class="tstSuccess msg" style="display: none;">
		                            {{ session('success') }}
		                        </div>
                    		@endif
                    		@if(session('error'))
		                        <div class="tstError error" style="display: none;">
		                            {{ session('error') }}
		                        </div>
                    		@endif
						<div class="row col-md-12">
							<a href="{{ route('usersetup.create') }}"><button type="button" class="btn btn-circle btn-success"><i class="fa fa-plus"></i> Add </button></a>
						</div>
						<div class="table-scrollable ">
							<table class="table display" width="100%">
								<thead class="text-center">
									<tr>
										<th>Name</th>
										<th>Email</th>
										<th>Phone</th>
										<th>Expiry Date</th>
										<th>Image</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									@foreach($users as $user)
									<tr class="text-center">
										<td>{{ $user->name }}</td>
										<td>{{ $user->email }}</td>
										<td>{{ $user->contactno }}</td>
										<td>{{ $user->expirydate }}</td>
										<td><object data="{{'data:image/jpg;base64,'.$user->image}}" style="height:80px;width:130px"></object></td>
										<td class="text-left">
											<!-- Button trigger modal -->
											<a href="{{ url('admin/usersetup/'.$user->id.'/edit') }}">
												<i class="material-icons text-warning">edit</i>
											</a>
											<a href="" data-toggle="modal" data-target="#delete{{$user->id}}">
												<i class="material-icons text-danger">delete</i>
											</a>
											<!-- Modal -->
											<div class="modal fade" id="delete{{ $user->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
											  <div class="modal-dialog modal-dialog-centered" role="document">
											  	<form action="{{ url('admin/usersetup/'.$user->id) }}" method="POST">
											  		@csrf
											  		@method('delete')
												   	<div class=" modal-content card-box">
												   		<div class="row">
														<div class="col-sm-12">
														<div class="card-head">
															<header>Delete User</header>
														</div>
														<div class="card-body row ml-5">
															<h4 class="text-white">Do you want to delete {{$user->name}}??</h4>
															<h4 class="text-white">Can't Revert it!!!</h4>
														</div>
														<div class="col-lg-12 p-t-20 p-b-20 text-center">
															<button type="button" class="btn btn-circle btn-secondary" data-dismiss="modal">Close</button>
												    		<button type="submit" class="btn btn-circle btn-danger">Delete</button>
														</div>
														</div>
														</div>
													</div>
												</form>
											  </div>
											</div>
											<!-- Modal Ends -->
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- end page content -->
@endsection
@section('script')

@endsection