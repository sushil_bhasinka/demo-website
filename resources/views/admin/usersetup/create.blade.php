@extends('layouts.adminlayouts.app')
@section('content')
<!-- start page content -->
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<div class="page-title-breadcrumb">
				<div class=" pull-left">
					<div class="page-title">User Setup</div>
				</div>
				<ol class="breadcrumb page-breadcrumb pull-right">
					<li><a class="parent-item"
							href="/home"><i class="fa fa-home"></i></a>&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li>Settings&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li class="active">User Setup</li>
				</ol>
			</div>
		</div>
		<input type="hidden" name="" value="/admin/usersetup" class="activeurl">
		<div class="row">
			<div class="col-md-12">
				<div class="card card-topline-green">
					<div class="card-head">
						<header>User Detail</header>
						<div class="tools">
							<a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
							<a class="t-collapse btn-color fa fa-chevron-down"
								href="javascript:;"></a>
						</div>
					</div>
					<div class="card-body">
					  	<form action="{{ route('usersetup.store') }}" method="POST" enctype="multipart/form-data">
					  		@csrf
					  		<div class="row">
					  			<div class="pull-left col-lg-6">
					  				<div class="card">
					  					<div class="card-header d-flex justify-content-between">
					                        <div class="header-title">
					                           <h4 class="card-title">User Information</h4>
					                        </div>
					                     </div>
					                     <div class="card-body row form-body">
											<div class="col-lg-12 p-t-20">
												<label class="control-label" for="name">Full Name</label>
												<input type="text" name="name" id="name" placeholder="Enter Full Name" class="form-control" required="" />
											</div>
											<div class="col-lg-12 p-t-20">
												<div class="form-label-group">
													<label class="control-label" for="useremail">Email
													</label>
													<input type="email" name="useremail" id="useremail" placeholder="abc@____.com" class="form-control" required=""/>
												</div>
											</div>
											<div class="col-lg-12 p-t-20">
												<div class="form-label-group">
													<label class="control-label" for="password">Password
													</label>
													<input type="password" name="password" id="password" placeholder="Enter Password" class="form-control password" required=""/>
												</div>
											</div>
											<div class="col-lg-12 p-t-20">
												<div class="form-label-group">
													<label class="control-label" for="confirmpassword">Confirm Password
													</label>
													<input type="password" name="confirmpassword" id="confirmpassword" placeholder="Enter Confirm Password" class="form-control confirmpassword" required=""/>
													<div class="confirmpassword_error text-danger"></div>
													<div class="confirmpassword_success text-success"></div>
												</div>
											</div>
											<div class="col-lg-12 p-t-20">
												<div class="form-label-group">
													<label class="control-label" for="contactno">Contact Number
													</label>
													<input type="text" name="contactno" id="contactno" placeholder="Enter Contact number" class="form-control" pattern="-?[0-9]*(\.[0-9]+)?" required=""/>
												</div>
											</div>
											<div class="col-lg-12 p-t-20">
												<div class="form-label-group">
													<label class="control-label" for="expirydate">Expiry Date
													</label>
													<input type="date" name="expirydate" id="expirydate" placeholder="Enter Expiry Date" class="form-control" required=""/>
												</div>
											</div>
											<div class="row col-lg-12 p-t-20">
												<div class="col-lg-8"> 
													<div class="form-label-group">
														<label class="control-label" for="image">User Image
														</label>
														<input type="file" name="image" id="image" class="image form-control" required="">
													</div>
												</div>
												<div class="col-lg-12"> 
													<div class="form-label-group">
														<img src="" class="imgs" height="100" width="150">
													</div>
												</div>
											</div>
										</div>
					  				</div>
					  			</div>
					  			<div class="pull-right col-lg-6">
					  				<div class="card">
										<div class="card-header d-flex justify-content-between">
					                        <div class="header-title">
					                           <h4 class="card-title">User Access</h4>
					                        </div>
					                    </div>					  					 
				                        <div class="card-body row form-body">
				                        	<div class="col-lg-7">
				                        		<div class="modal-header">
													<header>Access Level</header>
												</div>
				                        		<div class="checkboxes p-t-20">
													@foreach ($module as $mod)
													<ul>
														<li>
															<input type='checkbox' name="modules[]" value="{{ $mod->moduleid }}" class="parent" /> {{ $mod->modulename }}
															@foreach ($submodules as $submodule)
															<ul>
																@if ( $mod->moduleid == $submodule->moduleid)
																<li>
																	<input type='checkbox' name="submodules[]" class='child' value="{{ $submodule->submoduleid }}" /> {{ $submodule->submodulename }}
																</li>
																@endif
															</ul>
															@endforeach
														</li>
													</ul>
													@endforeach
												</div>
				                        	</div>
				                        	<div class="col-lg-5">
												<div class="modal-header">
													<header>Access Level</header>
												</div>
												<div class="checkebox p-t-20">
													<ul>
														@foreach ($accesslevels as $access)
														<li>
															<input type="checkbox" name="accesslevel[]" value="{{ $access->accesslevelid }}" class="checkbox"> {{ $access->accesslevel }}
														</li>
														@endforeach
													</ul>
												</div>
											</div>
				                        </div>
					  				</div>
					  			</div>
								<div class="col-lg-12 p-t-20 p-b-20 text-center">
						    		<button type="submit" class="btn btn-circle btn-success save">Save changes</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- end page content -->
@endsection
@section('script')

<script type="text/javascript">
	function preview(input){
		if(input.files && input.files[0]){
			var reader = new FileReader();
			reader.onload = function(e){
				$('.imgs').attr('src', e.target.result);
			}
			reader.readAsDataURL(input.files[0]);
			}
	}
	$('.image').change(function(){
		preview(this);
	})
</script>
<script type="text/javascript">
$(document).ready(function(){
	$(document).on('keyup','.confirmpassword',function(){
		var password=$('.password').val();
		var confirmpassword=$('.confirmpassword').val();
		console.log(password.toUpperCase() )
		console.log(confirmpassword.toUpperCase())
		if(password.toUpperCase() != confirmpassword.toUpperCase())
		{
			$('.confirmpassword_error').html("Password doesnot Matched!!!");
			$('.confirmpassword_success').html(" ");
            $('.save').attr('disabled',true);
		}
		else
		{
			$('.confirmpassword_success').html("Password Matched!!!");
			$('.confirmpassword_error').html(" ");
            $('.save').attr('disabled',false);
		}
	})
})
</script>
<script>
$('.checkbox').click(function(){
    $('.checkbox').each(function(){
        $(this).prop('checked', false); 
    }); 
    $(this).prop('checked', true);
});

$('input[type="checkbox"]').change(function(e) {
  var checked = $(this).prop("checked"),
      container = $(this).parent(),
      siblings = container.siblings();
  container.find('input[type="checkbox"]').prop({
    indeterminate: false,
    checked: checked
  });
  function checkSiblings(el) {
    var parent = el.parent().parent(),
        all = true;
    el.siblings().each(function() {
      return all = ($(this).children('input[type="checkbox"]').prop("checked") === checked);
    });
    console.log(all);
    if (all && checked) {
      parent.children('input[type="checkbox"]').prop({
        indeterminate: false,
        checked: checked
      });
      checkSiblings(parent);
    } else if (all && !checked) {
      parent.children('input[type="checkbox"]').prop("checked", checked);
      parent.children('input[type="checkbox"]').prop("checked", (parent.find('input[type="checkbox"]:checked').length > 0));
      checkSiblings(parent);
    } else {
      el.parents("li").children('input[type="checkbox"]').prop({
        indeterminate: true,
        checked: true
      });
    }
  }
  checkSiblings(container);
});
</script>
@endsection