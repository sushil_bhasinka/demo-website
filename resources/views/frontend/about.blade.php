@extends('layouts.frontendlayout.app')
@section('content')

    <!-- Breadcrumb Section Begin -->
    @if(count($backgroundimage))
    @foreach($backgroundimage as $bgimage)
    <section class="breadcrumb-section set-bg" data-setbg="{{'data:image/jpg;base64,'.$bgimage->image}}">
    @endforeach
    @else
    <section class="breadcrumb-section set-bg" data-setbg="frontend/img/breadcrumb-bg.jpg">
    @endif
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb-text">
                        <h2>About us</h2>
                        <div class="bt-option">
                            <a href="./index.html">Home</a>
                            <span>About</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Section End -->

    <!-- ChoseUs Section Begin -->
    @include('frontend.partials.aboutus')
    <!-- ChoseUs Section End -->

    <!-- About US Section Begin -->
    <section class="aboutus-section">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6 p-0">
                @foreach($descriptions as $description)
                    <div class="about-video set-bg" data-setbg="{{'data:image/jpg;base64,'.$description->image}}">
                        <a href="https://www.youtube.com/watch?v=EzKkl64rRbM" class="play-btn video-popup"><i
                                class="fa fa-caret-right"></i></a>
                    </div>
                @endforeach
                </div>
                <div class="col-lg-6 p-0">
                    <div class="about-text">
                        <div class="section-title">
                            <span>About Us</span>
                            <h2>What we have done</h2>
                        </div>
                        <div class="at-desc">
                            @foreach($descriptions as $description)
                            <p class="text-justify">{{ $description->description }}</p>
                            @endforeach
                        </div>
                        <div class="about-bar">
                            @foreach($progressbar as $key=>$bar)
                            <div class="ab-item">
                                <p>{{ $bar->title }}</p>
                                <div id="bar{{$key+1 }}" class="barfiller">
                                    <span class="fill" data-percentage="{{ $bar->progresspercentage }}"></span>
                                    <div class="tipWrap">
                                        <span class="tip"></span>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- About US Section End -->

    <!-- Team Section Begin -->
    @include('frontend.partials.ourteam')
    <!-- Team Section End -->

    <!-- Banner Section Begin -->
    @include('frontend.partials.banner')
    <!-- Banner Section End -->

    <!-- Testimonial Section Begin -->
    @include('frontend.partials.testimonial')
    <!-- Testimonial Section End -->
@endsection