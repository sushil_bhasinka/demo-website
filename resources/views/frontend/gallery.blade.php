@extends('layouts.frontendlayout.app')
@section('content')

    <!-- Breadcrumb Section Begin -->
    @if(count($galleries) >= 6)
    <section class="breadcrumb-section set-bg" data-setbg="{{'data:image/jpg;base64,'.$galleries[5]->image}}">
    @else
    <section class="breadcrumb-section set-bg" data-setbg="frontend/img/breadcrumb-bg.jpg">
    @endif
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb-text">
                        <h2>Gallery</h2>
                        <div class="bt-option">
                            <a href="./index.html">Home</a>
                            <a href="#">Pages</a>
                            <span>Gallery</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Section End -->

    <!-- Gallery Section Begin -->
    <div class="gallery-section gallery-page">
        <div class="gallery">
            <div class="grid-sizer"></div>
            @foreach($galleries as $key=>$gallery)
            <div class="gs-item  @if($loop->iteration === 1 || $loop->iteration === 6 || $loop->iteration === 7) grid-wide @endif set-bg" data-setbg="{{'data:image/jpg;base64,'.$gallery->image}}">
                <a href="{{'data:image/jpg;base64,'.$gallery->image}}" class="thumb-icon image-popup"><i class="fa fa-picture-o"></i></a>
            </div>
            @endforeach
        </div>
    </div>
    <!-- Gallery Section End -->
@endsection
   