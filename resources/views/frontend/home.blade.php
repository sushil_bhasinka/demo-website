@extends('layouts.frontendlayout.app')
@section('content')

 	@include('frontend.partials.slider')
    @include('frontend.partials.aboutus')
    @include('frontend.partials.class')
    @include('frontend.partials.banner')
    @include('frontend.partials.ourplan')
    @include('frontend.partials.gallery')
    @include('frontend.partials.ourteam')

@endsection