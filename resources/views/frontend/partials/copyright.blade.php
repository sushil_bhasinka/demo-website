<div id="copyright">
    <div class="container">
        <div class="row">
            <!-- Copyright Text Start -->
            <div class="copyright-text col-md-6">
                Powered by WordPress - Built by <a href="http://themesawesome.com/">Themes Awesome</a>            </div>
            <!-- Copyright Text End -->

            <!-- Social LInks Start -->
            <div class="social-links col-md-6">
                <ul class="no-padding">
	                        <li class="twitter soc-icon"><a href="http://twitter.com/#" class="fa fa-twitter"></a></li>
          <li class="facebook soc-icon"><a href="http://facebook.com/#" class="fa fa-facebook"></a></li>
          <li class="dribble soc-icon"><a href="https://dribbble.com/" class="fa fa-dribbble"></a></li>
          <li class="instagram soc-icon"><a href="https://instagram.com/" class="fa fa-instagram"></a></li>
          <li class="tumblr soc-icon"><a href="https://www.tumblr.com/" class="fa fa-tumblr"></a></li>
          <li class="vimeo soc-icon"><a href="https://vimeo.com/" class="fa fa-vimeo-square"></a></li>
                    </ul>
            </div>
            <!-- Social Links End -->
        </div>
    </div>
</div>