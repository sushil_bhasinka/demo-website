<!-- Testimonial Section Begin -->
    <section class="testimonial-section spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <span>Testimonial</span>
                        <h2>Our cilent say</h2>
                        <a href="#" class="primary-btn btn-normal appoinment-btn" data-toggle="modal" data-target="#testimonial">Testimonial</a>
                        <div class="modal fade text-left" id="testimonial" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                              <div class="modal-dialog modal-lg" role="document">
                                <form action="{{ url('/testimonial') }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="modal-content card-box bg-dark text-white">
                                        <div class="modal-header">
                                            <header>YOUR OPINIONS</header>
                                        </div>
                                        <div class="row">
                                            <div class="card-body">
                                                <div class="row col-lg-12">
                                                    <div class="col-lg-4">
                                                        <input type="text" name="name" class="form-control bg-dark text-white" placeholder="Full Name">
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="form-group">
                                                            <input type="file" name="image" id="image" class="image form-control bg-dark text-white">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4"> 
                                                        <div class="form-group">
                                                            <img src="" class="imgs" height="100" width="250">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 form-group">
                                                        <input type="text" name="email" class="form-control bg-dark text-white" placeholder="Email">
                                                    </div>
                                                    <div class="col-lg-6 form-group">
                                                        <input type="text" name="status" class="form-control bg-dark text-white" placeholder="Occupational">
                                                    </div>
                                                    <div class="col-lg-12 form-group">
                                                        <textarea name="testimonial" class="form-control bg-dark text-white" placeholder="Your Opinion"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 pt-2 pb-2 text-center">
                                                <button type="submit" class="btn btn-circle btn-success">Submit</button>
                                                <button type="button" class="btn btn-circle btn-danger" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                              </div>
                            </div>
                    </div>
                </div>
            </div>
            <div class="ts_slider owl-carousel">
                @foreach($testimonials as $testimonial)
                <div class="ts_item">
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <div class="ti_pic">
                                @if($testimonial->image == null)
                                <img src="{{'data:image/jpg;base64,'.$testimonial->imagecode}}" alt="">
                                @else
                               <img src="{{'data:image/jpg;base64,'.$testimonial->image}}" alt="">
                                @endif
                            </div>
                            <div class="ti_text">
                                <p>{!! nl2br(e($testimonial->testimonial)) !!}</p>
                                <h5>{{ $testimonial->fullname }}</h5>
                                <p class="text-muted">{{ $testimonial->position }}</p>
                                <div class="tt-rating">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
               
            </div>
        </div>
    </section>
    <!-- Testimonial Section End -->
