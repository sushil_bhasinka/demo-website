   <!-- Offcanvas Menu Section Begin -->
    <div class="offcanvas-menu-overlay"></div>

    <div class="offcanvas-menu-wrapper">
        <div class="canvas-close">
            <i class="fa fa-close"></i>
        </div>
        <div class="canvas-search search-switch" style="background-color: black;">
            <a href="/">
                @foreach($orgdetails as $org)
                <img src="{{'data:image/jpg;base64,'.$org->logo}}" alt="">
                @endforeach
            </a>
        </div>
        <nav class="canvas-menu mobile-menu">
            <ul>
            	@foreach($menus as $menu)
	                <li class=""><a href="{{ $menu->menuurl }}">{{ $menu->menuname }}</a>
	                	@if(count($menu->submenu)>0)
	                    	<ul class="dropdown">
			                	@foreach($menu['submenu'] as $submenu)
				                    <li><a href="{{ $submenu->submenuurl }}">{{ $submenu->submenuname }}</a></li>
			                    @endforeach
	                    	</ul>
                    	@endif
	                </li>
                @endforeach
            </ul>
        </nav>
        <div id="mobile-menu-wrap"></div>
        <div class="canvas-social">
            @foreach($icons as $icon)
                <a href="{{ $icon->url }}">{!! $icon->icon !!}</a>
            @endforeach
        </div>
    </div>
    <!-- Offcanvas Menu Section End -->

    <!-- Header Section Begin -->
    <header class="header-section">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-3">
                    <div class="logo">
                        <a href="/">
                            @foreach($orgdetails as $org)
                                <img src="{{'data:image/jpg;base64,'.$org->logo}}" alt="">
                            @endforeach
                        </a>
                    </div>
                </div>
                <div class="col-lg-7">
                    <nav class="nav-menu">
                        <ul>
                            @foreach($menus as $menu)
				                <li><a href="{{ $menu->menuurl }}">{{ $menu->menuname }}</a>
				                	@if(count($menu->submenu)>0)
				                    	<ul class="dropdown">
						                	@foreach($menu['submenu'] as $submenu)
							                    <li><a href="{{ $submenu->submenuurl }}">{{ $submenu->submenuname }}</a></li>
						                    @endforeach
				                    	</ul>
			                    	@endif
				                </li>
			                @endforeach
                        </ul>
                    </nav>
                </div>
                <div class="col-lg-2">
                    <div class="top-option">
                        
                        <div class="to-social">
                            @foreach($icons as $icon)
                                <a href="{{ $icon->url }}">{!! $icon->icon !!}</a>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="canvas-open">
                <i class="fa fa-bars"></i>
            </div>
        </div>
    </header>
    <!-- Header End -->