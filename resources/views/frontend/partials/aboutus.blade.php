<!-- ChoseUs Section Begin -->
    <section class="choseus-section spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <span>Why chose us?</span>
                        @foreach($whychooseustitle as $title)
                        <h2>{{ $title->title }}</h2>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="row">
                @foreach($aboutus as $abt)
                <div class="col-lg-3 col-sm-6">
                    <div class="cs-item">
                        <span class="{{ $abt->iconclass }}"></span>
                        <h4>{{ $abt->title }}</h4>
                        <p>{{ $abt->description }}</p>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
    <!-- ChoseUs Section End -->