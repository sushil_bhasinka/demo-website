<!-- Gallery Section Begin -->
    <div class="gallery-section">
        <div class="gallery">
            <div class="grid-sizer"></div>
            @if(count($galleries) <= 5)
            @foreach($galleries as $key=>$value)
            <div class="gs-item @if($loop->iteration === 1 || $loop->iteration === 6) grid-wide @endif set-bg" data-setbg="{{'data:image/jpg;base64,'.$value->image}}">
                <a href="{{'data:image/jpg;base64,'.$value->image}}" class="thumb-icon image-popup"><i class="fa fa-picture-o"></i></a>
            </div>
            @endforeach
            @else
            <div class="gs-item grid-wide set-bg" data-setbg="{{'data:image/jpg;base64,'.$galleries[0]->image}}">
                <a href="{{'data:image/jpg;base64,'.$galleries[0]->image}}" class="thumb-icon image-popup"><i class="fa fa-picture-o"></i></a>
            </div>
            <div class="gs-item set-bg" data-setbg="{{'data:image/jpg;base64,'.$galleries[1]->image}}">
                <a href="{{'data:image/jpg;base64,'.$galleries[1]->image}}" class="thumb-icon image-popup"><i class="fa fa-picture-o"></i></a>
            </div>
            <div class="gs-item set-bg" data-setbg="{{'data:image/jpg;base64,'.$galleries[2]->image}}">
                <a href="{{'data:image/jpg;base64,'.$galleries[2]->image}}" class="thumb-icon image-popup"><i class="fa fa-picture-o"></i></a>
            </div>
            <div class="gs-item set-bg" data-setbg="{{'data:image/jpg;base64,'.$galleries[3]->image}}">
                <a href="{{'data:image/jpg;base64,'.$galleries[3]->image}}" class="thumb-icon image-popup"><i class="fa fa-picture-o"></i></a>
            </div>
            <div class="gs-item set-bg" data-setbg="{{'data:image/jpg;base64,'.$galleries[4]->image}}">
                <a href="{{'data:image/jpg;base64,'.$galleries[4]->image}}" class="thumb-icon image-popup"><i class="fa fa-picture-o"></i></a>
            </div>
            <div class="gs-item grid-wide set-bg" data-setbg="{{'data:image/jpg;base64,'.$galleries[5]->image}}">
                <a href="{{'data:image/jpg;base64,'.$galleries[5]->image}}" class="thumb-icon image-popup"><i class="fa fa-picture-o"></i></a>
            </div>
            @endif
        </div>
    </div>
    <!-- Gallery Section End -->
