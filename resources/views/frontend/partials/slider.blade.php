<!-- Hero Section Begin -->
    <section class="hero-section">
        <div class="hs-slider owl-carousel">
        	@foreach($sliders as $slider)
            <div class="hs-item set-bg" data-setbg="{{'data:image/jpg;base64,'.$slider->image}}">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 offset-lg-6">
                            <div class="hi-text">
                                <span>{{ $slider->captionone }}</span>
                                {!! $slider->captiontwo !!}
                                <a href="#" class="primary-btn">Get info</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </section>
    <!-- Hero Section End -->