 <!-- Google Font -->
    <link href="{{asset('/frontend/css/muli-fontfamily.css')}}">
    <link href="{{asset('/frontend/css/oswald-fontfamily.css')}}" rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="{{asset('/frontend/css/bootstrap.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('/frontend/css/font-awesome.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('/frontend/css/flaticon.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('/frontend/css/owl.carousel.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('/frontend/css/barfiller.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('/frontend/css/magnific-popup.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('/frontend/css/slicknav.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('/frontend/css/style.css')}}" type="text/css">