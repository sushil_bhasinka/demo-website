<!-- Team Section Begin -->
    <section class="team-section spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="team-title">
                        <div class="section-title">
                            <span>Our Team</span>
                            <h2>TRAIN WITH EXPERTS</h2>
                        </div>
                        <a href="#" class="primary-btn btn-normal appoinment-btn">appointment</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="ts-slider owl-carousel">
                	@foreach($ourteams as $team)
	                    <div class="col-lg-4">
	                        <div class="ts-item set-bg" data-setbg="{{'data:image/jpg;base64,'.$team->image}}">
	                            <div class="ts_text">
	                                <h4>{{ $team->name }}</h4>
	                                <span>{{ $team->position }}</span>
	                            </div>
	                        </div>
	                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    <!-- Team Section End -->