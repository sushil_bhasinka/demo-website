@extends('layouts.frontendlayout.app')
@section('content')

    <!-- Breadcrumb Section Begin -->
    @foreach($backgroundimage as $value)
    <section class="breadcrumb-section set-bg" data-setbg="{{'data:image/jpg;base64,'.$value->image}}">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb-text">
                        <h2>Our Team</h2>
                        <div class="bt-option">
                            <a href="/">Home</a>
                            <span>Our team</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endforeach
    <!-- Breadcrumb Section End -->

    <!-- Team Section Begin -->
    <section class="team-section team-page spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="team-title">
                        <div class="section-title">
                            <span>Our Team</span>
                            <h2>TRAIN WITH EXPERTS</h2>
                        </div>
                        <a href="#" class="primary-btn btn-normal appoinment-btn">appointment</a>
                    </div>
                </div>
            </div>
            <div class="row">
            	@foreach($ourteams as $team)
	                <div class="col-lg-4 col-sm-6">
	                    <div class="ts-item set-bg" data-setbg="{{'data:image/jpg;base64,'.$team->image}}">
	                        <div class="ts_text">
	                            <h4>{{ $team->name }}</h4>
	                            <span>{{ $team->position }}</span>
	                            <div class="tt_social text-white">
	                               {{ $team->message }}
	                            </div>
	                        </div>
	                    </div>
	                </div>
                @endforeach
            </div>
        </div>
    </section>
    <!-- Team Section End -->
@endsection