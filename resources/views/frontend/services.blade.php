@extends('layouts.frontendlayout.app')
@section('content')

    <!-- Breadcrumb Section Begin -->
    @foreach($backgroundimage as $bgimage)
    <section class="breadcrumb-section set-bg" data-setbg="{{'data:image/jpg;base64,'.$bgimage->image}}">
    @endforeach
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb-text">
                        <h2>Services</h2>
                        <div class="bt-option">
                            <a href="/">Home</a>
                            <span>Services</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Section End -->

    <!-- Services Section Begin -->
    <section class="services-section spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <span>What we do?</span>
                        <h2>PUSH YOUR LIMITS FORWARD</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                @php
                $i=1;
                $j=8;
                @endphp

                @foreach($services as $key=>$service)
                @if($key <= 1)
                    <div class="col-lg-3 order-lg-{{$i++}} col-md-6 p-0">
                        <div class="ss-pic">
                            <img src="{{'data:image/jpg;base64,'.$service->image}}" alt="">
                        </div>
                    </div>
                    <div class="col-lg-3 order-lg-{{$i++}} col-md-6 p-0">
                        <div class="ss-text">
                            <h4>{{ $service->title }}</h4>
                            <p>{{ $service->description }}</p>
                            <a href="#">Explore</a>
                        </div>
                    </div> 
                @else
                <div class="col-lg-3 order-lg-{{$j--}} col-md-6 p-0">
                        <div class="ss-pic">
                            <img src="{{'data:image/jpg;base64,'.$service->image}}" alt="">
                        </div>
                    </div>
                    <div class="col-lg-3 order-lg-{{$j--}} col-md-6 p-0">
                        <div class="ss-text">
                            <h4>{{ $service->title }}</h4>
                            <p>{{ $service->description }}</p>
                            <a href="#">Explore</a>
                        </div>
                    </div> 
                @endif              
                @endforeach
            </div>
        </div>
    </section>
    <!-- Services Section End -->

    <!-- Banner Section Begin -->
    @include('frontend.partials.banner')
    <!-- Banner Section End -->

    <!-- Pricing Section Begin -->
     @include('frontend.partials.ourplan')
    <!-- Pricing Section End -->
@endsection