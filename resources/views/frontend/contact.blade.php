@extends('layouts.frontendlayout.app')
@section('content')
    <!-- Breadcrumb Section Begin -->
    @foreach($contact as $contactus)
    <section class="breadcrumb-section set-bg" data-setbg="{{'data:image/jpg;base64,'.$contactus->image}}">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb-text">
                        <h2>Contact Us</h2>
                        <div class="bt-option">
                            <a href="/">Home</a>
                            <span>Contact us</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Section End -->

    <!-- Contact Section Begin -->
    <section class="contact-section spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="section-title contact-title">
                        <span>Contact Us</span>
                        <h2>GET IN TOUCH</h2>
                    </div>
                    
                    <div class="contact-widget">
                        <div class="cw-text">
                            <i class="fa fa-map-marker"></i>
                            <p>{{ $contactus->wardno }}, {{ $contactus->tole }}, {{ $contactus->municipality }}<br/> {{ $contactus->district }}</p>
                        </div>
                        <div class="cw-text">
                            <i class="fa fa-mobile"></i>
                            <ul>
                                <li>{{ $contactus->phone }}</li>
                            </ul>
                        </div>
                        <div class="cw-text email">
                            <i class="fa fa-envelope"></i>
                            <p>{{ $contactus->email }}</p>
                        </div>
                    </div>
                   
                </div>
                <div class="col-lg-6">
                    <div class="leave-comment">
                        <form action="{{ url('/contact') }}" method="post">
                            @csrf
                            <input type="text" placeholder="Name" name="fullname">
                            <input type="text" placeholder="Email" name="email">
                            <select class="form-control" name="subject">
                                <option>Choose Subject.....</option>
                                <option>Inquery</option>
                                <option>Suggestion</option>
                                <option>Message</option>
                                <option>Other</option>
                            </select><br>

                            <textarea placeholder="Message" name="message"></textarea>
                            <button type="submit">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="map">
                {!! $contactus->map !!}
            </div>
        </div>
    </section>
    @endforeach
    <!-- Contact Section End -->
@endsection