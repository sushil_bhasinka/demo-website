<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1" name="viewport" />
	<meta name="description" content="Responsive Admin Template" />
	<meta name="author" content="SmartUniversity" />
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>Smart University</title>
	@include('admin.partials.header')
</head>
<!-- END HEAD -->

<body
	class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-dark dark-sidebar-color logo-dark dark-theme">
	<div class="page-wrapper">
		@if(Auth::user())
		@include('admin.partials.navbar')
		@include('admin.partials.sidebar')
		@endif

		@yield('content')
		@if(Auth::user())
		@include('admin.partials.footer')
		@endif
	</div>
</body>
	@include('admin.partials.javascript')
	@yield('script')



</html>
