<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    @foreach($orgdetails as $detail)
    <title>{{ $detail->name }}</title>
    @endforeach
   
    <!-- bootstrap -->
    <link href="{{ asset('/admin/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- style -->
    <link rel="stylesheet" href="{{ asset('/admin/css/pages/extra_pages.css') }}">
</head>

<body>
    <div class="limiter">
        <div class="container-login100 page-background">
            <div class="wrap-login100">
                <form class="login100-form validate-form" method="POST" action="{{ route('login') }}">
                    @csrf
                    <span class="login100-form-logo">
                        @foreach($orgdetails as $detail)
                        <img alt="logo" src="{{'data:image/jpg;base64,'.$detail->logo}}">
                        @endforeach
                    </span>
                    <span class="login100-form-title p-b-34 p-t-27">
                       {{__("Log In")}}
                    </span>
                    <div class="wrap-input100 validate-input" data-validate="Enter Email">
                        <input class="input100" type="email" name="email">
                        <span class="focus-input100" data-placeholder="Email"></span>
                    </div>
                    <div class="wrap-input100 validate-input" data-validate="Enter password">
                        <input class="input100" type="password" name="password">
                        <span class="focus-input100"  data-placeholder="Password"></span>
                    </div>

                    <div class="contact100-form-checkbox">
                        <input class="input-checkbox100" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                        <label class="label-checkbox100" for="remember">
                            {{ __('Remember Me') }}
                        </label>
                    </div>
                    <div class="container-login100-form-btn">
                        <button type="submit" class="login100-form-btn">
                            {{ __('Login') }}
                        </button>
                    </div>
                    <div class="text-center p-t-30">
                        @if (Route::has('password.request'))
                            <a class="txt1" href="{{ route('password.request') }}">
                                {{ __('Forgot Your Password?') }}
                            </a>
                        @endif
                    </div>
                </form>
             
            </div>
        </div>
    </div>
    <!-- start js include path -->
    <script src="{{ asset('/admin/plugins/jquery/jquery.min.js') }}"></script>
    <!-- bootstrap -->
    <script src="{{ asset('/admin/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('/admin/js/pages/extra-pages/pages.js') }}"></script>
    <!-- end js include path -->
</body>
</html>