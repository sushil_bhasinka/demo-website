@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if (session('msg'))
                        <div class="alert alert-success" role="alert">
                            <pre>{{ session('msg') }}</pre>
                        </div>
                    @endif
                    <form action="{{url('/sendSMS')}}" method="POST">
                        <center>
                        <div class="row">
                            <div class="col-sm-12">
                                <label for="input-number">Sent To:</label>
                                <input type="text" name="number" id="input-number" class="form-control" placeholder="Enter Phone Number">
                            </div>
                        </div>
                        <div class="row">
                            
                            <div class="col-sm-12">
                                <label for="input-message">Message:</label>
                                <textarea class="form-control" placeholder="write some message" name="message"></textarea>
                            </div>
                        </div><br> 
                        @csrf
                        <input type="submit" name="send" value="Send" class="btn btn-sm btn-success">
                       </center>
                    </form>

                    <form action="{{ url('/getBalance') }}" method="POST">
                       @csrf
                        <input type="submit" name="getBalance" value="Get Balance" class="btn btn-sm btn-primary"> 
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
