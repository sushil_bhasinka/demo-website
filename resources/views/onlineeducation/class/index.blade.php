@extends('layouts.adminlayouts.app')
@section('content')
<!-- start page content -->
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<div class="page-title-breadcrumb">
				<div class=" pull-left">
					<div class="page-title">Class Setup</div>
				</div>
				<ol class="breadcrumb page-breadcrumb pull-right">
					<li><a class="parent-item"
							href="/home"><i class="fa fa-home"></i></a>&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li>Online Education Setup&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li class="active">Class Setup</li>
				</ol>
			</div>
		</div>
		<input type="hidden" name="" value="/admin/class" class="activeurl">
		<div class="row">
			<div class="col-md-12">
				<div class="card card-topline-green">
					<div class="card-head">
						<header>Class Setup</header>
						<div class="tools">
							<a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
							<a class="t-collapse btn-color fa fa-chevron-down"
								href="javascript:;"></a>
						</div>
					</div>
					<div class="card-body " id="savetoimage">
						
							@if (session('msg'))
		                        <div class="tstSuccess msg" style="display: none;">
		                            {{ session('msg') }}
		                        </div>
                    		@endif
                    		@if ($errors->any())
						    <div class="tstError error" style="display: none;">
						            @foreach ($errors->all() as $error)
						                {{ $error }}
						            @endforeach
						    </div>
							@endif
						<div class="row col-md-12">
							 <!-- Button trigger modal -->
							<button type="button" class="btn btn-circle btn-success" data-toggle="modal" data-target="#addclass"><i class="fa fa-plus"></i> Add </button>
							
					
							<!-- Modal -->
							<div class="modal fade" id="addclass" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
							  <div class="modal-dialog modal-lg" role="document">
							  	<form action="{{ route('class.store') }}" method="POST">
							  		@csrf
								   	<div class="modal-content card-box">
										<div class="modal-header">
											<header>Add Class Setup</header>
										</div>
								   		<div class="row">
											<div class="card-body row form-body">
												<div class="row col-lg-12 p-t-20">
													<div class="col-lg-6"> 
														<div class="form-label-group">
															<label class="control-label"> Class Name
															</label>
															<input type="text" name="classname" class="form-control" placeholder="Class Name">
														</div>
													</div>
													<div class="col-lg-6"> 
														<div class="form-label-group">
															<label class="control-label"> Class Ordering
															</label>
															<input type="text" name="classorder" class="form-control" placeholder="Class Ordering">
														</div>
													</div>
												</div>
											</div>
											<div class="col-lg-12 p-t-20 p-b-20 text-center">
												<button type="button" class="btn btn-circle btn-secondary" data-dismiss="modal">Close</button>
									    		<button type="submit" class="btn btn-circle btn-success save">Save changes</button>
											</div>
										</div>
									</div>
								</form>
							  </div>
							</div>
							<!-- Modal Ends -->
						</div>
						<div class="table-scrollable ">
							<table class="table display" width="100%">
								<thead class="text-center">
									<tr>
										<th>SN</th>
										<th>Class Name</th>
										<th>Class Order</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									@php
									$i=1;
									@endphp
									@foreach($classes as $class)
									<tr class="text-center">
										<td>{{ $i++ }}</td>
										<td class="text-left">{{ $class->classname }}</td>
										<td class="text-center">{{ $class->classorder }}</td>
										<td class="text-center">
											<!-- Button trigger modal -->
											<a href="" data-toggle="modal" data-target="#edit{{$class->classid}}">
												<i class="material-icons text-warning">edit</i>
											</a>
											<!-- Modal -->
											<div class="modal fade" id="edit{{ $class->classid }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
											  <div class="modal-dialog modal-lg" role="document">
											  	<form action="{{ url('admin/class/'.$class->classid) }}" method="POST">
											  		@csrf
											  		@method('put')
												   	<div class="modal-content card-box">
														<div class="modal-header">
															<header>Edit Class Setup</header>
														</div>
												   		<div class="row">
															<div class="card-body row form-body">
																<div class="row col-lg-12 p-t-20">
																	<div class="col-lg-6 text-left"> 
																		<div class="form-label-group">
																			<label class="control-label"> Class Name
																			</label>
																			<input type="text" name="classname" class="form-control" placeholder="Class Name" value="{{ $class->classname }}">
																		</div>
																	</div>
																	<div class="col-lg-6 text-left"> 
																		<div class="form-label-group">
																			<label class="control-label"> Class Ordering
																			</label>
																			<input type="text" name="classorder" class="form-control" placeholder="Class Ordering" value="{{ $class->classorder }}">
																		</div>
																	</div>
																</div>
															</div>
															<div class="col-lg-12 p-t-20 p-b-20 text-center">
																<button type="button" class="btn btn-circle btn-secondary" data-dismiss="modal">Close</button>
													    		<button type="submit" class="btn btn-circle btn-success save">Update</button>
															</div>
														</div>
													</div>
												</form>
											  </div>
											</div>
											<!-- Modal Ends -->
											<a href="" data-toggle="modal" data-target="#delete{{$class->classid}}">
												<i class="material-icons text-danger">delete</i>
											</a>
											<!-- Modal -->
											<div class="modal fade" id="delete{{ $class->classid }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
											  <div class="modal-dialog modal-dialog-centered" role="document">
											  	<form action="{{ url('admin/class/'.$class->classid) }}" method="POST">
											  		@csrf
											  		@method('delete')
												   	<div class=" modal-content card-box">
												   		<div class="row">
														<div class="col-sm-12">
														<div class="card-head">
															<header>Delete Class Setup</header>
														</div>
														<div class="card-body row ml-5">
															<h4 class="text-white">Do you want to delete Class Setup??</h4>
															<h4 class="text-white">Can't Revert it !!!</h4>
														</div>
														<div class="col-lg-12 p-t-20 p-b-20 text-center">
															<button type="button" class="btn btn-circle btn-secondary" data-dismiss="modal">Close</button>
												    		<button type="submit" class="btn btn-circle btn-danger">Delete</button>
														</div>
														</div>
														</div>
													</div>
												</form>
											  </div>
											</div>
											<!-- Modal Ends -->
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- end page content -->
@endsection