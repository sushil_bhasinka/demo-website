@extends('layouts.adminlayouts.app')
@section('content')
<!-- start page content -->
<style type="text/css">
	img {
		height: 100px;
		width: 100px!important;
	}
</style>
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<div class="page-title-breadcrumb">
				<div class=" pull-left">
					<div class="page-title">Lesson Detail Setup</div>
				</div>
				<ol class="breadcrumb page-breadcrumb pull-right">
					<li><a class="parent-item"
							href="/home"><i class="fa fa-home"></i></a>&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li>Online Education Setup&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li class="active">Lesson Detail Setup</li>
				</ol>
			</div>
		</div>
		<input type="hidden" name="" value="/admin/lessondetail" class="activeurl">
		<div class="row">
			<div class="col-md-12">
				<div class="card card-topline-green">
					<div class="card-head">
						<header>Lesson Detail Setup</header>
						<div class="tools">
							<a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
							<a class="t-collapse btn-color fa fa-chevron-down"
								href="javascript:;"></a>
						</div>
					</div>
					<div class="card-body " id="savetoimage">
						
							@if (session('msg'))
		                        <div class="tstSuccess msg" style="display: none;">
		                            {{ session('msg') }}
		                        </div>
                    		@endif
                    		@if ($errors->any())
							    <div class="tstError error" style="display: none;">
							            @foreach ($errors->all() as $error)
							                {{ $error }}
							            @endforeach
							    </div>
							@endif
						<div class="row col-md-12">
							 <!-- Button trigger modal -->
							<button type="button" class="btn btn-circle btn-success" data-toggle="modal" data-target="#addlessondetail"><i class="fa fa-plus"></i> Add </button>
							
					
							<!-- Modal -->
							<div class="modal fade" id="addlessondetail" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
							  <div class="modal-dialog modal-lg" role="document">
							  	<form action="{{ route('lessondetail.store') }}" method="POST" enctype="multipart/form-data">
							  		@csrf
								   	<div class="modal-content card-box">
										<div class="modal-header">
											<header>Add Lesson Detail Setup</header>
										</div>
								   		<div class="row">
											<div class="card-body row form-body">
												<div class="row col-lg-12 p-t-20">
													<div class="col-lg-4"> 
														<div class="form-label-group">
															<label class="control-label"> Class Name
															</label>
															<select class="form-control classid" name="classid" required="">
																<option disabled selected>Choose Class.............</option>
																@foreach($classes as $class)
																<option value="{{ $class->classid }}">{{ $class->classname }}</option>
																@endforeach
															</select>
														</div>
													</div>
													<div class="col-lg-4"> 
														<div class="form-label-group">
															<label class="control-label"> Subject Name
															</label>
															<select class="form-control subjectid" name="subjectid" required="">
																<option disabled selected> Choose Subject.....</option>
															</select>
														</div>
													</div>
													<div class="col-lg-4"> 
														<div class="form-label-group">
															<label class="control-label"> Lesson Name
															</label>
															<select class="form-control lessonid" name="lessonid" required="">
																<option disabled selected> Choose Lesson.....</option>
															</select>
														</div>
													</div>
												</div>
												<div class="row col-lg-12 p-t-20">
													<div class="col-lg-4"> 
														<div class="form-label-group">
															<label class="control-label"> Lesson Content Entity
															</label>
															<select class="form-control" name="lessonentityid" required="">
																<option disabled selected>Choose Lesson Entity.............</option>
																@foreach($lessonentities as $lessonentity)
																<option value="{{ $lessonentity->lessonentityid }}">{{ $lessonentity->title }}</option>
																@endforeach
															</select>
														</div>
													</div>
												</div>
												<div class="row col-lg-12 p-t-20">
													<div class="col-lg-12">
														<label class="control-label"> Lesson Content
														</label>
														<textarea class="form-control textarea" name="content"></textarea>			
													</div>
												</div>
											</div>
											<div class="col-lg-12 p-t-20 p-b-20 text-center">
												<button type="button" class="btn btn-circle btn-secondary" data-dismiss="modal">Close</button>
									    		<button type="submit" class="btn btn-circle btn-success save">Save changes</button>
											</div>
										</div>
									</div>
								</form>
							  </div>
							</div>
							<!-- Modal Ends -->
						</div>
						<div class="table-scrollable ">
							<table class="table display" width="100%">
								<thead class="text-center">
									<tr>
										<th>SN</th>
										<th>Class Name</th>
										<th>Subject Name</th>
										<th>Lesson Name</th>
										<th>Lesson Entity</th>
										<th>Lesson Content</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									@php
									$i=1;
									@endphp
									@foreach($lessondetails as $lesson)
									<tr class="text-center">
										<td>{{ $i++ }}</td>
										<td class="text-left">{{ $lesson->classname }}</td>
										<td class="text-left">{{ $lesson->subjectname }}</td>
										<td class="text-left">{{ $lesson->lessonname }}</td>
										<td class="text-left">{{ $lesson->title }}</td>
										<td class="text-left">{!! $lesson->lessoncontent !!}</td>
										<td class="text-center">
											<!-- Button trigger modal -->
											<a href="" data-toggle="modal" data-target="#edit{{$lesson->lessondetailid}}">
												<i class="material-icons text-warning">edit</i>
											</a>
											<!-- Modal -->
											<div class="modal fade" id="edit{{ $lesson->lessondetailid }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
											  <div class="modal-dialog modal-lg" role="document">
											  	<form action="{{ url('admin/lessondetail/'.$lesson->lessondetailid) }}" method="POST" enctype="multipart/form-data">
											  		@csrf
											  		@method('put')
												   	<div class="modal-content card-box">
														<div class="modal-header">
															<header>Edit Lesson Detail Setup</header>
														</div>
												   		<div class="row">
															<div class="card-body row form-body text-left">
																<div class="row col-lg-12 p-t-20">
																	<div class="col-lg-4"> 
																		<div class="form-label-group">
																			<label class="control-label"> Class Name
																			</label>
																			<select class="form-control classid" name="classid"required="">
																				<option disabled selected>Choose Class.............</option>
																				@foreach($classes as $class)
																				@if($lesson->classid == $class->classid)
																				<option selected="" value="{{ $class->classid }}">{{ $class->classname }}</option>
																				@else
																				<option value="{{ $class->classid }}">{{ $class->classname }}</option>
																				@endif
																				@endforeach
																			</select>
																		</div>
																	</div>
																	<div class="col-lg-4"> 
																		<div class="form-label-group">
																			<label class="control-label"> Subject Name
																			</label>
																			<select class="form-control subjectid" name="subjectid" required="">
																				<option disabled > Choose Subject.....</option>
																				<option selected="" value="{{ $lesson->subjectid }}">{{ $lesson->subjectname }}</option>
																			</select>
																		</div>
																	</div>
																	<div class="col-lg-4"> 
																		<div class="form-label-group">
																			<label class="control-label">Lesson Name
																			</label>
																			<select class="form-control lessonid" name="lessonid" required="">
																				<option disabled > Choose Lesson.....</option>
																				<option selected="" value="{{ $lesson->lessonid }}">{{ $lesson->lessonname }}</option>
																			</select>
																		</div>
																	</div>
																</div>
																<div class="row col-lg-12 p-t-20">
																	<div class="col-lg-4"> 
																		<div class="form-label-group">
																			<label class="control-label"> Lesson Content Entity
																			</label>
																			<select class="form-control" name="lessonentityid"required="">
																				<option disabled selected>Choose Lesson Entity.............</option>
																				@foreach($lessonentities as $lessonentity)
																				@if($lessonentity->lessonentityid == $lesson->lessonentityid)
																				<option selected="" value="{{ $lessonentity->lessonentityid }}">{{ $lessonentity->title }}</option>
																				@else
																				<option value="{{ $lessonentity->lessonentityid }}">{{ $lessonentity->title }}</option>
																				@endif
																				@endforeach
																			</select>
																		</div>
																	</div>
																</div>
																<div class="row col-lg-12 p-t-20">
																	<div class="col-lg-12">
																		<label class="control-label"> Lesson Content
																		</label>
																		<textarea class="form-control textarea" name="content">{!! $lesson->lessoncontent !!}</textarea>			
																	</div>
																</div>
															</div>
															<div class="col-lg-12 p-t-20 p-b-20 text-center">
																<button type="button" class="btn btn-circle btn-secondary" data-dismiss="modal">Close</button>
													    		<button type="submit" class="btn btn-circle btn-success save">Save changes</button>
															</div>
														</div>
													</div>
												</form>
											  </div>
											</div>
											<!-- Modal Ends -->
											<a href="" data-toggle="modal" data-target="#delete{{$lesson->lessondetailid}}">
												<i class="material-icons text-danger">delete</i>
											</a>
											<!-- Modal -->
											<div class="modal fade" id="delete{{ $lesson->lessondetailid }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
											  <div class="modal-dialog modal-dialog-centered" role="document">
											  	<form action="{{ url('admin/lessondetail/'.$lesson->lessondetailid) }}" method="POST">
											  		@csrf
											  		@method('delete')
												   	<div class=" modal-content card-box">
												   		<div class="row">
														<div class="col-sm-12">
														<div class="card-head">
															<header>Delete Lesson Detail Setup</header>
														</div>
														<div class="card-body row ml-5">
															<h4 class="text-white">Do you want to delete Lesson Detail Setup??</h4>
															<h4 class="text-white">Can't Revert it !!!</h4>
														</div>
														<div class="col-lg-12 p-t-20 p-b-20 text-center">
															<button type="button" class="btn btn-circle btn-secondary" data-dismiss="modal">Close</button>
												    		<button type="submit" class="btn btn-circle btn-danger">Delete</button>
														</div>
														</div>
														</div>
													</div>
												</form>
											  </div>
											</div>
											<!-- Modal Ends -->
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>

							{{ $lessondetails->links() }}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- end page content -->
@endsection
@section('script')
<script type="text/javascript">
	$(document).ready(function(){
		$('.textarea').summernote();
	})
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$(document).on('change','.classid', function(){
			var classid=$(this).val();
			$('.subjectid').html('');
			var op="";
			$.ajax({
	          type:'get',
	          url:'{!!URL::to('/admin/getsubject')!!}',
	          data:{'classid':classid},
	          success:function(data){
	          	console.log(data);
	          	op+='<option disabled selected> Choose Subject.....</option>';
		          for(var i=0;i<data.length;i++){
		          op+='<option value="'+data[i].subjectid+'">'+data[i].subjectname+'</option>';
		           }

		           $('.subjectid').html(" ");
		           $('.subjectid').html(op);
	            }
	        })
		})
	})
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$(document).on('change','.subjectid', function(){
			var subjectid=$(this).val();
			var classid = $('.classid').val();
			$('.lessonid').html('');
			var op="";
			$.ajax({
	          type:'get',
	          url:'{!!URL::to('/admin/getlesson')!!}',
	          data:{'subjectid':subjectid,'classid':classid},
	          success:function(data){
	          	console.log(data);
	          	op+='<option disabled selected> Choose Lesson.....</option>';
		          for(var i=0;i<data.length;i++){
		          op+='<option value="'+data[i].lessonid+'">'+data[i].lessonname+'</option>';
		           }

		           $('.lessonid').html(" ");
		           $('.lessonid').html(op);
	            }
	        })
		})
	})
</script>
@endsection