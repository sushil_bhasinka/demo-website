<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrganizationDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organizationdetails', function (Blueprint $table) {
            $table->bigIncrements('organizationdetailid');
            $table->string('name');
            $table->longblob('logo');
            $table->text('content');
            $table->string('address');
            $table->string('email');
            $table->string('phonenumber');
            $table->string('faxnumber');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organizationdetails');
    }
}
