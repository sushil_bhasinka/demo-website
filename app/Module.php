<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $table='modules';
    public $timestamps=false;
    protected $primaryKey='moduleid';

    protected $fillable=[
        'moduleid', 'modulename', 'moduleorder',
    ];

	public function users()
	{
		return $this->belongsToMany('App\User','usermodules','id','moduleid');
	}
}
