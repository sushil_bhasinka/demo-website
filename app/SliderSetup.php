<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SliderSetup extends Model
{
    protected $table='sliders';
    public $timestamps=false;
    protected $primaryKey='id';
}
