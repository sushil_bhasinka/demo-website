<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{
    protected $table='lessons';
    public $timestamps=false;
    protected $primaryKey='lessonid';
}
