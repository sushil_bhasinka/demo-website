<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use App\User;
use App\Module;
use App\SubModule;
use App\AccessLevel;
use App\UserAccessLevel;
use App\OrganizationDetail;
use App\MenuSetup;
use App\SubMenuSetup;
use App\MemberSetup;
use App\FooterContactSetup;
use App\Gallery;
use App\SocialMediaIcon;
use App\AboutUs;
use App\QuickLinkSetup;
use App\Testimonial;
use Auth;
use DB;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        
    } 

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        view()->composer('*', function($view)
        {
            if(Auth::user()){
                $module = DB::table('modules')
                ->join('usermodules','usermodules.moduleid','=','modules.moduleid')
                ->where('usermodules.id','=',Auth::user()->id)->orderby('moduleorder')->get();
                $view->with('module', $module);
                // dump($module);
                $submodules = DB::table('submodules')
                ->join('usersubmodules','usersubmodules.submoduleid','=','submodules.submoduleid')
                ->where('usersubmodules.id','=',Auth::user()->id)->orderby('submoduleorder')->get();
                $view->with('submodules', $submodules);
            }

                $orgdetails=OrganizationDetail::all();
                $view->with('orgdetails', $orgdetails);

                $users=User::all();
                $view->with('users', $users);

                $accesslevels=AccessLevel::orderBy('accesslevelid','asc')->get();
                $view->with('accesslevels', $accesslevels);
        });

        

        View::composer(['frontend.partials.navbar','frontend.partials.aboutus','frontend.partials.ourteam','frontend.partials.contact','frontend.partials.gallery','frontend.partials.testimonial','frontend.partials.footer'], function ($view) 
        {

            $menus=MenuSetup::where('is_active','YES')->orderBy('menuorder')->get();
            foreach($menus  as $menu)
            {
                $submenus=SubMenuSetup::where([['is_active','YES'],['menusetupid',$menu->menusetupid]])->orderBy('submenuorder')->get();
                $menu->submenu=$submenus;
            }
            $view->with('menus', $menus);
           

            $view->with('aboutus',AboutUs::where('aboutustypeid','1')->orderBy('id','asc')->get());
            $view->with('whychooseustitle',AboutUs::where('aboutustypeid','5')->orderBy('id','asc')->get());
            $view->with('ourteams', MemberSetup::where('is_backgroundimage',null)->orderBy('id')->get());

            $view->with('contact', FooterContactSetup::all());

            $view->with('testimonials', Testimonial::where('is_backgroundimage',null)->get());

            $view->with('galleries', Gallery::inRandomOrder()->limit(6)->get());

            $view->with('icons', SocialMediaIcon::join('icons','socialmediaicons.iconid','=','icons.id')->select('socialmediaicons.*','icons.icon','icons.iconname')->get());
            $view->with('quicklink', QuickLinkSetup::all());
          /*  dump($menus);*/

        });
    }
}
