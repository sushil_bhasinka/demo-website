<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventSetup extends Model
{
    protected $table='events';
    public $timestamps=false;
    protected $primaryKey='id';
}
