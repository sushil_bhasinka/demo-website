<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FooterContactSetup extends Model
{
    protected $table='footercontactsetups';
    public $timestamps=false;
    protected $primaryKey='footercontactsetupid';
}
