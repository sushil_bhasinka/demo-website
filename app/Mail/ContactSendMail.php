<?php


namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactSendMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $contactdetails;

    public function __construct($contactdetails)
    {
        $this->contactdetails = $contactdetails;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('sushil.cresta@gmail.com','Sushil Bhasinka')->subject('ContactUs Message')->view('frontend/contactsendmail')->with('$contactdetails',$this->contactdetails);
    }
}
