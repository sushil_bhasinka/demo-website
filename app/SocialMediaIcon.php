<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialMediaIcon extends Model
{
    protected $table='socialmediaicons';
    public $timestamps=false;
    protected $primaryKey='id';
}
