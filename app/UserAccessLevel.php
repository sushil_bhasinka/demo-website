<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAccessLevel extends Model
{
    protected $table = "useraccesslevels";
    public $timestamps = false;
    protected $primaryKey='id';
}
