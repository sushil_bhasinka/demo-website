<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrganizationDetail extends Model
{
    protected $table='organizationdetails';
   	protected $primaryKey='organizationdetailid';
   	public $timestamps=false;
}
