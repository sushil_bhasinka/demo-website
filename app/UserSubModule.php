<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSubModule extends Model
{
    protected $table = "usersubmodules";
    public $timestamps = false;
    protected $primaryKey='usersubmoduleid';
}
