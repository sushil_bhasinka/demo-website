<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class lessondetail extends Model
{
    protected $table='lessondetails';
    public $timestamps=false;
    protected $primaryKey='lessondetailid';
}
