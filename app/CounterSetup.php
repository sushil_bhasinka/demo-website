<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CounterSetup extends Model
{
    protected $table='counters';
    public $timestamps=false;
    protected $primaryKey='id';
}
