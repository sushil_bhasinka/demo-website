<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NoticeSetup extends Model
{
    protected $table='notices';
    public $timestamps=false;
    protected $primaryKey='id';
}
