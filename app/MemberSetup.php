<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MemberSetup extends Model
{
    protected $table='members';
    public $timestamps=false;
    protected $primaryKey='id';
}
