<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserModule extends Model
{
    protected $table = "usermodules";
    public $timestamps = false;
    protected $primaryKey='usermoduleid';
}
