<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements MustVerifyEmail
{
    public function modules()
    {
        return $this->belongsToMany('App\Module','usermodules','id','moduleid');
    }
    
    public function Submodules()
    {
        return $this->belongsToMany('App\SubModule','usersubmodules','id','submoduleid');
    }

    public function accesslevels()
    {
        return $this->belongsToMany('App\AccessLevel','useraccesslevels','id','accesslevelid');
    }
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
