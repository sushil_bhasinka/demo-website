<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccessLevel extends Model
{
    protected $table='accesslevels';
    public $timestamps=false;
    protected $primaryKey='accesslevelid';

    public function users()
    {
    	return $this->belongsToMany('App\User','useraccesslevels','id','accesslevelid');
    }
}
