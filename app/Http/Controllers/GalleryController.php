<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Album;
use App\Gallery;
use Illuminate\Support\Facades\Session;
use Validator;
use File;
use Image;
use DB;

class GalleryController extends Controller
{
    public function __construct()
    {
       $this->middleware(['auth', 'verified']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $albums=Album::all();
        return view('admin/gallery/index',compact('albums'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param $albumid
     * @return \Illuminate\Http\Response
     */
    public function intoAlbum($albumid)
    {
        $albumname=Album::where('id',$albumid)->select('*')->orderBy('id','desc')->get();
        $albumgalleries=Gallery::where('albumid',$albumid)->select('*')->orderBy('id','desc')->get();
        return view('admin/gallery/create',compact('albumid','albumname','albumgalleries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validAlbum = Validator::make($request->all(),[
            'albumname' => 'required',
            'albumdate' => 'required',
        ]);
        if($validAlbum->passes())
        {
            $albums=new Album;
            $albums->id=getMaxId('albums', 'id');
            $albums->albumname=$request->albumname;
            $albums->albumdate=$request->albumdate;
            $albums->save();
            Session::flash('msg',"New Album Added Successfully!!");
            return redirect()->back();
        }
        elseif($validAlbum->fails())
        {
            Session::flash('error',"Name & Date are Required !!");
            return redirect()->back();
        }
    }

    public function fileStore(Request $request)
    {
         if($request->hasFile('file')){
            foreach($request->file('file') as $key=>$value)
            {

                $imageUpload = new Gallery;
                $image = $value;
                $imageName = $image->getClientOriginalName();
                $imageType = $image->getClientOriginalExtension();
                $imageStr =  (string) Image::make( $image )->encode( $imageType );
                $imageUpload->image=base64_encode($imageStr);

                $imageUpload->id=getMaxId('galleries', 'id');
                $imageUpload->name = $imageName;
                $imageUpload->albumid = $request->albumid;
                $imageUpload->save();

                return response()->json(['success'=>$imageName]);
            }
        }

    }
    public function fileDestroy(Request $request)
    {
        DB::delete("delete from galleries where albumid=".$request->albumid." and name='".$request->filename."'");
        $filename=$request->filename;
        return $filename;
    }


    public function deleteImage($id,$albumid)
    {
        DB::delete("delete from galleries where id=".$id." and albumid=".$albumid);
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
