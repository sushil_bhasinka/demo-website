<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FooterContactSetup;
use Session;
use Image;

class FooterContactSetupController extends Controller
{
    public function __construct()
    {
       $this->middleware(['auth', 'verified']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $footercontactsetups=FooterContactSetup::all();
        return view('admin/footercontactsetup/index',compact('footercontactsetups'));
    }

   

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {    
        $footercontactsetup=new FooterContactSetup;
        $footercontactsetup->footercontactsetupid=getMaxId('footercontactsetups', 'footercontactsetupid');
        $footercontactsetup->tole=$request->tole;
        $footercontactsetup->wardno=$request->wardno;
        $footercontactsetup->municipality=$request->municipality;
        $footercontactsetup->district=$request->district;
        $footercontactsetup->email=$request->email;
        $footercontactsetup->phone=$request->phone;
        $footercontactsetup->map=$request->map;
        if($request->hasFile('image')){
                $image = $request->file('image'); 
                $imageType = $image->getClientOriginalExtension();
                $imageStr =  (string) Image::make( $image )->encode( $imageType );
                $footercontactsetup->image=base64_encode($imageStr);
            }
        $footercontactsetup->save();
        Session::flash('msg',"Contact Us Added Successfully!!");
        return redirect()->back();
    }

  
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $footercontactsetupid)
    {
        $footercontactsetup = FooterContactSetup::where('footercontactsetupid', $footercontactsetupid)->first();

        $footercontactsetup->tole=$request->tole;
        $footercontactsetup->wardno=$request->wardno;
        $footercontactsetup->municipality=$request->municipality;
        $footercontactsetup->district=$request->district;
        $footercontactsetup->email=$request->email;
        $footercontactsetup->phone=$request->phone;
        $footercontactsetup->map=$request->map;
        if($request->hasFile('image')){
                $image = $request->file('image'); 
                $imageType = $image->getClientOriginalExtension();
                $imageStr =  (string) Image::make( $image )->encode( $imageType );
                $footercontactsetup->image=base64_encode($imageStr);
            }
        $footercontactsetup->update();
        Session::flash('msg',"Contact Us Update Successfully!!");
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($footercontactsetupid)
    {
        $footercontactsetup=FooterContactSetup::where('footercontactsetupid', $footercontactsetupid)->first();
        $footercontactsetup->delete();
        
        Session::flash('msg', ' Contact Us Deleted successfully.');
        return redirect()->back();
    }
}
