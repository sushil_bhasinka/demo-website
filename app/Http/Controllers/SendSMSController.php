<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use SmsTo;
use Session;


class SendSMSController extends Controller
{
	public function __construct()
    {
       $this->middleware(['auth', 'verified']);
    }
    public function store(Request $request)
    {
    	// Text message that will be sent to multiple numbers:
		$message = $request->message;

		// Array of mobile phone numbers (starting with the "+" sign and country code):
		$recipients ="+977".$request->number;

		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.sms.to/v1/sms/single/send",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => false,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS =>'{"messages":[{"to":"'.$recipients.'", "message": "'.$message.'"}]}',
		  CURLOPT_HTTPHEADER => array(
		    "Authorization: Bearer h8le9FFrBCi5wiVGVVWkKeiKtXnEMRBN",
		    "Accept: application/json",
		    "Content-Type: application/json"
		  ),
		));
        
		$response= curl_exec($curl);

		$err = curl_error($curl);

		curl_close($curl);
		$res=json_decode($response,true);
		Session::flash('msg',$res['message']);
		return redirect()->back();
		
	}

	public function getBalance()
	{

		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.sms.to/v1/balance",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "Authorization: Bearer h8le9FFrBCi5wiVGVVWkKeiKtXnEMRBN"
		  ),
		));

		 	$response = curl_exec($curl);
		
	 		$res=json_decode($response,true);

			curl_close($curl);

			Session::flash('msg',$res['balance']);
			return redirect()->back();
	}
}
