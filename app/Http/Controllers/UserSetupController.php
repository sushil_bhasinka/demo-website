<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use Image;
use Validator;
use App\User;
use App\UserModule;
use App\UserSubModule;
use App\UserAccessLevel;
Use Session;
use DB;
use Auth;


class UserSetupController extends Controller
{
    public function __construct()
    {
       $this->middleware(['auth', 'verified']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       return view('admin.usersetup.index');
    }

    public function create()
    {
        return view('admin.usersetup.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //return $request->all();
        $validator = Validator::make($request->all(),[
            'name' => 'required|max:50',
            'password'  => 'required|max:50',
            'useremail' =>  'required|email|unique:users,email',
            'contactno' =>  'required|numeric',
            'expirydate'    =>  'required'
        ]);

        $user = new User;
        if($validator->passes())
        {
            $maxid =   getMaxId('users', 'id');
            $user->id = $maxid;
            $user->name = $request->name;
            $user->password = Hash::make($request->password);
            $user->email = $request->useremail;
            $user->contactno = $request->contactno;
            $user->expirydate = $request->expirydate;
            $image = $request->file( 'image' );
            if($image!=null)
            {
                $imageType = $image->getClientOriginalExtension();
                $imageStr =  (string) Image::make( $image )->resize( 200, null, function ( $constraint ) {
                                                 $constraint->aspectRatio();
                                                })->encode( $imageType );
                $user->image= base64_encode( $imageStr );
            } 
            $user->save(); 

    //first delete the selected checkbox and then again save it
        DB::table('usermodules')->where('id', $id)->delete();
        DB::table('usersubmodules')->where('id',$id)->delete();
        DB::table('useraccesslevels')->where('id',$id)->delete();  
    // To save the module which are ticked from checkbox
           
        $user->modules()->sync($request->modules,false);
    // To save the submodule which are ticked from checkbox

       $user->submodules()->sync($request->submodules,false);

    // To save the accesslevel which are ticked from checkbox
        $user->accesslevels()->sync($request->accesslevel,false);

            Session::flash('success', 'New User Added Successfully !!!');
            return redirect()->route('usersetup.index');
        }
        else
        {
            Session::flash('error',"Email should be Unique !!");
            return redirect()->route('usersetup.index');
        }
    }

    public function edit($id)
    {
        $user = User::find($id);

        $usermodule = DB::Table("modules")
        ->select("modules.*",
        DB::raw("(select id from usermodules where usermodules.moduleid = modules.moduleid and usermodules.id = ".$id.")as id"))->orderby('moduleid')->get(); 

        $usersubmodule = DB::Table("submodules")
        ->select("submodules.*",
        DB::raw("(select id from usersubmodules where submodules.submoduleid = usersubmodules.submoduleid and usersubmodules.id=".$id.")as id"))->orderby('submoduleid')->get();
        
        $useraccesslevel=DB::Table("accesslevels")
        ->select("accesslevels.*",
        DB::raw("(select id from useraccesslevels where accesslevels.accesslevelid = useraccesslevels.accesslevelid and useraccesslevels.id=".$id.")as id"))->orderby('accesslevelid')->get();

        return view('admin/usersetup/edit',compact('user','usermodule','usersubmodule','useraccesslevel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //return $request->all();
        $validator = Validator::make($request->all(),[
            'name' => 'required|max:50',
            'useremail' =>  'required|email',
            'contactno' =>  'required|numeric',
            'expirydate'    =>  'required'
        ]);
        if($validator->passes())
        {
            $user = User::find($id);
            $user->name = $request->name;
            $user->email = $request->useremail;
            $user->contactno = $request->contactno;
            $user->expirydate = $request->expirydate;
            $image = $request->file( 'image' );
            if($image!=null)
            {
                $imageType = $image->getClientOriginalExtension();
                $imageStr =  (string) Image::make( $image )->
                                         resize( 200, null, function ( $constraint ) {
                                             $constraint->aspectRatio();
                                         })->encode( $imageType );
                $user->image= base64_encode( $imageStr );
            } 
            $user->save();   

    //first delete the selected checkbox and then again save it
        DB::table('usermodules')->where('id', $id)->delete();
        DB::table('usersubmodules')->where('id',$id)->delete();
        DB::table('useraccesslevels')->where('id',$id)->delete();

    // To save the module which are ticked from checkbox
        $user->modules()->sync($request->modules,false);
        
    // To save the submodule which are ticked from checkbox
        $user->submodules()->sync($request->submodules,false);

    // To save the accesslevel which are ticked from checkbox
        $user->accesslevels()->sync($request->accesslevel,false);

            Session::flash('success', "User Updated Successfully !!!");
            return redirect()->route('usersetup.index');
        }
        else
        {
            Session::flash('error',"Invalid Input Fields !!!");
            return redirect()->route('usersetup.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('users')->where('id', $id)->delete();
        DB::table('usermodules')->where('id', $id)->delete();
        DB::table('usersubmodules')->where('id',$id)->delete();
        DB::table('useraccesslevels')->where('id',$id)->delete();

        Session::flash('success', 'User Deleted successfully.');
        return redirect()->route('usersetup.index');
    }

    public function changepasswordcreate()
    {
        return view('admin.usersetup.changepassword');
    }

    public function changepassword(Request $request)
    {

          $this->validate($request, array(
                'oldpassword' => 'required',
                'password' => 'required|confirmed'    
           
        ));

        $oldpassword = $request->get('oldpassword');
        $password = $request->get('password');
        
        if(!Hash::check($oldpassword, Auth::user()->password))
        {
            Session::flash('error', 'Old Password is incorrect');
       
            return redirect()->back();
        }

        $user = User::find(Auth::user()->id);
        // $user->password = bcrypt($password);
        $user->password=Hash::make($password);
        $user->save();
         Session::flash('success', 'Password has been changed successfully');
        return redirect('/login');
    
    }
}
