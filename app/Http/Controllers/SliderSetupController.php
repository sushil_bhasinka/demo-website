<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SliderSetup;
use Session;
use Validator;
use File;
use Image;

class SliderSetupController extends Controller
{
    public function __construct()
    {
       $this->middleware(['auth', 'verified']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sliders=SliderSetup::all();
        return view('admin/slider/index',compact('sliders'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sliderimage = Validator::make($request->all(),[
            'image' => 'required|mimes:jpeg,png,jpg',
        ]);

      
        if($sliderimage->passes())
        {
            $slider=new SliderSetup;
            $slider->id=getMaxId('sliders', 'id');
            if($request->hasFile('image')){
                $image = $request->file('image'); 
                $imageType = $image->getClientOriginalExtension();
                $imageStr =  (string) Image::make( $image )->encode( $imageType );
                $slider->image=base64_encode($imageStr);
            }
            $slider->captionone=$request->captionone;
            $slider->captiontwo=$request->captiontwo;
            $slider->captionthree=$request->captionthree;
            $slider->save();
            Session::flash('msg',"Slider Detail Added Successfully!!");
            return redirect()->back();
        }
        else
        {
            Session::flash('error',"Invalid Image Type !!");
            return redirect()->back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $slider = SliderSetup::where('id', $id)->first();
        
        $sliderimage = Validator::make($request->all(),[
            'image' => 'mimes:jpeg,png,jpg',
        ]);
      
        if($sliderimage->passes())
        {
            if($request->hasFile('image')){
                $image = $request->file('image'); 
                $imageType = $image->getClientOriginalExtension();
                $imageStr =  (string) Image::make( $image )->encode( $imageType );
                $slider->image=base64_encode($imageStr);
            }
            $slider->captionone=$request->captionone;
            $slider->captiontwo=$request->captiontwo;
            $slider->captionthree=$request->captionthree;
            $slider->update();
            Session::flash('msg',"Slider Detail Added Successfully!!");
            return redirect()->back();
        }
        else
        {
            Session::flash('error',"Invalid Image Type !!");
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        SliderSetup::where('id', $id)->delete();
        Session::flash('msg',"Slider Detail Deleted Successfully!!");
        return redirect()->back();
    }
}
