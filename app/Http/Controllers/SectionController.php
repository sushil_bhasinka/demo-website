<?php

namespace App\Http\Controllers;

use App\Section;
use App\Classes;
use Illuminate\Http\Request;
use Session;
use Validator;

class SectionController extends Controller
{

    public function __construct()
    {
       $this->middleware(['auth', 'verified']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $classes = Classes::all();
        $sections = Section::join('Classes','classes.classid','=','sections.classid')->select('sections.*','classes.classname')->orderBy('classid')->orderBy('sectionorder')->paginate(4);
        return view('/onlineeducation/section/index',compact('classes','sections'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sectionname=Section::where([['sectionname',strtoupper($request->sectionname)],['classid',$request->classid]])->get();

        if(count($sectionname) == 0)
        {
            $sectionnamevalidator = Validator::make($request->all(),[
                'sectionname' => 'required',
                'classid' => 'required',
            ]);
        }
        else
        {
            $sectionnamevalidator = Validator::make($request->all(),[
                'sectionname' => 'required|unique:sections,sectionname',
                'classid' => 'required',
            ]);
        }

        $sectionorder=Section::where([['sectionorder',$request->sectionorder],['classid',$request->classid]])->get();

        if(count($sectionorder) == 0)
        {
            $sectionordervalidator = Validator::make($request->all(),[
                'sectionorder' => 'required',
                'classid' => 'required',
            ]);
        }
        else
        {
            $sectionordervalidator = Validator::make($request->all(),[
                'sectionorder' => 'required|unique:sections,sectionorder',
                'classid' => 'required',
            ]);
        }
        

        if($sectionnamevalidator->passes() && $sectionordervalidator->passes())
        {
            $sections=new Section;

            $sections->sectionid=getMaxId('sections', 'sectionid');
            $sections->sectionname=strtoupper($request->sectionname);
            $sections->sectionorder=$request->sectionorder;
            $sections->classid=$request->classid;
            $sections->save();

            Session::flash('msg',"Section Added Successfully!!");
            return redirect()->back();
        }
        elseif($sectionnamevalidator->fails())
        {
            return redirect()->back()->withErrors($sectionnamevalidator);
        }
        elseif ( $sectionordervalidator->fails()) {
            return redirect()->back()->withErrors($sectionordervalidator);
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sections=Section::where('sectionid',$id)->first();

        $validator = Validator::make($request->all(), [
            'sectionname' => 'required|unique:sections,sectionname,'.$id.',sectionid,classid,'.$request->classid,
            'sectionorder' => 'required|unique:sections,sectionorder,'.$id.',sectionid,classid,'.$request->classid,
        ]);

        if ($validator->passes()) {
            
            $sections->sectionname=strtoupper($request->sectionname);
            $sections->sectionorder=$request->sectionorder;
            $sections->classid=$request->classid;
            $sections->update();

            Session::flash('msg','Section Updated Successfully.');
            return redirect()->back();
        }
            return redirect()->back()->withErrors($validator);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Section::where('sectionid', $id)->delete();
        Session::flash('msg',"section Deleted Successfully!!");
        return redirect()->back();
    }
}
