<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Countersetup;
use Session;
use Validator;

class CounterSetupController extends Controller
{
    public function __construct()
    {
       $this->middleware(['auth', 'verified']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $counters=CounterSetup::all();
        return view('admin/counter/index',compact('counters'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $counternumber = Validator::make($request->all(),[
            'number' => 'required',
        ]);

        $countertitle = Validator::make($request->all(),[
            'title' => 'required|unique:counters,title',
        ]);

      
        if($counternumber->passes() && $countertitle->passes())
        {
            $counter=new CounterSetup;
            $counter->id=getMaxId('counters', 'id');
           
            $counter->number=$request->number;
            $counter->title=$request->title;
            $counter->description=$request->description;
            $counter->save();
            Session::flash('msg',"Counter Detail Added Successfully!!");
            return redirect()->back();
        }
        elseif($counternumber->fails())
        {
            Session::flash('error',"Counter Number Required!!");
            return redirect()->back();
        }
        elseif($countertitle->fails())
        {
            Session::flash('error',"Duplicate Counter Title");
            return redirect()->back();
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $counter = CounterSetup::where('id', $id)->first();
        
        $countertitle = Validator::make($request->all(),[
            'title' => 'required|unique:counters,title,'.$id,
        ]);

        $counternumber = Validator::make($request->all(),[
            'number' => 'required',
        ]);
      
        if($counternumber->passes() && $countertitle->passes())
        {
            $counter->title=$request->title;
            $counter->description=$request->description;
            $counter->update();
            Session::flash('msg',"Counter Detail Updated Successfully!!");
            return redirect()->back();
        }
        elseif($counternumber->fails())
        {
            Session::flash('error',"Counter Number Required !!");
            return redirect()->back();
        }
        elseif($countertitle->fails())
        {
            Session::flash('error',"Duplicate Counter Title");
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        CounterSetup::where('id', $id)->delete();
        Session::flash('msg',"Counter Detail Deleted Successfully!!");
        return redirect()->back();
    }
}
