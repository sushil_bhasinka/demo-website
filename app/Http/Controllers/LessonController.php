<?php

namespace App\Http\Controllers;

use App\Lesson;
use App\Classes;
use App\Subject;
use App\LessonEntity;
use Illuminate\Http\Request;
use Session;
use Validator;

class LessonController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $classes = Classes::all();
        $subjects = Subject::orderBy('subjectorder')->get();
        $lessons=lesson::join('classes','classes.classid','lessons.classid')
        ->join('subjects','subjects.subjectid','lessons.subjectid')
        ->select('lessons.*','classes.classname','subjects.subjectname')
        ->orderBy('classid')->orderBy('subjectid')->orderBy('lessonnumber')->orderBy('lessonorder')
        ->paginate(10);
        return view('/onlineeducation/lesson/index',compact('classes','subjects','lessons'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $lessonorder=lesson::where([['subjectid',$request->subjectid],['classid',$request->classid],['lessonorder',$request->lessonorder]])->get();

        if(count($lessonorder) == 0)
        {
            $lessonordervalidator = Validator::make($request->all(),[
                'lessonorder' => 'required',
                'classid' => 'required',
                'subjectid' => 'required',
                'lessonname' => 'required',
                'lessonnumber' => 'required',
            ]);
        }
        else
        {
            $lessonordervalidator = Validator::make($request->all(),[
                'lessonorder' => 'required|unique:lessons,lessonorder',
                'classid' => 'required',
                'subjectid' => 'required',
                'lessonname' => 'required',
                'lessonnumber' => 'required',
            ]);
        }

        $lessonnumber=lesson::where([['subjectid',$request->subjectid],['classid',$request->classid],['lessonnumber',strtoupper($request->lessonnumber)]])->get();

        $lessonname=lesson::where([['subjectid',$request->subjectid],['classid',$request->classid],['lessonname',strtoupper($request->lessonname)]])->get();

        if(count($lessonnumber) == 0 && count($lessonname) == 0)
        {
            $lessonnumbervalidator = Validator::make($request->all(),[
                'lessonnumber' => 'required',
                'classid' => 'required',
                'subjectid' => 'required',
                'lessonname' => 'required',
                'lessonorder' => 'required',
            ]);
        }
        else
        {
            $lessonnumbervalidator = Validator::make($request->all(),[
                'lessonnumber' => 'required|unique:lessons,lessonnumber',
                'classid' => 'required',
                'subjectid' => 'required',
                'lessonname' => 'required|unique:lessons,lessonname',
                'lessonorder' => 'required',
            ]);
        }

        if($lessonordervalidator->passes() && $lessonnumbervalidator->passes())
        {
            $lesson = new Lesson;
            $lesson->lessonid=getMaxId('lessons', 'lessonid');
            $lesson->classid=$request->classid;
            $lesson->subjectid=$request->subjectid;
            $lesson->lessonnumber=strtoupper($request->lessonnumber);
            $lesson->lessonname=strtoupper($request->lessonname);
            $lesson->lessonorder=$request->lessonorder;
            
            $lesson->save();
            Session::flash('msg',"Lessons Added Successfully!!");
                return redirect()->back();
        }
        elseif($lessonordervalidator->fails())
        {
            return redirect()->back()->withErrors($lessonordervalidator);
        }
        elseif($lessonnumbervalidator->fails())
        {
            return redirect()->back()->withErrors($lessonnumbervalidator);
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Lesson  $lesson
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $lesson = lesson::where('lessonid',$id)->first();

        $validator = Validator::make($request->all(), [
            'classid' => 'required',
            'subjectid' => 'required',
            'lessonnumber' => 'required|unique:lessons,lessonnumber,'.$id.',lessonid,subjectid,'.$request->subjectid.',classid,'.$request->classid.',lessonnumber,'.strtoupper($request->lessonnumber),
            'lessonname' => 'required|unique:lessons,lessonname,'.$id.',lessonid,subjectid,'.$request->subjectid.',classid,'.$request->classid.',lessonname,'.strtoupper($request->lessonname),
            'lessonorder' => 'required|unique:lessons,lessonorder,'.$id.',lessonid,classid,'.$request->classid.',subjectid,'.$request->subjectid,
        ]);

        if($validator->passes())
        {
            $lesson->classid=$request->classid;
            $lesson->subjectid=$request->subjectid;
            $lesson->lessonnumber=strtoupper($request->lessonnumber);
            $lesson->lessonname=strtoupper($request->lessonname);
            $lesson->lessonorder=$request->lessonorder;
            
            $lesson->update();
            Session::flash('msg',"Lessons Updated Successfully!!");
                return redirect()->back();
        }
        else
        {
            return redirect()->back()->withErrors($validator);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Lesson  $lesson
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        lesson::where('lessonid', $id)->delete();
        Session::flash('msg',"Lesson Deleted Successfully!!");
        return redirect()->back();
    }

    public function getsubject(Request $request)
    {
       return Subject::where('classid',$request->classid)->orderBy('subjectorder')->select('subjects.subjectid','subjects.subjectname')->orderBy('subjects.subjectorder')->get();
    }

    public function getlesson(Request $request)
    {
       return Lesson::where([['classid',$request->classid],['subjectid',$request->subjectid]])->orderBy('lessonorder')->select('lessons.lessonid','lessons.lessonname')->get();
    }
}
