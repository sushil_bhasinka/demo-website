<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EventSetup;
use Session;
use Validator;
use File;
use Image;

class EventSetupController extends Controller
{
    public function __construct()
    {
       $this->middleware(['auth', 'verified']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events=EventSetup::all();
        return view('admin/event/index',compact('events'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $eventimage = Validator::make($request->all(),[
            'image' => 'required|mimes:jpeg,png,jpg',
        ]);

        $other = Validator::make($request->all(),[
            'title' => 'required',
            'description' => 'required',
            'date' => 'required',
        ]);

      
        if($eventimage->passes() && $other->passes())
        {
            $event=new EventSetup;
            $event->id=getMaxId('events', 'id');
            if($request->hasFile('image')){
                $image = $request->file('image'); 
                $imageType = $image->getClientOriginalExtension();
                $imageStr =  (string) Image::make( $image )->
                                             resize( 200, null, function ( $constraint ) {
                                                 $constraint->aspectRatio();
                                             })->encode( $imageType );
                $event->image=base64_encode($imageStr);
            }
            $event->title=$request->title;
            $event->description=$request->description;
            $event->date=$request->date;
            $event->save();
            Session::flash('msg',"Event Detail Added Successfully!!");
            return redirect()->back();
        }
        elseif($eventimage->fails())
        {
            Session::flash('error',"Invalid Image Type !!");
            return redirect()->back();
        }
        elseif($other->fails())
        {
            Session::flash('error',"Title, Description, Date are required !!");
            return redirect()->back();
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $event = EventSetup::where('id', $id)->first();
        
        $other = Validator::make($request->all(),[
            'title' => 'required',            
            'description' => 'required',
            'date' => 'required',
        ]);

        $eventimage = Validator::make($request->all(),[
            'image' => 'mimes:jpeg,png,jpg',
        ]);
      
        if($eventimage->passes() && $other->passes())
        {
            if($request->hasFile('image')){
                $image = $request->file('image'); 
                $imageType = $image->getClientOriginalExtension();
                $imageStr =  (string) Image::make( $image )->
                                             resize( 200, null, function ( $constraint ) {
                                                 $constraint->aspectRatio();
                                             })->encode( $imageType );
                $event->image=base64_encode($imageStr);
            }
            $event->title=$request->title;
            $event->description=$request->description;
            $event->date=$request->date;
            $event->update();
            Session::flash('msg',"Event Detail Updated Successfully!!");
            return redirect()->back();
        }
        elseif($eventimage->fails())
        {
            Session::flash('error',"Invalid Image Type !!");
            return redirect()->back();
        }
        elseif($other->fails())
        {
            Session::flash('error',"Title, Description, Date are Required !!");
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        EventSetup::where('id', $id)->delete();
        Session::flash('msg',"Event Detail Deleted Successfully!!");
        return redirect()->back();
    }
}
