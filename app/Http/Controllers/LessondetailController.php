<?php

namespace App\Http\Controllers;

use App\lessondetail;
use App\lesson;
use App\Classes;
use App\Subject;
use App\LessonEntity;
use Illuminate\Http\Request;
use File;
use Image;
use Session;
use Validator;

class LessondetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $classes = Classes::all();
        $subjects = Subject::get();
        $lessons=Lesson::get();
        $lessonentities = LessonEntity::orderBy('order')->get();
        $lessondetails=lessondetail::join('classes','classes.classid','lessondetails.classid')
        ->join('subjects','subjects.subjectid','lessondetails.subjectid')
        ->join('lessons','lessons.lessonid','lessondetails.lessonid')
        ->join('lessonentities','lessonentities.lessonentityid','lessondetails.lessonentityid')
        ->select('lessondetails.*','classes.classname','subjects.subjectname','lessonentities.title','lessons.lessonname')
        ->orderBy('classid')->orderBy('subjectid')->orderBy('lessonnumber')->orderBy('lessonentityid')
        ->paginate(10);
        return view('/onlineeducation/lessondetail/index',compact('classes','subjects','lessonentities','lessondetails','lessons'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $lessonentityid=lessondetail::where([['subjectid',$request->subjectid],['classid',$request->classid],['lessonid',$request->lessonid],['lessonentityid',$request->lessonentityid]])->get();

        if(count($lessonentityid) == 0)
        {
            $lessonentityidvalidator = Validator::make($request->all(),[
                'classid' => 'required',
                'subjectid' => 'required',
                'lessonid' => 'required',
                'content' => 'required',
                'lessonentityid' => 'required'
            ]);
        }
        else
        {
            $lessonentityidvalidator = Validator::make($request->all(),[
                'lessonentityid' => 'required|unique:lessondetails,lessonentityid',
                'classid' => 'required',
                'subjectid' => 'required',
                'lessonid' => 'required',
                'content' => 'required',
            ]);
        }

        if($lessonentityidvalidator->passes())
        {
            $lesson = new Lessondetail;
            $lesson->lessonid=getMaxId('lessondetails', 'lessonid');
            $lesson->classid=$request->classid;
            $lesson->subjectid=$request->subjectid;
            $lesson->lessonid=$request->lessonid;
            $lesson->lessonentityid=$request->lessonentityid;
            $lesson->lessoncontent = $request->input('content');
            
            $lesson->save();
            Session::flash('msg',"Lesson Detail Added Successfully!!");
                return redirect()->back();
        }
        elseif($lessonentityidvalidator->fails())
        {
            return redirect()->back()->withErrors($lessonentityidvalidator);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\lessondetail  $lessondetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $lesson = lessondetail::where('lessondetailid',$id)->first();

        $validator = Validator::make($request->all(), [
            'classid' => 'required',
            'subjectid' => 'required',
            'lessonid' => 'required',
            'content' => 'required',
            'lessonentityid' => 'required|unique:lessondetails,lessonentityid,'.$id.',lessondetailid,subjectid,'.$request->subjectid.',classid,'.$request->classid.',lessonid,'.$request->lessonid,
        ]);

        if($validator->passes())
        {
            $lesson->classid=$request->classid;
            $lesson->subjectid=$request->subjectid;
            $lesson->lessonid=$request->lessonid;
            $lesson->lessonentityid=$request->lessonentityid;
            $lesson->lessoncontent = $request->input('content');
            
            $lesson->update();
            Session::flash('msg',"Lesson Detail Updated Successfully!!");
                return redirect()->back();
        }
        else
        {
            return redirect()->back()->withErrors($validator);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\lessondetail  $lessondetail
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        lessondetail::where('lessondetailid', $id)->delete();
        Session::flash('msg',"Lesson Detail Deleted Successfully!!");
        return redirect()->back();
    }
}
