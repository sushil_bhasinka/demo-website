<?php

namespace App\Http\Controllers;

use App\TimeTable;
use Illuminate\Http\Request;
use DB;
use Image;
use Session;

class TimeTableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $timetables = TimeTable::select('timetables.*','weekdays.day','workoutcategories.title','workoutcategories.filtercode')->join('weekdays','weekdays.id','timetables.weekdayid')->join('workoutcategories','workoutcategories.id','timetables.workoutcategoryid')->orderBy('weekdayid','asc')->orderBy('starttime','desc')->get();
        $weekdays=DB::select('select * from weekdays');
        $workoutcategories=DB::select('select * from workoutcategories');
        return view('admin/timetable/index',compact('timetables','weekdays','workoutcategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $timetable=new TimeTable;

            $timetable->id=getMaxId('timetables', 'id');
            if($request->hasFile('image')){
                $image = $request->file('image'); 
                $imageType = $image->getClientOriginalExtension();
                $imageStr =  (string) Image::make( $image )->encode( $imageType );
                $timetable->image=base64_encode($imageStr);
            }
            $timetable->weekdayid=$request->weekdayid;
            $timetable->starttime=$request->starttime;
            $timetable->endtime=$request->endtime;
            $timetable->workoutcategoryid=$request->workoutcategoryid;
            $timetable->workouttitle=$request->title;
            $timetable->trainer=$request->trainer;
            $timetable->is_backgroundimage=$request->backgroundimage;
            $timetable->save();

            Session::flash('msg',"Time Table Added Successfully!!");
            return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TimeTable  $timeTable
     * @return \Illuminate\Http\Response
     */
    public function show(TimeTable $timeTable)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TimeTable  $timeTable
     * @return \Illuminate\Http\Response
     */
    public function edit(TimeTable $timeTable)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TimeTable  $timeTable
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $timetable=TimeTable::where('id', $id)->first();
            if($request->hasFile('image')){
                $image = $request->file('image'); 
                $imageType = $image->getClientOriginalExtension();
                $imageStr =  (string) Image::make( $image )->encode( $imageType );
                $timetable->image=base64_encode($imageStr);
            }
            $timetable->weekdayid=$request->weekdayid;
            $timetable->starttime=$request->starttime;
            $timetable->endtime=$request->endtime;
            $timetable->workoutcategoryid=$request->workoutcategoryid;
            $timetable->workouttitle=$request->title;
            $timetable->trainer=$request->trainer;
            $timetable->is_backgroundimage=$request->backgroundimage;
            $timetable->update();

            Session::flash('msg',"Time Table Updated Successfully!!");
            return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TimeTable  $timeTable
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        TimeTable::where('id', $id)->delete();
        Session::flash('msg',"Time Table Deleted Successfully!!");
        return redirect()->back();
    }
}
