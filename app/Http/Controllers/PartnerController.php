<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Partner;
use Session;
use Validator;
use File;
use Image;

class PartnerController extends Controller
{
    public function __construct()
    {
       $this->middleware(['auth', 'verified']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $partners=Partner::all();
        return view('admin/partner/index',compact('partners'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $partnerlogo = Validator::make($request->all(),[
            'image' => 'required|mimes:jpeg,png,jpg',
        ]);

        $other = Validator::make($request->all(),[
            'name' => 'required',
            'address' => 'required',
        ]);

      
        if($partnerlogo->passes() && $other->passes())
        {
            $partner=new Partner;
            $partner->id=getMaxId('partners', 'id');
            if($request->hasFile('image')){
                $image = $request->file('image'); 
                $imageType = $image->getClientOriginalExtension();
                $imageStr =  (string) Image::make( $image )->
                                             resize( 200, null, function ( $constraint ) {
                                                 $constraint->aspectRatio();
                                             })->encode( $imageType );
                $partner->logo=base64_encode($imageStr);
            }
            $partner->name=$request->name;
            $partner->address=$request->address;
            $partner->save();
            Session::flash('msg',"Partner Detail Added Successfully!!");
            return redirect()->back();
        }
        elseif($partnerlogo->fails())
        {
            Session::flash('error',"Invalid Image Type !!");
            return redirect()->back();
        }
        elseif($other->fails())
        {
            Session::flash('error',"Name, Address, Date are required !!");
            return redirect()->back();
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $partner = Partner::where('id', $id)->first();
        
        $other = Validator::make($request->all(),[
            'name' => 'required',            
            'address' => 'required',
        ]);

        $partnerlogo = Validator::make($request->all(),[
            'image' => 'mimes:jpeg,png,jpg',
        ]);
      
        if($partnerlogo->passes() && $other->passes())
        {
            if($request->hasFile('image')){
                $image = $request->file('image'); 
                $imageType = $image->getClientOriginalExtension();
                $imageStr =  (string) Image::make( $image )->
                                             resize( 200, null, function ( $constraint ) {
                                                 $constraint->aspectRatio();
                                             })->encode( $imageType );
                $partner->logo=base64_encode($imageStr);
            }
            $partner->name=$request->name;
            $partner->address=$request->address;
            $partner->update();
            Session::flash('msg',"Partner Detail Updated Successfully!!");
            return redirect()->back();
        }
        elseif($partnerlogo->fails())
        {
            Session::flash('error',"Invalid Image Type !!");
            return redirect()->back();
        }
        elseif($other->fails())
        {
            Session::flash('error',"Name, Address, Date are Required !!");
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Partner::where('id', $id)->delete();
        Session::flash('msg',"Partner Detail Deleted Successfully!!");
        return redirect()->back();
    }
}
