<?php

namespace App\Http\Controllers;

use App\Module;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Session;
use DB;

class ModuleController extends Controller
{
    public function __construct()
    {
       $this->middleware(['auth', 'verified']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modules=Module::orderBy('moduleorder','asc')->paginate(10);
        return view('admin/modules/index',compact('modules'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
           'modulename'=>'required|min:3|unique:modules,modulename',
            'moduleorder'=>'required',
        ]);

        if ($validator->fails()) {
            
            Session::flash('error',"some problem occured!!");
            return redirect()->back();
        }

        $module=new Module;
        $module->moduleid=getMaxId('modules','moduleid');
        $module->modulename=$request->modulename;
        $module->moduleorder=$request->moduleorder;
        $module->save();
        Session::flash('msg',"Module Added Successfully.");
        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Module $module)
    {
        $validator = Validator::make($request->all(), [
           'modulename'=>'required|min:3',
            'moduleorder'=>'required',
        ]);

        if ($validator->fails()) {
            
            Session::flash('error',"some problem occured!!");
            return redirect()->back();
        }
        
        $module->update($request->all());
        Session::flash('msg','Module Updated Successfully.');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function destroy(Module $module)
    {
        $module->delete();
        Session::flash('msg','Module Deleted Successfully.');
        return redirect()->back();
    }
}
