<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\NoticeSetup;
use Session;
use Validator;
use Image;

class NoticeSetupController extends Controller
{
    public function __construct()
    {
       $this->middleware(['auth', 'verified']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notices=NoticeSetup::all();
        return view('admin/notice/index',compact('notices'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //return $request->all();
        $noticevalidate = Validator::make($request->all(),[
            'title' => 'required',
            'description' => 'required',
        ]);

      
        if($noticevalidate->passes())
        {
            $notice=new NoticeSetup;
            $notice->id=getMaxId('notices', 'id');

            if($request->hasFile('image')){
                $image = $request->file('image'); 
                $imageType = $image->getClientOriginalExtension();
                $imageStr =  (string) Image::make( $image )->encode( $imageType );
                $notice->image=base64_encode($imageStr);
            }

            $notice->title=$request->title;
            $notice->description=$request->description;
            $notice->video=$request->video;
            $notice->publishdate=$request->publishdate;
            $notice->expirydate=$request->expirydate;
            $notice->save();
            Session::flash('msg',"Notice Detail Added Successfully!!");
            return redirect()->back();
        }
        else
        {
            Session::flash('error',"All Input fields are Required !!");
            return redirect()->back();
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $notice = NoticeSetup::where('id', $id)->first();
        $noticevalidate = Validator::make($request->all(),[
            'title' => 'required',
            'description' => 'required',
        ]);

      
        if($noticevalidate->passes())
        {
            if($request->hasFile('image')){
                $image = $request->file('image'); 
                $imageType = $image->getClientOriginalExtension();
                $imageStr =  (string) Image::make( $image )->encode( $imageType );
                $notice->image=base64_encode($imageStr);
            }
            $notice->title=$request->title;
            $notice->description=$request->description;
            $notice->video=$request->video;
            $notice->publishdate=$request->publishdate;
            $notice->expirydate=$request->expirydate;
            $notice->update();
            Session::flash('msg',"Notice Detail Updated Successfully!!");
            return redirect()->back();
        }
        else
        {
            Session::flash('error',"All Input fields are Required !!");
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        NoticeSetup::where('id', $id)->delete();
        Session::flash('msg',"Notice Detail Deleted Successfully!!");
        return redirect()->back();
    }
}
