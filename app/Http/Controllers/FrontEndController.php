<?php

namespace App\Http\Controllers;

use App\FrontEnd;
use Illuminate\Http\Request;
use Validator;
use App\SliderSetup;
use App\Service;
use App\MemberSetup;
use App\FooterContactSetup;
use App\Gallery;
use App\AboutUs;
use App\TimeTable;
use App\MenuSetup;
use App\SubMenuSetup;
use DB;
use Image;
use Mail;
use App\Mail\SendMail;
use App\Mail\ContactSendMail;

class FrontEndController extends Controller
{
    
    public function index()
    {
        $sliders=SliderSetup::all();
        return view('frontend.home',compact('sliders'));
    }

  
    public function aboutus()
    {
        $descriptions = AboutUs::where('aboutustypeid','2')->orderBy('id','asc')->get();
        $progressbar = AboutUs::where('aboutustypeid','3')->orderBy('id','asc')->get();
        $backgroundimage = AboutUs::where('aboutustypeid','4')->orderBy('id','asc')->get();
        return view('frontend.about',compact('descriptions','progressbar','backgroundimage'));
    }

   
    public function classes()
    {
        return view('frontend.classes');
    }

   
    public function classestimetable()
    {
        $times=TimeTable::select('starttime','endtime')->get()->unique('starttime');
        $weekdays=DB::select('select * from weekdays');

            foreach ($weekdays as $key => $value) {
                
            $timetables=TimeTable::select('timetables.*','workoutcategories.title','workoutcategories.filtercode')->where([['weekdayid',$value->id],['is_backgroundimage',null]])->join('workoutcategories','workoutcategories.id','timetables.workoutcategoryid')->orderBy('starttime','desc')->orderBy('weekdayid','asc')->get();
            $value->timetable=$timetables;
            }
        
            /*return $menus;*/
         //return $weekdays;
        $workoutcategories=DB::select('select * from workoutcategories');
        return view('frontend.classes-timetable',compact('timetables','workoutcategories','times','weekdays'));
    }

  
    public function services()
    {
        $services = Service::where('servicetypeid','1')->orderBy('id','asc')->get();
        $backgroundimage= Service::where('servicetypeid','2')->orderBy('id','asc')->get();
        return view('frontend.services',compact('services','backgroundimage'));
    } 

    public function ourteam()
    {
        $ourteams = MemberSetup::where('is_backgroundimage',null)->orderBy('id')->get();
        $backgroundimage = MemberSetup::where('is_backgroundimage','on')->orderBy('id')->get();
        return view('frontend.team',compact('ourteams','backgroundimage'));
    }

    public function contact()
    {
        $contact = FooterContactSetup::all();
        return view('frontend.contact',compact('contact'));
    }

    public function sendmail(Request $request)
    {
        $contactdetails= array(
                'fullname'=>$request->fullname,
                'email'=>$request->email,
                'subject'=>$request->subject,
                'message'=>$request->message,
           );

        Mail::to('bhasanewar@gmail.com')->send(new ContactSendMail($contactdetails));

        return redirect()->back();
    }

    public function ourblog()
    {
        return view('frontend.blog');
    }

    public function gallery()
    {
        $galleries = Gallery::all();
        return view('frontend.gallery',compact('galleries'));
    }

    public function bmicalculator()
    {
        return view('frontend.bmi-calculator');
    }

    public function testimonial(Request $request)
    {
        $valid = Validator::make($request->all(),[
            'image' => 'required|mimes:jpeg,png,jpg',
        ]);

        if($valid->passes())
        {
             if($request->hasFile('image')){
                $image = $request->file('image'); 
                $imageType = $image->getClientOriginalExtension();
                $imageStr =  (string) Image::make( $image )->encode( $imageType );
            }

            $details= array(
                'fullname'=>$request->name,
                'email'=>$request->email,
                'occupation'=>$request->status,
                'testimonial'=>$request->testimonial,
                'image'=>base64_encode($imageStr),
           );
           
            Mail::to('bhasanewar@gmail.com')->send(new SendMail($details));

            return redirect()->back();
        }
    }

}
