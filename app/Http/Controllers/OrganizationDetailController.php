<?php

namespace App\Http\Controllers;

use App\OrganizationDetail;
use Illuminate\Http\Request;
use File;
use Image;
use DB;
use Validator;
use Session;

class OrganizationDetailController extends Controller
{
   public function __construct()
    {
       $this->middleware(['auth', 'verified']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orgdetails=OrganizationDetail::all();
        return view('admin/organizationdetail/index',compact('orgdetails'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $validator = Validator::make($request->all(),[
            'organizationlogo' => 'image|mimes:jpeg,png,jpg'
        ]);
        if($validator->passes())
        {
            $organizationdetail=new OrganizationDetail;
            if($request->hasFile('organizationlogo')){
                $image = $request->file('organizationlogo'); 
                $imageType = $image->getClientOriginalExtension();
                $imageStr =  (string) Image::make( $image )->
                                             resize( 200, null, function ( $constraint ) {
                                                 $constraint->aspectRatio();
                                             })->encode( $imageType );
            $organizationdetail->logo=base64_encode($imageStr);
            }

            $organizationdetail->organizationdetailid=getMaxId('organizationdetails', 'organizationdetailid');
            $organizationdetail->name=$request->organizationname;
            $organizationdetail->address=$request->organizationaddress;
            $organizationdetail->email=$request->organizationemail;
            $organizationdetail->phonenumber=$request->organizationphone;
            $organizationdetail->faxnumber=$request->organizationfax;
            $organizationdetail->content=$request->organizationabout;
            $organizationdetail->save();
            Session::flash('msg',"Organization Detail Added Successfully!!");
            return redirect()->back();
        }
        else
        {
            Session::flash('error',"Invalid Image Type !!");
            return redirect()->back();
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OrganizationDetail  $organizationDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $organizationdetailid)
    {
        $organizationdetail = OrganizationDetail::where('organizationdetailid', $organizationdetailid)->first();
        $validator = Validator::make($request->all(),[
            'organizationlogo' => 'image|mimes:jpeg,png,jpg'
        ]);
        if($validator->passes())
        {
            if($request->hasFile('organizationlogo')){
                $image = $request->file('organizationlogo'); 
                $imageType = $image->getClientOriginalExtension();
                $imageStr =  (string) Image::make( $image )->
                                             resize( 200, null, function ( $constraint ) {
                                                 $constraint->aspectRatio();
                                             })->encode( $imageType );
            $organizationdetail->logo=base64_encode($imageStr);
            }

            $organizationdetail->name=$request->organizationname;
            $organizationdetail->address=$request->organizationaddress;
            $organizationdetail->email=$request->organizationemail;
            $organizationdetail->phonenumber=$request->organizationphone;
            $organizationdetail->faxnumber=$request->organizationfax;
            $organizationdetail->content=$request->organizationabout;
            $organizationdetail->update();
            Session::flash('msg',"Organization Detail Added Successfully!!");
            return redirect()->back();
        }
        else
        {
            Session::flash('error',"Invalid Image Type !!");
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OrganizationDetail  $organizationDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy($organizationdetailid)
    {
        $organizationdetail=OrganizationDetail::where('organizationdetailid', $organizationdetailid)->first();
        $organizationdetail->delete();
        
        Session::flash('msg', ' Organization Detail Deleted successfully.');
        return redirect()->back();
    }
}
