<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;
use Session;
use Validator;
use File;
use Image;
use DB;

class ServiceController extends Controller
{
    public function __construct()
    {
       $this->middleware(['auth', 'verified']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $servicetype=DB::select("select * from servicetype");
        
        $services=Service::join('servicetype','services.servicetypeid','=','servicetype.id')->select('services.*','servicetype.type')->get();
        return view('admin/service/index',compact('services','servicetype'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $service=new Service;

        $service->id=getMaxId('services', 'id');
        if($request->hasFile('image')){
            $image = $request->file('image'); 
            $imageType = $image->getClientOriginalExtension();
            $imageStr =  (string) Image::make( $image )->encode( $imageType );
            $service->image=base64_encode($imageStr);
        }
        $service->title=$request->title;
        $service->description=$request->description;
        $service->servicetypeid=$request->servicetypeid;
        $service->save();

        Session::flash('msg',"Service Detail Added Successfully!!");
        return redirect()->back();
      
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $service = Service::where('id', $id)->first();
        
        if($request->hasFile('image')){
            $image = $request->file('image'); 
            $imageType = $image->getClientOriginalExtension();
            $imageStr =  (string) Image::make( $image )->encode( $imageType );
            $service->image=base64_encode($imageStr);
        }
        $service->title=$request->title;
        $service->description=$request->description;
        $service->servicetypeid=$request->servicetypeid;

        $service->update();
        
        Session::flash('msg',"Service Detail Updated Successfully!!");
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Service::where('id', $id)->delete();
        Session::flash('msg',"Service Detail Deleted Successfully!!");
        return redirect()->back();
    }
}
