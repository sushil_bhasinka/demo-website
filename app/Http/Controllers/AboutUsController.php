<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AboutUs;
use Session;
use Validator;
use File;
use Image;
use DB;

class AboutUsController extends Controller
{
    public function __construct()
    {
       $this->middleware(['auth', 'verified']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $aboutustype=DB::select("select * from aboutustype");
        $aboutus=AboutUs::join('aboutustype','aboutus.aboutustypeid','=','aboutustype.id')->select('aboutus.*','aboutustype.title as type')->orderBy('aboutustypeid')->get();
        return view('admin/aboutus/index',compact('aboutus','aboutustype'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $aboutustype = Validator::make($request->all(),[
            'aboutustypeid' => 'required',
        ]);

        if($aboutustype->passes())
        {
            $aboutus=new AboutUs;

            $aboutus->id=getMaxId('aboutus', 'id');

            if($request->hasFile('image')){
                $image = $request->file('image'); 
                $imageType = $image->getClientOriginalExtension();
                $imageStr =  (string) Image::make( $image )->encode( $imageType );
                $aboutus->image=base64_encode($imageStr);
            }

            $aboutus->title=$request->title;
            $aboutus->description=$request->description;
            $aboutus->aboutustypeid=$request->aboutustypeid;
            $aboutus->iconclass=$request->icon;
            $aboutus->progresspercentage=$request->progresspercentage;

            $aboutus->save();

            Session::flash('msg',"About Us Detail Added Successfully!!");
            return redirect()->back();
        }
        elseif($aboutustype->fails())
        {
            Session::flash('error',"Please select About Type !!");
            return redirect()->back();
        }
       
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $aboutus = AboutUs::where('id', $id)->first();
        
        $aboutustype = Validator::make($request->all(),[
            'aboutustypeid' => 'required',
        ]);

        if($aboutustype->passes())
        {
            if($request->hasFile('image')){
                $image = $request->file('image'); 
                $imageType = $image->getClientOriginalExtension();
                $imageStr =  (string) Image::make( $image )->encode( $imageType );
                $aboutus->image=base64_encode($imageStr);
            }
            $aboutus->title=$request->title;
            $aboutus->description=$request->description;
            $aboutus->aboutustypeid=$request->aboutustypeid;
            $aboutus->iconclass=$request->icon;
            $aboutus->progresspercentage=$request->progresspercentage;

            $aboutus->update();
            
            Session::flash('msg',"About Us Detail Updated Successfully!!");
            return redirect()->back();
         }
        elseif($aboutustype->fails())
        {
            Session::flash('error',"Please select About Type !!");
            return redirect()->back();
        }
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        AboutUs::where('id', $id)->delete();
        Session::flash('msg',"About Us Deleted Successfully!!");
        return redirect()->back();
    }
}
