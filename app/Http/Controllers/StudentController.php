<?php

namespace App\Http\Controllers;

use App\Student;
use App\Classes;
use App\Section;
use App\User;
use App\UserAccessLevel;
use App\UserModule;
use App\UserSubModule;
use Illuminate\Http\Request;
use Validator;
use Image;
use Session;
use Hash;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students=Student::join('classes','classes.classid','students.studentclass')
        ->join('sections','sections.sectionid','students.studentsection')
        ->paginate(5);
        return view('admin/student/index',compact('students'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $classes=Classes::orderBy('classorder')->get();
        return view('admin/student/create',compact('classes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $studentvalid = Validator::make($request->all(),[
            'image' => 'required|mimes:jpeg,png,jpg',
            'fname' => 'required',
            'lname' => 'required',
            'classid' => 'required',
            'sectionid' => 'required',
            'rollno' => 'required',
            'address' => 'required',
            'fathername' => 'required',
            'mothername' => 'required',
            'email' => 'required',
            'mobileno' => 'required',
            'altmobileno' => 'required',
            'username' => 'required',
            'emailid' => 'required',
            'password' => 'required',
            'cpass' => 'required',
        ]);

      
        if($studentvalid->passes())
        {
            $user=new User;
            $maxid =   getMaxId('users', 'id');
            $user->id = $maxid;
            $user->name=$request->username;
            $user->email=$request->emailid;
            $user->password = Hash::make($request->password);
            $user->contactno = $request->mobileno;


            $student=new Student;
            $student->studentid=getMaxId('students', 'studentid');
            $student->userid=$maxid;
            if($request->hasFile('image')){
                $image = $request->file('image'); 
                $imageType = $image->getClientOriginalExtension();
                $imageStr =  (string) Image::make( $image )->encode( $imageType );
                $student->image=base64_encode($imageStr);
                $user->image=base64_encode($imageStr);
            }
            $user->save();
            $student->firstname=$request->fname;
            $student->lastname= $request->lname;
            $student->studentclass=$request->classid;
            $student->studentsection=$request->sectionid;
            $student->studentrollno=$request->rollno;
            $student->studentaddress=$request->address;
            $student->fathername=$request->fathername;
            $student->fatheroccupation=$request->fjob;
            $student->mothername=$request->mothername;
            $student->motheroccupation=$request->mjob;
            $student->mobileno=$request->mobileno;
            $student->alternativemobileno=$request->altmobileno;
            $student->parentemail=$request->email;
            $student->save();

            $useraccesslevel=new UserAccessLevel;
            $useraccesslevel->id=$maxid;
            $useraccesslevel->accesslevelid=4;
            $useraccesslevel->save();

            Session::flash('msg',"Student Information Added Successfully!!");
            return redirect('/admin/student');
        }
        elseif($studentvalid->fails())
        {
            return redirect('/admin/student')->withErrors($studentvalid);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        $classes=Classes::orderBy('classorder')->get();
        $sections=Section::get();
        $user=User::join('students','students.userid','users.id')->where('users.id',$student->userid)->select('users.*')->get();
        return view('admin/student/edit',compact('student','classes','sections','user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student)
    {
        //return $request->all();

        $studentvalid = Validator::make($request->all(),[
            'fname' => 'required',
            'lname' => 'required',
            'classid' => 'required',
            'sectionid' => 'required',
            'rollno' => 'required',
            'address' => 'required',
            'fathername' => 'required',
            'mothername' => 'required',
            'email' => 'required',
            'mobileno' => 'required',
            'altmobileno' => 'required',
            'username' => 'required',
            'emailid' => 'required',
        ]);

      
        if($studentvalid->passes())
        {
            //return $request->all();
            $user=User::find($student->userid);
            $user->name=$request->username;
            $user->email=$request->emailid;
            if($request->password != null)
            {
                $user->password = Hash::make($request->password);
            }
            $user->contactno = $request->mobileno;

            if($request->hasFile('image')){
                $image = $request->file('image'); 
                $imageType = $image->getClientOriginalExtension();
                $imageStr =  (string) Image::make( $image )->encode( $imageType );
                $student->image=base64_encode($imageStr);
                $user->image=base64_encode($imageStr);
            }
            $user->update();
            $student->firstname=$request->fname;
            $student->lastname= $request->lname;
            $student->studentclass=$request->classid;
            $student->studentsection=$request->sectionid;
            $student->studentrollno=$request->rollno;
            $student->studentaddress=$request->address;
            $student->fathername=$request->fathername;
            $student->fatheroccupation=$request->fjob;
            $student->mothername=$request->mothername;
            $student->motheroccupation=$request->mjob;
            $student->mobileno=$request->mobileno;
            $student->alternativemobileno=$request->altmobileno;
            $student->parentemail=$request->email;
            $student->update();
           

            Session::flash('msg',"Student Information Added Successfully!!");
            return redirect('/admin/student');
        }
        elseif($studentvalid->fails())
        {
            return redirect('/admin/student')->withErrors($studentvalid);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        $student->delete();
        User::where('id',$student->userid)->delete();
        UserAccessLevel::where('id',$student->userid)->delete();
        UserModule::where('id',$student->userid)->delete();
        UserSubModule::where('id',$student->userid)->delete();

        Session::flash('msg',"Student Information and User Login Information Deleted Successfully!!");

        return redirect('/admin/student');
    }

    public function getsection(Request $request)
    {
        return Section::where('classid',$request->classid)->orderBy('sectionorder')->select('sections.sectionid','sections.sectionname')->orderBy('sections.sectionorder')->get();
    }
}
