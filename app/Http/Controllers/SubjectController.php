<?php

namespace App\Http\Controllers;

use App\Subject;
use App\Classes;
use Illuminate\Http\Request;
use Session;
use Validator;

class SubjectController extends Controller
{
    public function __construct()
    {
       $this->middleware(['auth', 'verified']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $classes = Classes::all();
        $subjects = Subject::join('Classes','classes.classid','=','subjects.classid')->select('subjects.*','classes.classname')->orderBy('classid')->orderBy('subjectorder')->paginate(10);
    
        return view('/onlineeducation/subject/index',compact('classes','subjects'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $subjectname=Subject::where([['subjectname',strtoupper($request->subjectname)],['classid',$request->classid]])->get();

        if(count($subjectname) == 0)
        {
            $subjectnamevalidator = Validator::make($request->all(),[
                'subjectname' => 'required',
                'classid' => 'required',
            ]);
        }
        else
        {
            $subjectnamevalidator = Validator::make($request->all(),[
                'subjectname' => 'required|unique:subjects,subjectname',
                'classid' => 'required',
            ]);
        }

        $subjectorder=Subject::where([['subjectorder',$request->subjectorder],['classid',$request->classid]])->get();

        if(count($subjectorder) == 0)
        {
            $subjectordervalidator = Validator::make($request->all(),[
                'subjectorder' => 'required',
                'classid' => 'required',
            ]);
        }
        else
        {
            $subjectordervalidator = Validator::make($request->all(),[
                'subjectorder' => 'required|unique:subjects,subjectorder',
                'classid' => 'required',
            ]);
        }
        

        if($subjectnamevalidator->passes() && $subjectordervalidator->passes())
        {
            $subjects=new Subject;

            $subjects->subjectid=getMaxId('subjects', 'subjectid');
            $subjects->subjectname=strtoupper($request->subjectname);
            $subjects->subjectorder=$request->subjectorder;
            $subjects->classid=$request->classid;
            $subjects->save();

            Session::flash('msg',"Subject Added Successfully!!");
            return redirect()->back();
        }
        elseif($subjectnamevalidator->fails())
        {
            return redirect()->back()->withErrors($subjectnamevalidator);
        }
        elseif($subjectordervalidator->fails())
        {
            return redirect()->back()->withErrors($subjectordervalidator);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $subjects=Subject::where('subjectid',$id)->first();

        $validator = Validator::make($request->all(), [
            'subjectname' => 'required|unique:subjects,subjectname,'.$id.',subjectid,classid,'.$request->classid,
            'subjectorder' => 'required|unique:subjects,subjectorder,'.$id.',subjectid,classid,'.$request->classid,
        ]);

        if ($validator->passes()) {
            
           $subjects->subjectname=strtoupper($request->subjectname);
            $subjects->subjectorder=$request->subjectorder;
            $subjects->classid=$request->classid;
            $subjects->update();

        Session::flash('msg','Subject Updated Successfully.');
        return redirect()->back();
        }
            return redirect()->back()->withErrors($validator);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Subject::where('subjectid', $id)->delete();
        Session::flash('msg',"Subject Deleted Successfully!!");
        return redirect()->back();
    }
}
