<?php

namespace App\Http\Controllers;

use App\Classes;
use Illuminate\Http\Request;
use Session;
use Validator;

class ClassesController extends Controller
{

    public function __construct()
    {
       $this->middleware(['auth', 'verified']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $classes=Classes::orderBy('classorder')->get();
        return view('onlineeducation/class/index',compact('classes'));
    }

   

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $class = Validator::make($request->all(),[
            'classname' => 'required|unique:classes,classname',
            'classorder' => 'required|unique:classes,classorder',
        ]);

        if($class->passes())
        {
            $classes=new Classes;

            $classes->classid=getMaxId('classes', 'classid');
            $classes->classname=$request->classname;
            $classes->classorder=$request->classorder;
            $classes->save();

            Session::flash('msg',"Class Added Successfully!!");
            return redirect()->back();
        }
        elseif($class->fails())
        {
            return redirect()->back()->withErrors($class);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Classes  $classes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $classes=Classes::where('classid',$id)->first();

        $validator = Validator::make($request->all(), [
            'classname' => 'required|unique:classes,classname,'.$id.',classid',
            'classorder' => 'required|unique:classes,classorder,'.$id.',classid',
        ]);

        if ($validator->passes()) {
            
           $classes->classname=$request->classname;
            $classes->classorder=$request->classorder;
            $classes->update();

        Session::flash('msg','Class Updated Successfully.');
        return redirect()->back();
        }
            
            return redirect()->back()->withErrors($validator);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Classes  $classes
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Classes::where('classid', $id)->delete();
        Session::flash('msg',"Class Deleted Successfully!!");
        return redirect()->back();
    }
}
