<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MemberSetup;
use Session;
use Validator;
use File;
use Image;

class MemberSetupController extends Controller
{
    public function __construct()
    {
       $this->middleware(['auth', 'verified']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $members=MemberSetup::all();
        return view('admin/member/index',compact('members'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $name = Validator::make($request->all(),[
            'name' => 'required|unique:members,name',
        ]);
        $other = Validator::make($request->all(),[
            'image' => 'required|mimes:jpeg,png,jpg',
            'position' => 'required',
        ]);

      
        if($name->passes() && $other->passes())
        {
            $member=new MemberSetup;
            $member->id=getMaxId('members', 'id');
            if($request->hasFile('image')){
                $image = $request->file('image'); 
                $imageType = $image->getClientOriginalExtension();
                $imageStr =  (string) Image::make( $image )->encode( $imageType );
                $member->image=base64_encode($imageStr);
            }
            $member->name=$request->name;
            $member->position=$request->position;
            $member->about=$request->about;
            $member->message=$request->message;

            $member->is_backgroundimage=$request->backgroundimage;

            $member->save();
            Session::flash('msg',"Member Message Added Successfully !!");
            return redirect()->back();
        }
        elseif($name->fails())
        {
            Session::flash('error',"Duplicated Member Name !!");
            return redirect()->back();
        }
        
        elseif($other->fails())
        {
            Session::flash('error',"Position Image are Required !!");
            return redirect()->back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $member = MemberSetup::where('id', $id)->first();
        
        $name = Validator::make($request->all(),[
            'name' => 'required|unique:members,name,'.$id,
        ]);
        $other = Validator::make($request->all(),[
            'image' => 'mimes:jpeg,png,jpg',
            'position' => 'required'
        ]);

      
        if($name->passes() && $other->passes())
        {
            if($request->hasFile('image')){
                $image = $request->file('image'); 
                $imageType = $image->getClientOriginalExtension();
                $imageStr =  (string) Image::make( $image )->encode( $imageType );
                $member->image=base64_encode($imageStr);
            }
            $member->name=$request->name;
            $member->position=$request->position;
            $member->about=$request->about;
            $member->message=$request->message;
            $member->is_backgroundimage=$request->backgroundimage;
            $member->update();
            Session::flash('msg',"Member Message Updated Successfully !!");
            return redirect()->back();
        }
        elseif($name->fails())
        {
            Session::flash('error',"Duplicated Member Name !!");
            return redirect()->back();
        }
       
        elseif($other->fails())
        {
            Session::flash('error',"Positon, Image are Required !!");
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        MemberSetup::where('id', $id)->delete();
        Session::flash('msg',"Member Message Deleted Successfully!!");
        return redirect()->back();
    }
}
