<?php

namespace App\Http\Controllers;

use App\SubModule;
use App\Module;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Session;
use DB;

class SubModuleController extends Controller
{
   public function __construct()
    {
       $this->middleware(['auth', 'verified']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $submodule=DB::table('submodules as sm')
            ->join('modules as md', 'sm.moduleid', '=', 'md.moduleid')
            ->select('sm.*', 'md.modulename')
            ->paginate(10);
        $modules=Module::all();
        return view('admin/submodules/index',compact('submodule','modules'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $uniquesubmodule=SubModule::where([['submodulename', '=', $request->submodulename],['moduleid', '=', $request->moduleid]])->get();

        $uniqueurl=SubModule::where([['url', '=', $request->submoduleurl],['moduleid', '=', $request->moduleid]])->get();
        
        if(count($uniquesubmodule)!=0)
        {
            $validator = Validator::make($request->all(), [
                        'submodulename'=>'required|min:3||unique:submodules,submodulename',
                        ]);

            if ($validator->fails()) {
                
                Session::flash('error',"submodule name have to be unique!!");
                return redirect()->back();
            }
        }
        elseif(count($uniqueurl)!=0)
        {
            $validator = Validator::make($request->all(), [
                         'submoduleurl'=>'required|unique:submodules,url'
                        ]);

            if ($validator->fails()) {
                
                Session::flash('error',"submodule url have to be unique!!");
                return redirect()->back();
            }
        }

        else
        {
            $validator = Validator::make($request->all(), [
                            'submodulename'=>'required',
                            'submoduleorder'=>'required',
                        ]);

            if ($validator->fails()) {
                
                Session::flash('error',"submodule name and submodule order required!!");
                return redirect()->back();
            }
        }

        $submodule=new SubModule;
        $submodule->submoduleid=getMaxId('submodules','submoduleid');
        $submodule->moduleid=$request->moduleid;
        $submodule->submodulename=$request->submodulename;
        $submodule->url=$request->submoduleurl;
        $submodule->submoduleorder=$request->submoduleorder;
        $submodule->save();
        Session::flash('msg',"Sub Module Added Successfully.");
        return redirect()->back();
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SubModule  $subModule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $submoduleid)
    {
        $submodule=SubModule::where('submoduleid', $submoduleid)->first();
        //validation form modulename
        if($request->submodulename == $submodule->submodulename)
        {
            $validator = Validator::make($request->all(), [
                          'submodulename'=>'required'
                        ]);

            if ($validator->fails()) {
                
                Session::flash('error',"Submodule Name required !!");
                return redirect()->back();
            }
        }
        elseif ($request->submoduleorder==$submodule->submoduleorder) {
            $validator = Validator::make($request->all(), [
                          'submoduleorder'=>'required'
                        ]);

            if ($validator->fails()) {
                
                Session::flash('error',"Submodule Order required !!");
                return redirect()->back();
            }
        }
         elseif($request->submoduleurl!=$submodule->url)
        {
           $validurl=DB::select('select * from submodules where moduleid='. $request->moduleid . ' and url= "'.$request->submoduleurl.'"');

            if(count($validurl)!=0)
            {
                $validator = Validator::make($request->all(), [
                          'submoduleurl'=>'required|unique:submodules,url'
                        ]);

                if ($validator->fails()) {
                    Session::flash('error',"Submodule url have to be unique !!");
                    return redirect()->back();
                }
             
            }
            else
            {
                $validator = Validator::make($request->all(), [
                          'submoduleurl'=>'required'
                        ]);

                if ($validator->fails()) {
                    
                    Session::flash('error',"Submodule url required !!");
                    return redirect()->back();
                }
            }

        }  
        else
        {
           $validsubmodule=DB::select("select * from submodules where moduleid=".$request->moduleid ." and submodulename=".$request->submodulename);

            if(count($validsubmodule)!=0)
            {
                $validator = Validator::make($request->all(), [
                            'submoduleorder'=>'required',
                            'submodulename'=>'required|max:50|unique:submodules,submodulename'
                        ]);

                if ($validator->fails()) {
                    
                    Session::flash('error',"Submodule order and submodule name required !!");
                    return redirect()->back();
                }
            }
            else
            {
                $validator = Validator::make($request->all(), [
                           'submoduleorder'=>'required'
                        ]);

                if ($validator->fails()) {
                    
                    Session::flash('error',"Submodule order required !!");
                    return redirect()->back();
                }
            }
        }              
        
        //save into database
        $submodule->moduleid=$request->moduleid;
        $submodule->submodulename=$request->submodulename;
        $submodule->submoduleorder=$request->submoduleorder;
        $submodule->url=$request->submoduleurl;
        $submodule->update();

        //set flash data with success message
        Session::flash('msg', ' Sub Module updated successfully.');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SubModule  $subModule
     * @return \Illuminate\Http\Response
     */
    public function destroy($submoduleid)
    {
        $submodule=SubModule::find($submoduleid);
        $submodule->delete();
        
        Session::flash('msg', ' Sub Module Deleted successfully.');
        return redirect()->back();
    }
}
