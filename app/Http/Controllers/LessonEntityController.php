<?php

namespace App\Http\Controllers;

use App\LessonEntity;
use Illuminate\Http\Request;
use Session;
use Validator;


class LessonEntityController extends Controller
{
    public function __construct()
    {
       $this->middleware(['auth', 'verified']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lessonentities=LessonEntity::orderBy('order')->get();
        return view('onlineeducation/lessoncontententity/index',compact('lessonentities'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $lessonEntity = Validator::make($request->all(),[
            'title' => 'required|unique:lessonentities,title',
            'order' => 'required|unique:lessonentities,order',
        ]);

        if($lessonEntity->passes())
        {
            $lessonEntities=new LessonEntity;

            $lessonEntities->lessonentityid=getMaxId('lessonentities', 'lessonentityid');
            $lessonEntities->title=$request->title;
            $lessonEntities->order=$request->order;
            $lessonEntities->save();

            Session::flash('msg',"Lesson Entity Added Successfully!!");
            return redirect()->back();
        }
        elseif($lessonEntity->fails())
        {
            return redirect()->back()->withErrors($lessonEntity);
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LessonEntity  $lessonEntity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $lessonentities=LessonEntity::where('lessonentityid',$id)->first();

        $validator = Validator::make($request->all(), [
            'title' => 'required|unique:lessonentities,title,'.$id.',lessonentityid',
            'order' => 'required|unique:lessonentities,order,'.$id.',lessonentityid',
        ]);

        if ($validator->passes()) {
            
           $lessonentities->title=$request->title;
            $lessonentities->order=$request->order;
            $lessonentities->update();

        Session::flash('msg','Lesson Entity Updated Successfully.');
        return redirect()->back();
        }

        return redirect()->back()->withErrors($validator);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LessonEntity  $lessonEntity
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        LessonEntity::where('lessonentityid', $id)->delete();
        Session::flash('msg',"Lesson Entity Deleted Successfully!!");
        return redirect()->back();
    }
}
