<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SocialMediaIcon;
use Session;
use Validator;
use DB;

class SocialMediaIconController extends Controller
{
    public function __construct()
    {
       $this->middleware(['auth', 'verified']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $icons=DB::select("select * from icons");

        $socialmediaicons=SocialMediaIcon::join('icons','socialmediaicons.iconid','=','icons.id')->select('socialmediaicons.*','icons.icon','icons.iconname')->get();

        return view('admin/socialmediaicon/index',compact('socialmediaicons','icons'));
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $iconid = Validator::make($request->all(),[
            'iconid' => 'required|unique:socialmediaicons,iconid,',
        ]);

        $url = Validator::make($request->all(),[
            'url' => 'required|unique:socialmediaicons,url,',
        ]);
        if($iconid->passes() && $url->passes())
        {
            $socialmediaicon=new SocialMediaIcon;
            $socialmediaicon->id=getMaxId('socialmediaicons', 'id');
            $socialmediaicon->iconid=$request->iconid;
            $socialmediaicon->url=$request->url;
            $socialmediaicon->save();
            Session::flash('msg',"Social Media Icon Added Successfully!!");
            return redirect()->back();
        }
        elseif($iconid->fails())
        {
            Session::flash('error',"Duplicated Social Media name !!");
            return redirect()->back();
        }
        elseif($url->fails())
        {
            Session::flash('error', "Duplicated Social Media url !!");
            return redirect()->back();
        }
    }

    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $socialmediaicon=SocialMediaIcon::where('id', $id)->first();
        $iconid = Validator::make($request->all(),[
            'iconid' => 'required|unique:socialmediaicons,iconid,'.$id,
        ]);

        $url = Validator::make($request->all(),[
            'url' => 'required|unique:socialmediaicons,url,'.$id,
        ]);
        if($iconid->passes() && $url->passes())
        {
            $socialmediaicon->iconid=$request->iconid;
            $socialmediaicon->url=$request->url;
            $socialmediaicon->update();
            Session::flash('msg',"Social Media Icon Update Successfully!!");
            return redirect()->back();
        }
        elseif($iconid->fails())
        {
            Session::flash('error',"Duplicated Social Media name !!");
            return redirect()->back();
        }
        elseif($url->fails())
        {
            Session::flash('error', "Duplicated Social Media url !!");
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        SocialMediaIcon::where('id', $id)->delete();
        Session::flash('msg',"Social Media Icon Deleted Successfully!!");
        return redirect()->back();
    }
}
