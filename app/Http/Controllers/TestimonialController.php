<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Testimonial;
use Session;
use Image;
use Validator;

class TestimonialController extends Controller
{
    public function __construct()
    {
       $this->middleware(['auth', 'verified']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $testimonials = Testimonial::all();
        return view('admin/testimonial/index',compact('testimonials'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //return $request->image;
        $valid = Validator::make($request->all(),[
            'image' => 'image|mimes:jpeg,png,jpg',
        ]);

        if($valid->passes())
        {
        $testimonials=new Testimonial;
            $testimonials->id=getMaxId('testimonials', 'id');
            if($request->hasFile('image')){
                $image = $request->file('image'); 
                $imageType = $image->getClientOriginalExtension();
                $imageStr =  (string) Image::make( $image )->encode( $imageType );
                $testimonials->image=base64_encode($imageStr);
            }
            $testimonials->fullname=$request->name;
            $testimonials->position=$request->position;
            $testimonials->email=$request->email;
            $testimonials->testimonial=$request->testimonial;
            $testimonials->is_backgroundimage=$request->backgroundimage;
            $testimonials->imagecode=$request->imagecode;
            $testimonials->save();
            Session::flash('msg',"Testimonial Added Successfully!!");
            return redirect()->back();

        }
        else
        {
            Session::flash('error',"Invalid Image format!!");
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $testimonials=Testimonial::where('id', $id)->first();
        $valid = Validator::make($request->all(),[
            'image' => 'image|mimes:jpeg,png,jpg',
        ]);

        if($valid->passes())
        {
            if($request->hasFile('image')){
                $image = $request->file('image'); 
                $imageType = $image->getClientOriginalExtension();
                $imageStr =  (string) Image::make( $image )->encode( $imageType );
                $testimonials->image=base64_encode($imageStr);
            }
            $testimonials->fullname=$request->name;
            $testimonials->position=$request->position;
            $testimonials->email=$request->email;
            $testimonials->testimonial=$request->testimonial;
            $testimonials->is_backgroundimage=$request->backgroundimage;
            $testimonials->imagecode=$request->imagecode;
            $testimonials->update();
            Session::flash('msg',"Testimonial Updated Successfully!!");
            return redirect()->back();

        }
        else
        {
            Session::flash('error',"Invalid Image format!!");
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Testimonial::where('id', $id)->delete();
        Session::flash('msg',"Testimonial Deleted Successfully!!");
        return redirect()->back();
    }
}
