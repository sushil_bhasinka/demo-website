<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\QuickLinkSetup;
use Session;
use Validator;

class QuickLinkSetupController extends Controller
{
    public function __construct()
    {
       $this->middleware(['auth', 'verified']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $quicklinksetups=QuickLinkSetup::all();
        return view('admin/quicklinksetup/index',compact('quicklinksetups'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $linkname = Validator::make($request->all(),[
            'linkname' => 'required|unique:quicklinksetups,linkname,',
        ]);

        $linkurl = Validator::make($request->all(),[
            'linkurl' => 'required|unique:quicklinksetups,linkurl,',
        ]);
        if($linkname->passes() && $linkurl->passes())
        {
            $quicklinksetup=new QuickLinkSetup;
            $quicklinksetup->id=getMaxId('quicklinksetups', 'id');
            $quicklinksetup->linkname=$request->linkname;
            $quicklinksetup->linkurl=$request->linkurl;
            $quicklinksetup->save();
            Session::flash('msg',"Quick Link Added Successfully!!");
            return redirect()->back();
        }
        elseif($linkname->fails())
        {
            Session::flash('error',"Duplicated Link name !!");
            return redirect()->back();
        }
        elseif($linkurl->fails())
        {
            Session::flash('error', "Duplicated Link url !!");
            return redirect()->back();
        }
    }

  
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $quicklinksetup=QuickLinkSetup::where('id', $id)->first();

        $linkname = Validator::make($request->all(),[
            'linkname' => 'required|unique:quicklinksetups,linkname,'.$id,
        ]);

        $linkurl = Validator::make($request->all(),[
            'linkurl' => 'required|unique:quicklinksetups,linkurl,'.$id,
        ]);
        if($linkname->passes() && $linkurl->passes())
        {
            $quicklinksetup->linkname=$request->linkname;
            $quicklinksetup->linkurl=$request->linkurl;
            
            $quicklinksetup->update();
            Session::flash('msg',"Quick Link Updated Successfully!!");
            return redirect()->back();
        }
        elseif($linkname->fails())
        {
            Session::flash('error',"Duplicated Link name !!");
            return redirect()->back();
        }
        elseif($linkurl->fails())
        {
            Session::flash('error', "Duplicated Link url !!");
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        QuickLinkSetup::where('id', $id)->delete();
        Session::flash('msg',"Quick Link Deleted Successfully!!");
        return redirect()->back();
    }
}
