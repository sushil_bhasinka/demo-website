<?php

namespace App\Http\Controllers;

use App\MenuSetup;
use Illuminate\Http\Request;
use DB;
use Validator;
use Session;

class MenuSetupController extends Controller
{
    public function __construct()
    {
       $this->middleware(['auth', 'verified']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menusetups=MenuSetup::all();
        return view('admin/menusetup/index',compact('menusetups'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'menuname' => 'required|unique:menusetups,menuname',
            'menuurl' => 'required',
            'menuorder' => 'required',
            'is_active' => 'required'
        ]);
        if($validator->passes())
        {
            $menusetup=new MenuSetup;
            $menusetup->menusetupid=getMaxId('menusetups','menusetupid');
            $menusetup->menuname=$request->menuname;
            $menusetup->menuurl=$request->menuurl;
            $menusetup->menuorder=$request->menuorder;
            if(isset($request->is_active))
            {
                $menusetup->is_active="YES";
            }
            else
            {
                $menusetup->is_active="NO";

            }
            $menusetup->save();
            Session::flash('msg',"Menu Setup Added Successfully!!");
            return redirect()->back();
        }
        else
        {
            Session::flash('error',"All Inputs Fields are Required !!");
            return redirect()->back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MenuSetup  $menuSetup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $menusetupid)
    {
        $menusetup=MenuSetup::where('menusetupid', $menusetupid)->first();

        $validator = Validator::make($request->all(),[
            'menuname' => 'required',
            'menuurl' => 'required',
            'menuorder' => 'required',
            'is_active' => 'required'
        ]);
        if($validator->passes())
        {
            $menusetup->menuname=$request->menuname;
            $menusetup->menuurl=$request->menuurl;
            $menusetup->menuorder=$request->menuorder;
            if(isset($request->is_active))
            {
                $menusetup->is_active="YES";
            }
            else
            {
                $menusetup->is_active="NO";

            }
            $menusetup->update();
            Session::flash('msg',"Menu Setup Updated Successfully!!");
            return redirect()->back();
        }
        else
        {
            Session::flash('error',"All Inputs Fields are Required !!");
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MenuSetup  $menuSetup
     * @return \Illuminate\Http\Response
     */
    public function destroy($menusetupid)
    {
        $menusetup=MenuSetup::where('menusetupid', $menusetupid)->first();
        $menusetup->delete();
        
        Session::flash('msg', ' Menu Setup Deleted successfully.');
        return redirect()->back();
    }
}
