<?php

namespace App\Http\Controllers;

use App\SubMenuSetup;
use App\MenuSetup;
use Illuminate\Http\Request;
use DB;
use Validator;
use Session;

class SubMenuSetupController extends Controller
{
    public function __construct()
    {
       $this->middleware(['auth', 'verified']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menusetups=MenuSetup::all();
        $submenusetups=DB::select("select sms.*,ms.menuname from submenusetups sms,menusetups ms where sms.menusetupid=ms.menusetupid");
        return view('admin/submenusetup/index',compact('menusetups','submenusetups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'submenuname' => 'required|unique:submenusetups,submenuname',
            'submenuurl' => 'required',
            'menusetupid' => 'required',
            'submenuorder' => 'required',
            'is_active' => 'required'
        ]);
        if($validator->passes())
        {
            $submenusetup=new SubMenuSetup;
            $submenusetup->submenusetupid=getMaxId('submenusetups','submenusetupid');
            $submenusetup->menusetupid=$request->menusetupid;
            $submenusetup->submenuname=$request->submenuname;
            $submenusetup->submenuurl=$request->submenuurl;
            $submenusetup->submenuorder=$request->submenuorder;
            if(isset($request->is_active))
            {
                $submenusetup->is_active="YES";
            }
            else
            {
                $submenusetup->is_active="NO";

            }
            $submenusetup->save();
            Session::flash('msg',"Sub-Menu Setup Added Successfully!!");
            return redirect()->back();
        }
        else
        {
            Session::flash('error',"All Inputs Fields are Required !!");
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SubMenuSetup  $subMenuSetup
     * @return \Illuminate\Http\Response
     */
    public function show(SubMenuSetup $subMenuSetup)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SubMenuSetup  $subMenuSetup
     * @return \Illuminate\Http\Response
     */
    public function edit(SubMenuSetup $subMenuSetup)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SubMenuSetup  $subMenuSetup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $submenusetupid)
    {
        $submenusetup=SubMenuSetup::where('submenusetupid', $submenusetupid)->first();
        $validator = Validator::make($request->all(),[
            'submenuname' => 'required',
            'submenuurl' => 'required',
            'menusetupid' => 'required',
            'submenuorder' => 'required',
            'is_active' => 'required'
        ]);
        if($validator->passes())
        {
            $submenusetup->menusetupid=$request->menusetupid;
            $submenusetup->submenuname=$request->submenuname;
            $submenusetup->submenuurl=$request->submenuurl;
            $submenusetup->submenuorder=$request->submenuorder;
            if(isset($request->is_active))
            {
                $submenusetup->is_active="YES";
            }
            else
            {
                $submenusetup->is_active="NO";

            }
            $submenusetup->save();
            Session::flash('msg',"Sub-Menu Setup Added Successfully!!");
            return redirect()->back();
        }
        else
        {
            Session::flash('error',"All Inputs Fields are Required !!");
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SubMenuSetup  $subMenuSetup
     * @return \Illuminate\Http\Response
     */
    public function destroy( $submenusetupid)
    {
        $submenusetup=SubMenuSetup::where('submenusetupid', $submenusetupid)->first();
        $submenusetup->delete();
        
        Session::flash('msg', 'Sub-Menu Setup Deleted successfully.');
        return redirect()->back();
    }
}
