<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuSetup extends Model
{
    protected $table='menusetups';
    public $timestamps=false;
    protected $primaryKey='menusetupid';

    
}
