<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubModule extends Model
{
    protected $table='submodules';
    public $timestamps=false;
    protected $primaryKey='submoduleid';
    public function users()
	{
		return $this->belongsToMany('App\User','usersubmodules','id','submoduleid');
	}
}
