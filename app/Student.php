<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table='students';
    public $timestamps=false;
    protected $primaryKey='studentid';

    protected $fillable = [
    	"firstname",
"lastname",
"image",
"studentclass",
"studentsection",
"studentrollno",
"studentaddress",
"fathername",
"fatheroccupation",
"mothername",
"motheroccupation",
"mobileno",
"alternativemobileno",
"parentemail",
    ];
}
