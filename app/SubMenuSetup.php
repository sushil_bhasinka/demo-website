<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubMenuSetup extends Model
{
    protected $table='submenusetups';
    public $timestamps=false;
    protected $primaryKey='submenusetupid';

   
}
