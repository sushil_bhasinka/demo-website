<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LessonEntity extends Model
{
    protected $table='lessonentities';
    public $timestamps=false;
    protected $primaryKey='lessonentityid';
}
