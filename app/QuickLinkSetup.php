<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuickLinkSetup extends Model
{
    protected $table='quicklinksetups';
    public $timestamps=false;
    protected $primaryKey='id';
}
